---
level: Nivel 1
school: Ilusión
casting-time: Acción
range: Cono 15ft
targets: Todos dentro del rango
components: V, S, M (una pizca de polvo o de arena tintada de rojo, amarillo y azul)
duration: 1 Turno
prepared: false
---
# `=this.file.name`

> [!infobox|wfull center]
> # `=this.file.name`
> ###### `=this.level`, `=this.school`
> |   |
> ---|---|
> Tiempo de Conjuración | `=this.casting-time`|
> Duración | `=this.duration` |
> Rango | `=this.range` |
> Objetivos | `=this.targets` |
> Componentes | `=this.components` |
> ###### Descripción
> Un cegador haz de luz intermitente y colorido surge de tu mano. Tira 6d10: el resultado indica cuántos puntos de golpe de criaturas puede afectar este conjuro. Las criaturas que se encuentren dentro de un cono de 15 pies cuyo origen seas tú quedan afectadas en orden ascendente de acuerdo a sus puntos de golpe actuales (ignora a las criatura inconscientes y a las que no pueden ver).
> A partir de la criatura que tiene menos puntos de golpe actuales, cada criatura afectada por este conjuro queda cegada hasta que termina el conjuro. Réstale al total los puntos de golpe de cada criatura antes de pasar a la siguiente criatura con menos puntos de golpe. Para afectar a una criatura, su número de puntos de golpe debe ser igual o menor que el total restante.
> **En niveles superiores.** Cuando lanzas este conjuro usando un espacio de conjuro de nivel 2 o superior, tira 2d10 adicionales por cada nivel por encima de 1.