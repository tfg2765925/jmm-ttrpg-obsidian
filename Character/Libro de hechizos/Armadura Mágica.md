---
level: Nivel 1
school: Abjuración
casting-time: 1 Accción
range: Toque
targets: 1 objetivo
components: V S M (Un trozo de cuero curtido)
duration: 8 horas
prepared: true
---
# `=this.file.name`

> [!infobox|wfull center]
> # `=this.file.name`
> ###### `=this.level`, `=this.school`
> |   |
> ---|---|
> Tiempo de Conjuración | `=this.casting-time`|
> Duración | `=this.duration` |
> Rango | `=this.range` |
> Objetivos | `=this.targets` |
> Componentes | `=this.components` |
> ###### Descripción
> Tocas a una criatura voluntaria que no lleve armadura y una fuerza protectora mágica la rodea hasta que el conjuro termina. La CA base del objetivo pasa a ser 13 + su modificador por Destreza. El conjuro termina si el objetivo se pone una armadura o si, como acción, disipas el conjuro.