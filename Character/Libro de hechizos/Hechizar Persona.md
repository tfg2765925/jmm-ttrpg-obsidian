---
level: Nivel 1
school: Encantamiento
casting-time: 1 Acción
range: 30ft
targets: 1 Criatura
components: V, S
duration: 1 Hora
prepared: true
---
# `=this.file.name`

> [!infobox|wfull center]
> # `=this.file.name`
> ###### `=this.level`, `=this.school`
> |   |
> ---|---|
> Tiempo de Conjuración | `=this.casting-time`|
> Duración | `=this.duration` |
> Rango | `=this.range` |
> Objetivos | `=this.targets` |
> Componentes | `=this.components` |
> ###### Descripción
> Intentas hechizar a un humanoide que puedes ver dentro del alcance. Este debe hacer una tirada de salvación de Sabiduría, para la que tendrá ventaja si tus compañeros o tú estáis luchando contra él. Si falla, queda hechizado hasta que el conjuro termina o hasta que tus compañeros o tú lo dañáis. La criatura hechizada te ve como un conocido amistoso. Cuando el conjuro termina, la criatura sabe que estaba hechizada por ti.
> **En niveles superiores.** Cuando lanzas este conjuro usando un espacio de conjuro de nivel 2 o superior, puedes elegir como objetivo a una criatura adicional por cada nivel por encima de 1. Las criaturas deben estar a 30 pies o menos entre sí.
