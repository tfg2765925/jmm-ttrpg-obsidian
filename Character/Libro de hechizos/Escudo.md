---
level: Nivel 1
school: Abjuración
casting-time: 1 Reacción
range: Personal
targets: 1 Objetivo
components: V S
duration: 1 Turno
prepared: true
---
# `=this.file.name`

> [!infobox|wfull center]
> # `=this.file.name`
> ###### `=this.level`, `=this.school`
> |   |
> ---|---|
> Tiempo de Conjuración | `=this.casting-time`|
> Duración | `=this.duration` |
> Rango | `=this.range` |
> Objetivos | `=this.targets` |
> Componentes | `=this.components` |
> ###### Descripción
> Una barrera invisible de fuerza mágica te protege. Hasta el principio de tu siguiente turno, tienes un bonificador de +5 a la CA, incluso contra el ataque que lo activa, y no recibes daño del conjuro Proyectil mágico.