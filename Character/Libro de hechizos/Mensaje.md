---
level: Truco
school: Transmutación
casting-time: 1 Acción
range: 120ft
targets: 1 objetivo
components: V S M (Un hilo de cobre)
duration: 1 Turno
prepared: false
---
# `=this.file.name`

> [!infobox|wfull center]
> # `=this.file.name`
> ###### `=this.level`, `=this.school`
> |   |
> ---|---|
> Tiempo de Conjuración | `=this.casting-time`|
> Duración | `=this.duration` |
> Rango | `=this.range` |
> Objetivos | `=this.targets` |
> Componentes | `=this.components` |
> ###### Descripción
> Señalas con el dedo a una criatura dentro del alcance y susurras un mensaje. El objetivo (y solo el objetivo) escucha el mensaje y puede responder con un susurro que solo tú puedes escuchar.
> Puedes lanzar este conjuro a través de objetos sólidos si conoces al objetivo y sabes que está al otro lado de la barrera. El silencio mágico, una piedra de 1 pie, 1 pulgada de metal común, una delgada capa de plomo o un bloque de madera de 3 pies bloquean el conjuro. El conjuro no tiene por qué seguir una línea recta y puede viajar libremente en las esquinas o
