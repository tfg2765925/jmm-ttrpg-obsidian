---
level: Nivel 1
school: Evocación
casting-time: 1 Acción
range: 120ft
targets: Hasta 3 criaturas
components: V, S
duration: Instantáneo
prepared: true
---
# `=this.file.name`

> [!infobox|wfull center]
> # `=this.file.name`
> ###### `=this.level`, `=this.school`
> |   |
> ---|---|
> Tiempo de Conjuración | `=this.casting-time`|
> Duración | `=this.duration` |
> Rango | `=this.range` |
> Objetivos | `=this.targets` |
> Componentes | `=this.components` |
> ###### Descripción
> Creas tres dardos brillantes de fuerza mágica. Cada dardo alcanza a una criatura de tu elección que puedas ver dentro del alcance. Cada dardo inflige 1d4 + 1 puntos de daño por fuerza. Todos los dardos se impactan al mismo tiempo y puedes dirigirlos a una criatura o a varias.
> **En niveles superiores.** Cuando lanzas este conjuro usando un espacio de conjuro de nivel 2 o superior, el conjuro crea un dardo más por cada nivel por encima de 1.
