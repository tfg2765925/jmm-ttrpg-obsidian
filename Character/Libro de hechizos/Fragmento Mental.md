---
level: Truco
school: Encatamiento
casting-time: 1 Acción
range: 60
targets: 1 Objetivo
components: V
duration: Instantáneo
prepared: true
---
# `=this.file.name`

> [!infobox|wfull center]
> # `=this.file.name`
> ###### `=this.level`, `=this.school`
> |   |
> ---|---|
> Tiempo de Conjuración | `=this.casting-time`|
> Duración | `=this.duration` |
> Rango | `=this.range` |
> Objetivos | `=this.targets` |
> Componentes | `=this.components` |
> ###### Descripción
> Introduces una púa desorientadora de energía psíquica en la mente de una criatura que puedas ver dentro del alcance. El objetivo deberá superar una tirada de salvación de Inteligencia o recibirá 1d6 de daño psíquico y restará 1d4 en la siguiente tirada de salvación que haga antes del final de tu siguiente turno.
> El daño de este conjuro aumenta en 1d6 cuando alcanzas ciertos niveles: el nivel 5 (2d6), el nivel 11 (3d6) y el nivel 17 (4d6)