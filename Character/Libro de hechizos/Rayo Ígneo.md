---
level: Truco
school: Evocación
casting-time: 1 Acción
range: "120"
targets: 1 Objetivo
components: V S
duration: Instantáneo
prepared: "false"
---
# `=this.file.name`

> [!infobox|wfull center]
> # `=this.file.name`
> ###### `=this.level`, `=this.school`
> |   |
> ---|---|
> Tiempo de Conjuración | `=this.casting-time`|
> Duración | `=this.duration` |
> Rango | `=this.range` |
> Objetivos | `=this.targets` |
> Componentes | `=this.components` |
> ###### Descripción
> Disparas una lanza de fuego a un objetivo a rango realiza un ataque de hechizo contra el objetivo. En caso de golpear este recibe `dice:1d10` de daño de fuego. Cualquier objeto inflamable comienza a arder en caso si no está siendo portado por ninguna persona. Este hechizo aumenta de daño en 1d10 en los siguientes niveles: 5, 11 y 17