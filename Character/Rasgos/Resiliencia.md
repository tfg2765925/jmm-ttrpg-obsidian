# `=this.file.name`

> [!infobox|wfull center]
> ###### Descripción
> Posees ventaja en tiradas salvadoras contra veneno y posees resistencia al daño por veneno.
> - No necesitas comer, beber o respirar.
> - Eres inmune a la enfermedad.
> - No necesitas dormir y los conjuros de dormir no te afectan.