# `=this.file.name`

> [!infobox|wfull center]
> ###### Descripción
> Cuando tomas un descanso largo debes permanecer al menos seis horas en estado inactivo. en ese estado puedes ver y oir.
