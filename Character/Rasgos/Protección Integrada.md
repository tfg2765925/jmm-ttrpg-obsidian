# `=this.file.name`

> [!infobox|wfull center]
> ###### Descripción
> Tu cuerpo posee capas defensivas que pueden mejorarse con armadura:
> Ganas +1 a la Clase de Armadura.
> Sólo puedes ponerte armadura en la que seas competente. Tardas una hora en incorporar la armadura a tu cuerpo y otra hora en quitarla.
> Mientras vivas tu armadura no se puede quitar de tu cuerpo en contra de tu voluntad.