---
proficient: false
abilityscr:
  str: 10
  dex: 14
  con: 16
  wis: 12
  int: 17
  cha: 8
prof:
  SV:
    str: false
    dex: false
    con: false
    wis: true
    int: true
    cha: false
  level: 2
  arc: true
  atl: false
  eng: false
  med: false
  rel: false
  his: true
  per: true
  invs: true
  pers: true
  acr: false
  inti: false
class: Mago
race: Forjado
level: 1
subclass: Escuela de la Abjuración
background: Pinocho
maxhp: 9
currenthp: 9
temphp: 
shield: false
basearmor: 10
lv1SpellSlot: 2
modStr: 0
modDex: 2
modCon: 3
modWis: 1
modInt: 3
modCha: -1
inventory:
  slot1: true
  slot2: true
---
> [!column| flex 2 no-title]
> > [!infobox|wfit left]
> > # Solren
> > ![[Solren.PNG|cover hsmall]]
> > ###### Datos
> >||
> > :---:|:---:|
> > **Nivel** | `INPUT[inlineSelect(option(1), option(2), option(3), option(4), option(5), option(6), option(7), option(8), option(9), option(10), option(11), option(12), option(13), option(14), option(15), option(16), option(17), option(18), option(19), option(20)):level]`
> > **Clase** | `INPUT[text:class]`
> > **Sub-clase** | `INPUT[text:subclass]`
> > **Raza** | `INPUT[text:race]`
> > **Trasfondo** | `INPUT[text:background]`
> > ###### Estadísticas
> > ||
> > :---:|:---:|
> > STR | `INPUT[number:abilityscr.str]`
> > DEX | `INPUT[number:abilityscr.dex]` 
> > CON | `INPUT[number:abilityscr.con]`
> >  WIS | `INPUT[number:abilityscr.wis]`
> >  INT | `INPUT[number:abilityscr.int]`
> >  CHA | `INPUT[number:abilityscr.cha]`
> > ###### Tiradas de Salvación
> > | Nombre | Prof | Valor | Nombre | Prof | Valor
> > | ---- | ---- |---- | ---- |---- | ---- |
> > | STR | `INPUT[toggle:prof.SV.str]` | `VIEW[{modStr}+{prof.level}*{prof.SV.str}]` | DEX | `INPUT[toggle:prof.SV.dex]` | `VIEW[{modDex}+{prof.level}*{prof.SV.dex}]`
> > | CON | `INPUT[toggle:prof.SV.con]` | `VIEW[{modCon}+{prof.level}*{prof.SV.con}]` | WIS | `INPUT[toggle:prof.SV.wis]` | `VIEW[{modWis}+{prof.level}*{prof.SV.wis}]`
> > | INT | `INPUT[toggle:prof.SV.int]` | `VIEW[{modInt}+{prof.level}*{prof.SV.int}]` | CHA | `INPUT[toggle:prof.SV.cha]` | `VIEW[{modCha}+{prof.level}*{prof.SV.cha}]`
> > ###### Habilidades
> > | Nombre | Prof | Valor | Nombre | Prof | Valor
> > | ---- | ---- |---- | ---- |---- | ---- |
> > | Acrobacias | `INPUT[toggle:prof.acr]` | `VIEW[{modDex}+{prof.level}*{prof.acr}]` | Arcana | `INPUT[toggle:prof.arc]` | `VIEW[{modInt}+{prof.level}*{prof.arc}]`
> > | Atletismo | `INPUT[toggle:prof.atl]` | `VIEW[{modStr}+{prof.level}*{prof.atl}]` | Engaño | `INPUT[toggle:prof.eng]` | `VIEW[{modCha}+{prof.level}*{prof.eng}]`
> > | Historia | `INPUT[toggle:prof.his]` | `VIEW[{modInt}+{prof.level}*{prof.his}]` | Interpretación | `INPUT[toggle:prof.inte]` | `VIEW[{modCha}+{prof.level}*{prof.inte}]`
> > | Intimidación | `INPUT[toggle:prof.inti]` | `VIEW[{modCha}+{prof.level}*{prof.inti}]` | Investigación | `INPUT[toggle:prof.invs]` | `VIEW[{modInt}+{prof.level}*{prof.invs}]`
> > | Juego de Manos | `INPUT[toggle:prof.juego]` | `VIEW[{modCha}+{prof.level}*{prof.juego}]` | Medicina | `INPUT[toggle:prof.med]` | `VIEW[{modWis}+{prof.level}*{prof.med}]`
> > | Naturaleza | `INPUT[toggle:prof.nat]` | `VIEW[{modWis}+{prof.level}*{prof.nat}]` | Percepción | `INPUT[toggle:prof.per]` | `VIEW[{modWis}+{prof.level}*{prof.per}]`
> > | Perspicacia | `INPUT[toggle:prof.pers]` | `VIEW[{modWis}+{prof.level}*{prof.pers}]` | Persuasión | `INPUT[toggle:prof.persuasion]` | `VIEW[{modCha}+{prof.level}*{prof.persuasion}]`
> > | Religión | `INPUT[toggle:prof.rel]` | `VIEW[{modWis}+{prof.level}*{prof.rel}]` | Sigilo | `INPUT[toggle:prof.sig]` | `VIEW[{modDex}+{prof.level}*{prof.sig}]`
> > | Supervivencia | `INPUT[toggle:prof.sup]` | `VIEW[{modWis}+{prof.level}*{prof.sup}]` | T. Animales | `INPUT[toggle:prof.ani]` | `VIEW[{modWis}+{prof.level}*{prof.ani}]`
>
> >[!infobox|wmed left]
> ># Rasgos
> >###### Raciales
> > - [[Resiliencia]]
> > - [[Vigilancia de Centinela]]
> > - [[Protección Integrada]]
> > - [[Diseño Especializado]]
> > ###### Clase
> > - Libro de Hechizos
> > - Recuperación Arcana
> > ###### Otros
> ># Habilidades Pasivas
> > | Percepción | Investigación |Perspicacia|
> | --- | --- | --- |
> |   `VIEW[10 + {modWis}+{prof.level}*{prof.per}]`  |  `VIEW[10 + {modInt}+{prof.level}*{prof.invs}]`   |   `VIEW[10 +{modInt}+{prof.level}*{prof.pers}]`  |
> >###### Otras proficiencias
> >||
> > :---:|:---:|
> > Idiomas | <ul><li>Común</li><li>Sylvano</li></ul>
> > Armadura | Ninguna
> > Armas | <ul><li>Bastón</li><li>Daga</li><li>Ballesta Ligera</li><li>Dardo</li><li>Honda</li></ul>
> > Herramientas | 
> > # Combate
> > ###### Vida
> > | Máxima | Total | Temporal |
> > |---|---|---|
> > | `VIEW[{maxhp}]` | `INPUT[number:currenthp]` | `INPUT[number:temphp]` |
> > ###### Estadísticas
> > ||
> > :---:|:---:|
> > Resistencias |<ul><li>Veneno</li></ul> |
> > AC|Base:`INPUT[number:basearmor]` Total: `VIEW[{basearmor} + 1 + {modDex} + 2*{shield}]` |
> > Iniciativa | `VIEW[{modDex}]`|
> > Velocidad de Movimiento | 30ft
> > Escudo Equipado `INPUT[toggle:shield]`
> > 
> > | Acción | Tirada | Daño |
> > | ------ | ------ | ------ |
> > | [[Rayo Ígneo]] |   `dice:1d20`+`VIEW[{modInt} + {prof.level}]`    | `dice: 1d10` (Fuego)|
> > | Daga |   `dice:1d20`+`VIEW[{modDex} + {prof.level}]`  |   `dice: 1d4`+`VIEW[{modDex}]` (Corte o Punzante)  |

> [!infobox|wfull center]
> ##  Hechizos
> ### Máximo de Hechizos preparados: `VIEW[{level} + {modInt}]`
> ### Salvación de hechizo: `VIEW[8 + {modInt} + {prof.level}]`
> ### Bonus de Ataque de Conjuro `VIEW[{modInt} + {prof.level}]` 
> | | | | | | 
> | :---: | :---: | :---: | :---: | :---: |
> |**Trucos** <ul><li>[[Rayo Ígneo]]</li><li>[[Mensaje]]</li><li>[[Fragmento Mental]]</li></ul> | **Nivel 1** (`VIEW[{lv1SpellSlot}]`/2) <br> 0`INPUT[slider(minValue(0), maxValue(2),stepSize(1)):lv1SpellSlot]`2 <ul><li>`INPUT[toggle:Libro de hechizos/Familiar#prepared]` [[Familiar]]</li><li>`INPUT[toggle:Libro de hechizos/Armadura Mágica#prepared]` [[Armadura Mágica]]</li><li>`INPUT[toggle:Libro de hechizos/Escudo#prepared]`[[Escudo]]</li><li>`INPUT[toggle:Libro de hechizos/Misil Mágico#prepared]`[[Misil Mágico]]</li><li>`INPUT[toggle:Libro de hechizos/Rociada de Color#prepared]`[[Rociada de Color]]</li><li>`INPUT[toggle:Libro de hechizos/Hechizar Persona#prepared]`[[Hechizar Persona]]</li></ul> | **Nivel 2** | **Nivel 3** | **Nivel 4** |
> | **Nivel 5** | **Nivel 6** | **Nivel 7**  | **Nivel 8** | **Nivel 9** |

> [!infobox|wfull center]
>## Inventario
>
> |Nombre|Equipado|Peso|Cantidad|
> | --- | --- | --- | --- |
> | Ropa de viaje |  `INPUT[toggle:inventory.slot1]` |     |   1  |
> | Daga |  `INPUT[toggle:inventory.slot2]` |     |   1  |

`VIEW[round(floor(({level}+7)/4),0)][math(hidden):prof.level]`
`VIEW[floor(({abilityscr.str}-10)/2)][math(hidden):modStr]`
`VIEW[floor(({abilityscr.dex}-10)/2)][math(hidden):modDex]`
`VIEW[floor(({abilityscr.con}-10)/2)][math(hidden):modCon]`
`VIEW[floor(({abilityscr.wis}-10)/2)][math(hidden):modWis]`
`VIEW[floor(({abilityscr.int}-10)/2)][math(hidden):modInt]`
`VIEW[floor(({abilityscr.cha}-10)/2)][math(hidden):modCha]`
`VIEW[6 + {level}*{modCon}][math(hidden):maxhp]`

