# `=this.file.name`
Hadas es el término paraguas que recoge a todas las criaturas que cruzaron desde los parajes féericos hacia este plano de la realidad. Varían en formas y tamaños así como en intencionalidades. Algunas hadas son bestias inconscientes que atacan a cualquier criatura en la cercanía mientras que otras residen en metrópolis secretas alejadas de la población de [[Marquis]].

## Enemigos en esta categoría
- [[Jinente de Ratones]]
- [[Hada Guerrera]]
- [[Hada Hechicera]]