
```statblock
name: Jinete de Ratones
layout: Daggerheart Layout
description: Hada guerrera montada a lomos de un ratón de campo porta un arco pétreo.
damage: [2, 12, 18]
hp: 5
ac: 14
stress: 5
attacks: 
- name: Arco Pétreo
  desc: "Larga Distancia `dice: 1d20+3` Daño `dice: 2d10` (físico)"
moves: 
- name: Salto de Ratón
  desc: "Gasta 1 punto de miedo para saltar hacia una distancia cercana, al impactar todos las criaturas cuerpo a cuerpo con el Jinete han de superar una tirada de Fuerza o recibir `dice: 3d6` de daño físico"
```