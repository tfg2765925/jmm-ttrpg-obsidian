
```statblock
name: Hada Guerrera
layout: Daggerheart Layout
description: Hada insectoide de tamaño minúsculo, porta un escudo y un sable feérico además de magia antigua
damage: [4, 10, 18]
"hp": 5
"ac": 12
stress: 3
attacks: 
- name: Sable Féerico
  desc: "Cuerpo a Cuerpo `dice: 1d20+4` Daño `dice: 3d6` (físico)"
moves: 
- name: Bloqueo con escudo
  desc: "Como reacción gasta un punto de miedo para alzar el escudo y bloquear 2 puntos de daño"
- name: Llama féerica
  desc: "Gasta 1 punto de miedo para lanzar un rayo de energía feérica a dos objetivos en rango lejano `dice: 1d20+4`. En caso de golpear los objetivo que incendiado y recibe `dice: 1d4` de daño de fuego"
```