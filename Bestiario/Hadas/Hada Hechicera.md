
```statblock
name: Hada Hechicera
layout: Daggerheart Layout
description: Pequeño humano insectoide vistiendo con túnicas de sabio de Pueblo Añejo han estudiado las magia antigua y la usan para curar y ayudar a sus aliados
damage: [1, 8, 15]
hp: 4
ac: 14
stress: 4
attacks: 
- name: Bastón de Polvo
  desc: "Larga distancia `dice: 1d20+3` Daño `dice: 2d6` (mágico)"
moves: 
- name: Presencia Calmante
  desc: "Cuando un aliado termina su turno a distancia cercana del Hada Hechicera recupera 1 PV"
- name: Encantamiento de Protección
  desc: "Gasta un punto de miedo para entonar un cántico arcano todos los aliados cercanos reciben una de las siguientes opciones <ul><li>Recuperan 2 PV</li> <li>Su armadura aumenta en 1</li> <li>Obtienen ventaja en su siguiente ataque</li></ul>"
```