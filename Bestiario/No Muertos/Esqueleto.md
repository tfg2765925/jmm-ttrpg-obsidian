
```statblock
name: Esqueleto
layout: Daggerheart Layout
description: Restos óseos maltratados por el apso del timepo, la magia negra que mantiene a este no muerto parece a punto de desvanecer
damage: [1, 1, 1]
hp: 1
ac: 8
stress: 1
attacks: 
- name: Garras
  desc: "Cuerpo a Cuerpo `dice: 1d20-1` Daño 3 (físico)"
moves: 
- name: Esbirro (pasiva)
  desc: "Por cada 4 puntos de daño que reciba esta criatura, el atacante puede derrotar a otro esbirro a rango de su ataque."
- name: Ataque de Grupo
  desc: "Selecciona a un objetivo y ativa a todos los Esqueletos (Esbirro) a rango cercano de él. Realizan una tirada de ataque conjunta `dice: 1d20-1` en caso de golpear cada uno realiza 3 puntos de daño físico."
```