# `=this.file.name`
Cuando se produce la muerte se separa el alma del cuerpo. Cuando una conciencia arcana o divina se adentra en el cuerpo físico abandonado se produce una resurección. El cuerpo se encuentra animado por un entre extraño y no por el alma que previamente lo ocupaba, en muchos casos la personalidad cambia pero si la posesión del cuerpo ha sido reciente es posible que la nueva alma tenga los recuerdos del antiguo propietario.

## Enemigos en esta categoría
- [[Arquero Esqueleto]]
- [[Caballero Esqueleto]]
- [[Esqueleto]]
- [[Rebovino]]
- [[Zombie]]