
```statblock
name: Guerrero Esqueleto
layout: Daggerheart Layout
description: Los restos óseos de un antiguo guerrero animados por magia negra. Aun conserva las armas con las que se le enterró.
damage: [1, 4, 8]
hp: 3
ac: 10
stress: 1
exp: 
attacks: 
- name: Cimitarra
  desc: "Cuerpo a Cuerpo `dice: 1d20` Daño `dice: 2d8` (físico)"
moves: 
- name: Sólo Huesos
  desc: "Esta criatura es resistente al daño físico"
- name: No se puede matar (reacción)
  desc: "Cuando el guerrero esqueleto queda reducido a 0 puntos de vida y aún quedan enemigos en la escena, lanza un `dice: 1d6`. En un 6 el guerrero se reforma con todos sus puntos de vida y estrés intactos."
```
