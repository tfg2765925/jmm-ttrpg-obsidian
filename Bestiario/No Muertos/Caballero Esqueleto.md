
```statblock
name: Caballero Esqueleto
layout: Daggerheart Layout
description: Los restos óseos de un antiguo General animados por magia negra. Aun conserva las armas y la carisma que portaba en vida.
damage: [1, 7, 13]
hp: 5
ac: 13
stress: 2
attacks: 
- name: Espadón Oxidado
  desc: "Muy cercano `dice: 1d20+2` Daño `dice: 2d8` (físico)"
moves: 
- name: Sólo Huesos
  desc: "Esta criatura es resistente al daño físico"
- name: Corte Óseo
  desc: "Gasta un punto de estrés para realizar un ataque de espadón contra todos los enemigos a rango muy cercano. En caso de éxito reciben el daño y marca un punto de estrés."
- name: Golpe Póstumo
  desc: "Cuando esta criatura muere, realiza un ataque de espadón contra una criatura a rango muy cercano. En caso de éxito esta recibe `dice: 2d10+5` puntos de daño físico y pierde `dice: 1d4` de esperanza."
```
