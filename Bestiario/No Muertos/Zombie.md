
```statblock
name: Zombie
layout: Daggerheart Layout
description: Un cadáver animado mediante magia negra, se mueve de forma lenta y torpe motivado únicamente por el hambre.
damage: [1, 4, 6]
hp: 4
ac: 9
stress: 1
attacks: 
- name: Mordisco
  desc: "Cuerpo a Cuerpo `dice: 1d20` Daño `dice: 2d8` (físico)"
moves: 
- name: Son demasiados
  desc: "Si un otro zombie se encuentra a rango cercano del objetivo a golpear, el zombie original tiene ventaja en su ataque."
- name: Horripilante (pasiva)
  desc: "Cuando el zombie golpea a una criatura esta pierde también un punto de esperanza, si no puediese quitarse ningún punto de esperaza se elimina un punto de vida extra."
```