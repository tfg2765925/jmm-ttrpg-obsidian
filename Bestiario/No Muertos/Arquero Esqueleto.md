
```statblock
name: Arquero Esqueleto
layout: Daggerheart Layout
description: Los restos óseos de un antiguo guerrero animados por magia negra. Con sorprendente puntería para carecer de ojos.
damage: [1, 4, 7]
hp: 3
ac: 9
stress: 1
exp: 
attacks: 
- name: Arco largo
  desc: "Lejano `dice: 1d20+2` Daño `dice: 2d8` (físico)"
moves: 
- name: Sólo Huesos (pasiva)
  desc: "Esta criatura es resistente al daño físico"
- name: Oportunista (pasiva)
  desc: "Cuando uno o más aliados están junto al objetivo de un ataque este recibe el doble de daño."
- name: Disparo Cargado
  desc: "Realiza un ataque de arco contra una criatura vulnerable. En caso de éxito marca un punto de estrés para realizar `dice: 2d10` de daño físico."
```
