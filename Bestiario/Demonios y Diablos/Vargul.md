
```statblock
name: Vargul 
layout: Daggerheart Layout
description: Demonios que se asemejan a una cabeza un mana con alas de muerciélago de color rosado emergiendo de dónde deberían estar sus orejas.
damage: [1, 1, 1]
hp: 1
ac: 12
stress: 1
exp: Alado +3
attacks: 
- name: Mordisco
  desc: "Cuerpo a Cuerpo `dice: 1d20-1` Daño `dice: 1d6` (físico)"
moves: 
- name: Ataque de Grupo
  desc: "Selecciona a un objetivo y ativa a todos los Vargul a rango lejano de él. Realizan una tirada de ataque conjunta `dice: 1d20-1` en caso de golpear cada uno realiza `dice: 1d6` puntos de daño físico."
- name: Infección
  desc: "Gasta un punto de miedo para realizar un ataque contra una criatura cuerpo a cuerpo `dice: 1d20` en caso de golpear la criatura recibe 1 punto de daño y es infectada con una fiebre infernal. La fiebre infernal toma un punto de vida cada hora a la criatura afectada hasta que esta realize un descanso largo o reciba un hechizo de curación"
```