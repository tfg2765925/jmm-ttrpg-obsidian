
```statblock
name: Diablo Anciano
layout: Daggerheart Layout
description: Monstruosidad que se alimenta de las emociones mortales, este especímen ha consumido tanta materia que ha crecido hasta alcanzar proporciones colosales. Su hambre lo ha transformado en una criatura amorfa y he hinchada pero sigue siendo un oponente formidable.
damage: [1, 31, 71]
hp: 14
ac: 17
stress: 8
attacks: 
- name: Llamarada
  desc: "Rango lejano `dice: 1d20+4` Daño `dice: 4d10+4` (mágico)."
moves: 
- name: Presencia Maléfica
  desc: "Todas las criaturas que terminen su turno a rango cercano del Diablo han de realizar una tirada de instinto con DC 15. Las criaturas reciben `dice: 3d6` de daño mágico, la mitad en caso de superar la dificultad."
- name: Consumir
  desc: "Gasta 2 puntos de miedo para realizar un ataque `dice: 1d20+6` contra una criatura cercana. En caso de golpear la criatura que atrapada en el interior del diablo y recibe `dice: 1d6+2` de daño de físico al final de cada uno de sus turnos."
```