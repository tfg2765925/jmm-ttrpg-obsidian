
```statblock
name: Diablillo
layout: Daggerheart Layout
description: Un pequeño humanoide con el tren inferior de un pollo y piel rojiza.
damage: [1, 1, 1]
hp: 1
ac: 10
stress: 1
attacks: 
- name: Garras
  desc: "Cuerpo a Cuerpo `dice: 1d20-2` Daño 4 (mágico)."
moves: 
- name: Esbirro (pasiva)
  desc: "Por cada 4 puntos de daño que reciba esta criatura, el atacante puede derrotar a otro esbirro a rango de su ataque."
- name: Ataque de Grupo
  desc: "Selecciona a un objetivo y ativa a todos los Diablilloa rango cercano de él. Realizan una tirada de ataque conjunta `dice: 1d20-2` en caso de golpear cada uno realiza 4 puntos de daño mágico."
```