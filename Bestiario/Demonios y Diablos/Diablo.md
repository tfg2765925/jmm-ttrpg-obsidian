
```statblock
name: Diablo
layout: Daggerheart Layout
description: Humanoide de azufre nativo del plano infernal. Se alimentan de las emociones mortales permitiéndoles crecer en tamaño y poder.
damage: [1, 19, 36]
hp: 8
ac: 14
stress: 5
attacks: 
- name: Llamarada
  desc: "Rango lejano `dice: 1d20+2` Daño `dice: 3d10+6` (mágico)."
moves: 
- name: Sifón
  desc: "Gasta un punto de miedo, el Diablo apunta hasta a dos enemigos y estos deben realizar una tirada de instinto con DC 13, reciben `dice: 3d8` de daño mágico y estar vulnerables hasta el final de su turno. En caso de superar la tirada sólo reciben la mitad del daño."
```