
```statblock
name: Demonio Mayor
layout: Daggerheart Layout
description: Gran demonio escamoso con cuerpo de león y dos cabezas de caballo, cubierto en una densa humareda negra que emerge de sus hombros
damage: [1, 14, 45]
hp: 9
ac: 16
stress: 5
attacks: 
- name: Mordisco
  desc: "Cuerpo a Cuerpo `dice: 1d20+2` Daño `dice: 4d8+5` (físico)"
moves: 

- name: Bicéfalo
  desc: "El demonio comeinza con dos cabezas, Puede activarse un número de veces igual a las cabezas restantes del demonio. Cuando el demonio recibe daño grave o severo pierde una cabeza."
- name: Fuego Infernal
  desc: "Gasta 1 punto de miedo para realizar un ataque `dice: 1d20+5` contra un grupo de enemigos a distancia lejana y agrupados a distancia cercana. Los objetivos reciben `dice: 6d8` de daño mágico."
- name: Segador
  desc: "Cuando el demonio realiza tiradas de daño marca 1 punto de estrés para añadir el número de puntos de vida actuales del demonio a la tirada."
```