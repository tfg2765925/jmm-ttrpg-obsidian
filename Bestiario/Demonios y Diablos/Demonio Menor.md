
```statblock
name: Demonio Menor
layout: Daggerheart Layout
description: Un demonio de tamaño mediano, protegido por una gruesa mata de pelo negro de la que asoman dos cuernos de carnero.
damage: [1, 8, 15]
hp: 7
ac: 14
stress: 4
exp: 
attacks: 
- name: 
  desc: "Cuerpo a Cuerpo `dice: 1d20+3` Daño `dice: 4d6` (físico)"
moves: 
- name: Todos Caerán (pasiva)
  desc: "CUalquier criatura a rango cercano del demonio que tiren con miedo pierde una esperanza."
- name: Segador
  desc: "Cuando la criatura realiza tiradas de daño marca 1 punto de estrés para añadir el número de puntos de vida actuales del demonio a la tirada."
- name: Fuego Infernal
  desc: "Gasta 1 punto de miedo para invocad una lluvia de fuego en rango lejano del demonio. Todas las criaturas que no sean demonios deven superar una tirada de agilidad(14) o recibir `dice: 4d8` de daño mágico. Las criaturas que superen la tirada reciben la mitad de daño."
```