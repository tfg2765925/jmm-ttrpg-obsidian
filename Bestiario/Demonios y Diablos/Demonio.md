
```statblock
name: Demonio
layout: Daggerheart Layout
description: Demonio cornudo con cuerpo humanoide y cola de serpiente, cubierto en una densa humareda negra que emerge de sus hombros
damage: [1, 13, 31]
hp: 6
ac: 14
stress: 2
attacks: 
- name: Garras
  desc: "Cuerpo a Cuerpo `dice: 1d20+2` Daño `dice: 4d8+5` (físico)"
moves: 
- name: Fuego Infernal
  desc: "Gasta 1 punto de miedo para realizar un ataque `dice: 1d20+3` contra un grupo de enemigos a distancia lejana y agrupados a distancia cercana. Los objetivos reciben `dice: 4d8` de daño mágico."
- name: Segador
  desc: "Cuando el demonio realiza tiradas de daño marca 1 punto de estrés para añadir el número de puntos de vida actuales del demonio a la tirada."
```