# `=this.file.name`
En esta categoría de criaturas se introducen todos los habitantes originarios de los círculos del inferno. Seres de azufre que aparecen en [[Marquis]] mediante portales o proyecciones astrales. A pesar de pertenecer a la misma categoría los Diablos y Demonios son claramente distinguibles por su aspecto y comportamiento.
Los Diablos son emavoros, seres que se alimentan de emociones y drenan la energía vital de las criaturas cercanas en el proceso. La mayoría de los diablos se oculta en poblaciones extensas teniendo especial cuidado en no desvelar su verdadera naturaleza. A medida que devoran emociones crecen en poder y tamaño dificultando la tarea de mantener su tapadera, es habitual que los diablos más precavidos pasen décadas sin alimentarse para evitar transformarse en un Diablo Anciano.
Los Demonios son quimeras construidas a partir de los miedos de las razas inteligentes de Marquis. Se alimentan de carne y son insaciables. Aunque inteligentes y muchos demonios mayores son especialmente astutos los mueve un deseo de consumir y destruir. 

## Enemigos en esta categoría
- [[Demonio]]
- [[Demonio Mayor]]
- [[Demonio Menor]]
- [[Diablillo]]
- [[Diablo Anciano]]
- [[Diablo]]