# `=this.file.name`
Criaturas de los confines del mundo, invasores de otros planos de la realidad y monstruos de la Guerra Divina son algunas de las criaturas clasificadas como Abominaciones por el Gremio. Generalmente motivadas por un irrefrenable deseo de destrucción estas criaturas vive por y para matar. La aparición de una abominación en cualquier parte del continente es suficiente para enviar a un contingente de élite a investigar.

## Enemigos en esta categoría
- [[Avatar de Nuzrum]]
- [[Fátula]]