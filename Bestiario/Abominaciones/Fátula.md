
```statblock
name: Fátula
image: img/Fatula.jpg
layout: Daggerheart Layout
description: Amalgama de cabezas humanas con piel pálida y sonrisas inquietantes, todas unidades mediante largos cuellos musculosos a una masa de carne y huesos amorfa.
damage: [1, 21, 40]
hp: 9
ac: 18
stress: 6
exp: Sentidos Agudos +3, Aterrador +2
attacks: 
- name: Mordisco
  desc: "Cercano `dice: 1d20+5` Daño `dice: 3d12` (físico)"
moves: 
- name: Amenaza Policéfala (pasiva)
  desc: "La Fátula comienza con 5 cabezas, cuando recibe daño severo o grave pierde una."
- name: Implacable
  desc: "Esta criatura puede activarse un número de veces igual a las cabezas que tenga por ronda, gasta un marcador por cada una."
- name: Regeneración
  desc: "Si la hidra ha perdido algún punto de vida, gasta un punto de miedo para recuperar 2HP y regenerar una cabeza."
- name: Segador
  desc: "Cuando la criatura realiza tiradas de daño marca 1 punto de estrés para añadir el número de puntos de vida actuales de la criatura a la tirada."
- name: Debilidad Mágica (pasiva)
  desc: "Cuando la hidra recibe daño mágico se encuentra aturdida. Mientras está artudida no puede regenerar cabezas pero es inmune al daño mágico. Siempre que la hidra se active mientras se encuentra aturdida tira un `dice: 1d8` de un 5 o más deja de estar aturdida."
```