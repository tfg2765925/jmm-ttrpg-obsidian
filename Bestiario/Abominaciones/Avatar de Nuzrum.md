
```statblock
name: Avatar de Nuzrum
layout: Daggerheart Layout
description: Un humanoide corrompido por el dios muerto Nuzrum. El dios caído no es consciente de su situación controla a esta grotesca marioneta de forma errática y macarba.
damage: [1, 8, 15]
hp: 10
ac: 14
stress: 4
attacks: 
- name: Dagas de hueso
  desc: "Cuerpo a Cuerpo `dice: 1d20+3` Daño `dice: 2d6` (físico)"
moves: 
- name: Implacable
  desc: "Esta criatura puede activarse hasta 2 veces por ronda, gasta un marcador por cada una."
- name: Chispa nigromántica
  desc: "Gasta un punto de estrés para lanzar una bola de fuego a dos objetos que se encuentren a rango lenjano, `dice: 1d20+5`. En caso de golepar se encuentran vulnerables hasta que recuperen vida y reciben `dice: 3d8` de daño mágico."
- name: Cosumir
  desc: "Gasta 2 puntos de miedo para abir la boca situada en el pecho del avatar, de ella emergen 4 brazos que tratan de agarrar a una criatura cuerpo a cuerpo `dice: 1d20+3`. Puede liberarse sueprando una tirada de fuerza(15), o cuando el avatar recibe daño severo. Por cada marcador que se posicione en el contador la criatura recibe `dice: 1d12+3` de daño mágico."
```