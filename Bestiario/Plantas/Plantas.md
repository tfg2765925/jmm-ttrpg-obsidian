# `=this.file.name`
En esta categoría se agrupan a los espíritus de la naturaleza que comparten características con la vegetación del Marquis. Es un termino amplio que  abarca tanto a creaciones mortales como a antiguos guardianes de la Guerra Divina, y a especímenes naturales. La inteligencia de esta criaturas abarca prácticamente todo el espectro de la consciencia.

## Enemigos en esta categoría
- [[Defensor Verde]]
- [[Dríade Joven]]
- [[Pimpollo]]