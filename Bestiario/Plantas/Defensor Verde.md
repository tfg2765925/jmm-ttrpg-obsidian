
```statblock
name: Defensor Verde
layout: Daggerheart Layout
description: Un corpulento monstruo compuesto de viñas y otras plantas trepadoras.
damage: [1, 8, 14]
hp: 7
ac: 10
stress: 3
exp: Enorme +3
attacks: 
- name: Viñas
  desc: "Cercano `dice: 1d20+2` Daño `dice: 1d10+3` (físico)"
moves: 
- name: Golpe sísmico
  desc: "Golpea el suelo con gran poder, todos los enemigos en rango muy cercano salen despedidos a rango lejano. Marca un punto de estrés por cada enemigo desplazado."
- name: Enredar
  desc: "Realiza un ataque contra un objetigo en rango cercano `dice: 1d20+3`, en caso de éxito gasta un punto de miedo para realizar `dice: 2d8+3` y atraer al objeto a rango cuerpo a cuerpo atrapandolo. Esrta condición termina cuando el obejtivo supera una tirada de fuerza(14) o el Defensor recibe daño severo. "
```