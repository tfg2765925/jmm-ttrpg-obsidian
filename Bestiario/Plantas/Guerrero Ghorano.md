
```statblock
name: Guerrero Ghorano
layout: Daggerheart Layout
description: Una planta con aspecto humanoide vestido con una armadura de corteza y hojas.
damage: [1, 6, 11]
hp: 4
ac: 11
stress: 2
exp: Rastreador +2
attacks: 
- name: Guadaña
  desc: "Cuerpo a Cuerpo `dice: 1d20` Daño `dice: 2d8+2` (físico)"
moves: 
- name: Tácticas de Equipo
  desc: "Cuando el guerror golpea con su mordisco y otro guerrero se encuentra a distancia cuerpo a cuerpo del objetivo realiza `dice: 2d10` de daño físico en su lugar." 
- name: Control del bosque
  desc: "Gasta un punto de miedo para derribar un árbol en rango cercano, cualquier criatura que el árbol golpease en su caída debe superar una tirada de agilidad(15) o recibir `dice: 1d20` de daño físico."
- name: Mimetizarse (reacción)
  desc: "Después de un ataque existoso, marca un punto de estrés para mantenerse oculto hasta su siguiente ataque o que un jugador supere una tirada de instinto(14)." 
```