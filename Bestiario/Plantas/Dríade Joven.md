
```statblock
name: Dríade Joven
layout: Daggerheart Layout
description: Un árbol humanoide, dirigiendo una pequeña fuerza de espíritus del bosque.
damage: [1, 6, 11]
hp: 4
ac: 11
stress: 2
exp: Liderazgo +3
attacks: 
- name: Guadaña
  desc: "Cuerpo a Cuerpo `dice: 1d20` Daño `dice: 2d8+2` (físico)"
moves: 
- name: Voz del Bosque
  desc: "Marca un punto de estrés para activar a `dice: 1d4` aliados a rango de un objetivo que pueda golpear. Sus ataques realizan la mitad de daño."
- name: Prisión de Espinas
  desc: "Gasta un punto de miedo para formar una jaula de ramas espinosas alrededor de una obejtivo a rango muy cercano. El objeto se encuentra apresado, cualquier acción contra la jaula marca un punto de estrés. El objetivo puede liberarse superando una tirada de fuerza(15)."
```