
```statblock
name: Pimpollo
layout: Daggerheart Layout
description: Espíriu del bosque toma la forma de un pequeño árbol con patas
damage: [1, 1, 1]
hp: 1
ac: 10
stress: 1
exp: Amigo de la naturalez +1
attacks: 
- name: Rama pinchuda
  desc: "Cuerpo a Cuerpo `dice: 1d20-2` Daño 4 (físico)"
moves: 
- name: Esbirro (pasiva)
  desc: "Por cada 4 puntos de daño que reciba esta criatura, el atacante puede derrotar a otro esbirro a rango de su ataque."
- name: Ataque de Grupo
  desc: "Selecciona a un objetivo y ativa a todos los Pimpollos (Esbirro) a rango cercano de él. Realizan una tirada de ataque conjunta `dice: 1d20-2` en caso de golpear cada uno realiza 4 puntos de daño físico."
```