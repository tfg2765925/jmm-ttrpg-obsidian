
```statblock
name: Ogro de Cueva
layout: Daggerheart Layout
description: Un descoumnal humnaoide con apetito para la carne humana
damage: [1, 8, 15]
hp: 6
ac: 15
stress: 3
attacks: 
- name: Garrotazo
  desc: "Cuerpo a Cuerpo `dice: 1d20+1` Daño `dice: 2d10` (físico)"
moves: 
- name: Implacable
  desc: "Esta criatura puede activarse hasta 2 veces por ronda, gasta un marcador por cada una."
- name: Quiebrahuesos (pasiva)
  desc: "Cuando un jugador gasta un punto de armadura para reducir del daño, es sólo la mitad de efectivo."
- name: Furia Desenfrenada (reacción)
  desc: "Cuando esta criatura recibe daño severo se mueve a cualquier posición a rango cercano, realizando una tirada de ataque `dice: 1d20-1`. Todas las criaturas sobre las que pase cuya esquiva sea menor reciben `dice: 2d6` de daño físico."
- name: Pedrada (2 activaciones)
  desc: "Marca un punto de estrés para lanzar objetos contra tosdas las criaturas frente al ogro a rango muy lejano. Realiza una tirada de ataque `dice: 1d20` en caso de golpear reciben `dice: 1d20+10` de daño físico."
```