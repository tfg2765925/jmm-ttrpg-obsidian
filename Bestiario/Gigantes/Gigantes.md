# `=this.file.name`
Humanoides cuyo tamaño puede variar en decenas de metros entre especie y especie. Creados por los [[Tiranos]], como una versión mejorada de la humanidad, para enfrentarse alas fuerzas del [[Nonantheón]] pronto empezaron a desarrollar las mismas ideas de libertad que sus predecesores. A mitad de la guerra la principal fuerza gigante había cambiado de bando y la fuerza divina creó un nueva versión, inteligente pero movida por sus instintos y un deseo de carne humana, los Ogros, Trolls y Ojácanos nacieron para reforzar las filas de los dioses. 

La principal especie de gigante habita con otras razas humanoides, aunque son mucho los que prefieren asentamientos gigantes por la comodidad de un hogar a la escala correcta. 

## Enemigos en esta categoría
-[[Ogro de Cueva]]