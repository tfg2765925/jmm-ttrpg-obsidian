
```statblock
name: Constructo-Modelo medio
layout: Daggerheart Layout
description: Autómata mecánico de tamaño estándart equipado con un cañon mágico de alto calibre y un campo anti magia
damage: [1, 17, 34]
hp: 8
ac: 17
stress: 
attacks: 
- name: Cañon Mágico
  desc: "Larga distancia `dice: 1d20+2` Daño `dice: 2d10+8` (mágico)"
moves: 
- name: Armadura Quebradiza
  desc: "Al recibir un golpe crítico la difficultad de esta criatura se reduce en 2"
- name: Escudo Anti-magia
  desc: "Gasta 2 puntos de miedo para activar el escudo, el constructo gana resistencia a todo el daño mágico durante los siguientes 3 turnos."
```