
```statblock
name: Golem de Carne
layout: Daggerheart Layout
description: Amalgama de cuerpos condensada en la forma de un grotescto humanoide, viste con harapos una urna como casco que oculta el núcleo nigromántico que lo alimenta
damage: [1, 8, 15]
hp: 8
ac: 10
stress:
attacks: 
- name: Espadón de Hueso
  desc: "Cuerpo a Cuerpo `dice: 1d20+1` Daño `dice: 3d6` (físico)"
moves: 
- name: Regeneración
  desc: "Al final de la ronda si el Golem no ha recibido daño mágico este recupera 1 punto de vida"
- name: Aversión a la magia
  desc: "Recibir daño mágico deja al golem vulnerable hasta el final de su turno"
- name: Masa de Carne
  desc: "Gasta un punto de miedo para disparar una mole de carne a un objetivo a larga distancia `dice: 1d20+2`. La criatura recibe `dice: 3d8` (físico)."
```