
```statblock
name: Mechadragón
layout: Daggerheart Layout
description: Aeronave con forma de dragon pilotada por un soldado de Enbo, posee todas las cualidades de un dragón pero carece de su gracia.
damage: [1, 17, 34]
hp: 12
ac: 20
stress: 6
attacks: 
- name: Mordisco y Garras
  desc: "Cuerpo a Cuerpo `dice: 1d20+3` Daño `dice: 5d8+11` (físico)"
moves: 
- name: Volador
  desc: "El dragón puede alzar el vuelo como su movimiento."
- name: Aliento de fuego
  desc: "Activa este contador cuando la criatura observa a un enemigo, por cada contador colocado en el rastreador de acciones retira un punto del contador. Cuando se activa todas las criaturas en rango cercano del mechadragón deben superar una tirada de destreza con DC 16 o recibir `dice: 3d10+5` de daño mágico. Las criaturas que superen la tirada deben elegir entre recibir la mitad del daño o marcar 2 puntos de estrés. Éxito crítico permite no recibir daño pero ha de marcarse un punto de estrés."
```