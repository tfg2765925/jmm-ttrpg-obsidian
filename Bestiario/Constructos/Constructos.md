# `=this.file.name`
Son formas de vida artificial normalmente animadas con magia aunque ocasionalmente alimentadas por electricidad o sustancias combustibles. Los constructos vienen en miles de formas y tamaños pero desde su concepción su principal función ha sido el combate, bien para funcionar como un guardián o en el campo de batalla. El grado de consciencia que tienen permite clasificarlos como: Clanks o autómatas, los primeros siendo considerados como una raza viva y los últimos como máquinas siguiendo una programación.

## Enemigos en esta categoría
- [[Constructo-Modelo pequeño]]
- [[Constructo-Modelo medio]]
- [[Constructo-Modelo grande]]
- [[Constructo-Modelo coloso]]
- [[Golem de Carne]]
- [[Mechadragón]]
- [[Protogolem]]