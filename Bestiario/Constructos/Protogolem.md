
```statblock
name: Protogolem
layout: Daggerheart Layout
description: Sirviente mecánico de acero y piedra construido por los ingenieros de la escuela de Unn.
damage: [1, 7, 15]
hp: 9
ac: 13
stress: 4
attacks: 
- name: Guanteletes 
  desc: "Cuerpo a Cuerpo `dice: 1d20+4` Daño `dice: 3d8` (físico)"
moves: 
- name: Defecto de Diseño (pasiva)
  desc: "Todo daño físico que reciba la criatura aumenta en 1 punto de vida."
- name: Sobrecarga (reacción)
  desc: "Gasta un punto de estrés para añadir un +10 de daño a una tirada que realize el protogolem."
- name: Pisotear
  desc: "Cuando esa criatura se activa marca un punto de estrés y realiza una tirada de ataque `dice: 1d20+2`. Todas las criaturas en su camino con esquiva inferior a la tirada reciben `dice: 3d10` de daño físico."
- name: Autodestrucción (reacción)
  desc: "Cuando esta criatura marca su último punto de vida activa su protocolo de autodestrucción, atacando `dice: 1d20+9` a todas las criaturas en rango muy cercano. Aquellas afectadas reciben `dice: 2d12` de daño mágico."
```