
```statblock
name: Constructo-Modelo grande
layout: Daggerheart Layout
description: Autómata mecánico gran envergadura, creados por las mentes más brillantes de Marquis. Este constructo es prácticamente indestructible, está equipado con guanteletes eléctircos y la última tecnología de auto-reparación.
damage: [1, 30, 48]
hp: 15
ac: 20
stress: 
attacks: 
- name: Guanteletes Eléctricos
  desc: "Cercano `dice: 1d20+3` Daño `dice: 3d10+8` (mágico)"
moves: 
- name: Gran Tamaño
  desc: "El constructo puede agarrar a dos criaturas medias al mismo tiempo."
- name: Armadura Quebradiza
  desc: "Al recibir un golpe crítico la difficultad de esta criatura se reduce en 2"
- name: Nanobots
  desc: "Gasta 2 puntos de miedo para reparar todos los daños recibidos, si el robot no se ve interrumpido hasta siguiente ronda."
```