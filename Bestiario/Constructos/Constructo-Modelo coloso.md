
```statblock
name: Constructo-Modelo coloso
layout: Daggerheart Layout
description: Proyecto secreto desarrollado por Okya y sus ingenieros. Este descomnula constructo alcanza los 50 metros de altura y actúa como una fortaleza móvil para la el Instituo.
damage: [15, 40, 80]
hp: 20
ac: 22
stress: 
attacks: 
- name: Aplastar
  desc: "Larga distancia `dice: 1d20+4` Daño `dice: 5d10+8` (físico)"
moves: 
- name: Coloso
  desc: "El constructo es considerado una estructura para efectos de movimientos y habilidades."
- name: Armadura Quebradiza
  desc: "Al recibir un golpe crítico la difficultad de esta criatura se reduce en 2"
- name: Rayo de la Muerte
  desc: "Gasta 2 puntos de miedo para realizar un ataque a distancia lejana `dice: 1d20+10`, afectando a todas las criaturas a rango cercano de las explosión que reciben `dice: 6d6` (fuego)."
```