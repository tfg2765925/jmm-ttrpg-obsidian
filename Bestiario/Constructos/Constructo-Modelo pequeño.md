
```statblock
name: Constructo-Modelo pequeño
layout: Daggerheart Layout
description: Pequeño sirviente mecánico, lo que carece en resistencia e intimidación lo compensa con agilidad y capacidades de camuflaje
damage: [1, 10, 19]
hp: 6
ac: 16
stress: 
attacks: 
- name: Daga Secreta
  desc: "Cuerpo a Cuerpo `dice: 1d20+1` Daño `dice: 3d6+3` (físico)"
moves: 
- name: Armadura Quebradiza
  desc: "Al recibir un golpe crítico la difficultad de esta criatura se reduce en 2"
- name: Camuflaje
  desc: "Gasta 1 punto de miedo para activar el modo sigilo, todas las criaturas han de superar una tirada de percepción DC 18 o desconocer el paradero del cosntructo. Aquellas que la superen sólo tienen una idea aproximada de su ubicación."
```