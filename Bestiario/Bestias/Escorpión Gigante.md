
```statblock
name: Escorpión Gigante
layout: Daggerheart Layout
description: Un insecto del tamaño de un hombre adulto con intimidantes pinzas y un letal aguijón.
damage: [1, 7, 13]
hp: 6
ac: 13
stress: 3
exp: Camuflaje +3
attacks: 
- name: Pinzas
  desc: "Cuerpo a Cuerpo `dice: 1d20+2` Daño `dice: 2d8` (físico)"
moves: 
- name: Doble Golpe
  desc: "Marca un punto de estrés para realizar un ataque de pinzas contra dos objetivos a rango cuerpo a cuerpo"
- name: Aguijón Venenoso
  desc: "Realiza un ataque contra un objetivo a rango muy cercano `dice: 1d20+2`. En caso de éxito reciben `dice: 3d8` de daño físico, puedes gastar un punto de miedo para envenenar al obejtivo. Mientras estén envenenados deben realizar tirar un d6 antes de realizar cualquier acción, en caso de obtener un 4 o menos marcan un punto de esrtrés. El estado de envenenado desaparece tras un descanso corto o superar una tirada de conocimiento(16)."
```