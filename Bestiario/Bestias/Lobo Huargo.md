
```statblock
name: Lobo Huargo
layout: Daggerheart Layout
description: Gran bestia de pelaje gris, siempre acompañado por su manada
damage: [1, 5, 9]
hp: 4
ac: 12
stress: 2
exp: Sentidos Agudos +3
attacks: 
- name: Mordisco
  desc: "Cuerpo a Cuerpo `dice: 1d20+2` Daño `dice: 2d6` (físico)"
moves: 
- name: Tácticas de Equipo
  desc: "Cuando el lobo golpea con su mordisco y otro lobo se encuentra a distancia cuerpo a cuerpo del objetivo realiza `dice: 2d10` de daño físico en su lugar." 
- name: Ataque desgarrador
  desc: "Marca un punto de estrés para realizar un ataque cuerpo a cuerpo contra un objetivo `dice: 1d20`. En caso de golpear la criatura recibe `dice: 4d6` de daño físico y se considera vulnerable hasta que recuperen vida."
```