
```statblock
name: Lobuza
layout: Daggerheart Layout
description: Quimera con cabeza y alas de lechuza y cuerpo de lobo, pueden llegar a medir hasta 3 metros de altura y son hábiles rastreadores.
damage: [1, 4, 8]
hp: 3
ac: 10
stress: 0
attacks: 
- name: Mordisco
  desc: "Cuerpo a Cuerpo `dice: 1d20+1` Daño `dice: 1d6+3` (físico)"
moves: 
```