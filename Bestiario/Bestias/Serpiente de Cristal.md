
```statblock
name: Serpiente de Cristal
layout: Daggerheart Layout
description: Anaconda de escamas transparentes que deja un reguero de critales a su paso.
damage: [1, 6, 10]
hp: 5
ac: 14
stress: 3
exp: 
attacks: 
- name: Colmillos
  desc: "Muy cercano `dice: 1d20+1` Daño `dice: 1d8+3` (físico)"
moves: 
- name: Escamas de fragmentación (pasiva)
  desc: "Cualquier ataque exitoso contra la serpiente marca un punto de armadura del atacante."
- name: Torbellino de cristal
  desc: "Realiza una tirada de ataque contra todas las criaturas a rango muy cercano. Cualquier criatura golpeada recibe `dice: 1d8+3` puntos de daño físico."
- name: Projectiles
  desc: "Gasta un punto de miedo para invocar un `dice: 1d6`. Cuando se activa la serpeinte tira este dado, en un 5 o más todas las criaturas frente a la sertiente en rango lejano, deben superar una tirada de agilidad(14) o recibir `dice: 1d10+4` de daño físico-"
```