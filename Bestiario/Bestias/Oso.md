
```statblock
name: Oso
layout: Daggerheart Layout
description: Gran bestia cce pelaje marrón y poderosas garras
damage: [1, 9, 17]
hp: 7
ac: 14
stress: 2
exp: Sentidos agudos +2, Depredador +3
attacks: 
- name: Garras
  desc: "Cuerpo a Cuerpo `dice: 1d20+1` Daño `dice: 2d10` (físico)"
moves: 
- name: Fuerza Abrumadora (pasiva)
  desc: "Criaturas que marcan puntos de vida tras un ataque del oso son empujados a rango cercano."
- name: Mordiso
  desc: "Marca un punto de estrés para realiza run ataque cuerpo a cuerpo. En caso de éxito el objetido recibe `dice: 3d8` de daño físico y se encuentra atrapado y ha de superar una tirada de fuerza(14) para liberarse."
```