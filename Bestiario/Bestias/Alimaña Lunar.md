
```statblock
name: Alimaña Lunar
layout: Daggerheart Layout
description: Una rata corriente, obtiene una extraordinaria inteligencia con la luna llena.
damage: [1, 1, 1]
exp: Diminuto +4
hp: 1
ac: 14
stress: 1
attacks: 
- name: Moriscos
  desc: "Cuerpo a Cuerpo `dice: 1d20-3` Daño `dice: 2d10` (físico)"
moves: 
- name: Escabullirse (pasiva)
  desc: "La alimaña lunar tiene ventaja para realizar tiradas para esconderse"
- name: Inteligencia Lunar
  desc: "Si la luna está llena la alimaña lunar puede invocar un proyectil mágico `dice: 1d20+5` en rango lejano que afecta hasta a dos criaturas  a rango cercano entre sí que inflingiendo `dice: 3d8` de daño mágico."
```