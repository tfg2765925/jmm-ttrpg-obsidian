# `=this.file.name`
Bestias es el nombre que reciben todas la criaturas animales de [[Marquis]]. Comúnmente encontradas en la superficie algunas se han abierto paso a las mazmorras a pesar de no ser nativas de las mismas. Este fenómeno ha llevado a la deducción de que sólo las criaturas con algún trazo de magia pueden ser generadas por las mazmorras. Las bestias son puramente biológicas, aunque esta categoría engloba a licántropos transformados.

## Enemigos en esta categoría
- [[Alimaña Lunar]]
- [[Escorpión Gigante]]
- [[Gusano Gigante]]
- [[Hurón Gigante]]
- [[Lobo Huargo]]
- [[Lobuza]]
- [[Oso]]
- [[Rata Gigante]]
- [[Rey de las Ratas]]
- [[Serpiente de Cristal]]
- [[Serpiente Gigante]]