
```statblock
name: Rey de las Ratas
layout: Daggerheart Layout
description: Una espeluznante masa de rodedores que avanza como un único organismo.
damage: [1, 6, 10]
hp: 6
ac: 10
stress: 3
attacks: 
- name: Moriscos
  desc: "Cuerpo a Cuerpo `dice: 1d20-3` Daño `dice: 2d10` (físico)"
moves: 
- name: En tu cara
  desc: "Todas los enemigos a rango cercano del rey tienen desventaja contra cualquier otra criatura que no sea el Rey de las Ratas"
- name: Horda
  desc: "Cuando se marcan 4 puntos de vida del rey, sus ataques pasan a realizar `dice: 2d4`"
```