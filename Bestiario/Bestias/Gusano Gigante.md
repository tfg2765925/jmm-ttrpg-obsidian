
```statblock
name: Gusano Gigante
layout: Daggerheart Layout
description: Insecto del tamaño de un caballo protegido por un exoesqueleto marrón, depredador de emboscada que  ataca a sus presas desde las profundidades de la arena.
damage: [1, 8, 15]
hp: 7
ac: 14
stress: 3
exp: Sentido Sísmico +3
attacks: 
- name: 
  desc: "Muy Cerca `dice: 1d20+3` Daño `dice: 1d12+3` (físico)"
moves: 
- name: Implacable
  desc: "Esta criatura puede activarse hasta 2 veces por ronda, gasta un marcador por cada una."
- name: Erupción
  desc: "Marca un punto de estrés para que la criatura emerga de la tierra, derribando a todas las criaturas que no superen una tirada de agilidad(14), y dejandolas vulnerables hasta que actúen."
- name: Escupir Ácido
  desc: "Realiza una tirada de ataque `dice: 1d20+3` contra todas la criaturas frente al gusano a rango cercano. En caso de golpear la criaturas reciben `dice: 1d20` de daño físico y su armadura se reduce permanentemente a la mitad hasta que la reparen."
- name: Sangre Acídica (reacción)
  desc: "Cuando el gusano recibe daño severo todas las criaturas en rango cuerpo a cuerpo reciben `dice: 2d10` de daño físico. El suelo en este rango se llena de ácido y atravesarlo imflinge `dice: 1d6` de daño físico."
```