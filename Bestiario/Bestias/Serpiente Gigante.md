
```statblock
name: Serpiente Gigante
layout: Daggerheart Layout
description: Sierpe descomunal que habita en las profundidades de la selva de contienentes lejanos. Algunas especies se han adaptado a la vida en las Ciudades Perdidas
damage: [1, 16, 25]
hp: 15
ac: 12
stress: 6
attacks: 
- name: Mordisco
  desc: "Cercano `dice: 1d20+3` Daño `dice: 3d6` (físico) + `dice: 2d4` (mágico)."
moves: 
- name: Serpentear
  desc: "Una vez por ronda puede marcar un punto de estrés para moverse en cualquier dirección cercana si ha recibidos daños."
- name: Veneno de Serpiente
  desc: "Gasta 1 punto de miedo para envenear a una criatura afectada previamente por un mordisco. El objetivo debe superar una tirada de fuerza DC 16 o estar vulnerable durante `dice: 1d6+1` turnos."
- name: Engullir
  desc: "Gasta 2 puntos de miedo para realizar un ataque `dice 1d20+6` contra una criatura cercana. En caso de golpear la criatura que atrapada en el interior de la serpiente y recibe `dice: 1d6+2` de daño de físico al final de cada uno de sus turnos."
```