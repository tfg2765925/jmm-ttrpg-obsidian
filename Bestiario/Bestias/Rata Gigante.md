
```statblock
name: Rata Gigante
layout: Daggerheart Layout
description: Mosntruosa rata del tamaño de un gato doméstico, cubierta de repulsivas pustulas.
damage: [1, 1, 1]
hp: 1
ac: 10
stress: 1
exp: Sentidos Agudos +3
attacks: 
- name: Garras
  desc: "Cuerpo a Cuerpo `dice: 1d20-4` Daño `dice: 2d10` (físico)"
moves: 
- name: Esbirro (pasiva)
  desc: "Por cada 3 puntos de daño que reciba esta criatura, un jugador puede derrotar a otro esbirro a rango."
- name: Ataque de Grupo
  desc: "Selecciona a un objetivo y ativa a todos las Ratas Gigantes rango cercano de él. Realizan una tirada de ataque conjunta `dice: 1d20-4` en caso de golpear cada una realiza 3 puntos de daño."
```