
```statblock
name: Hurón Gigante
layout: Daggerheart Layout
description: Bestia descomunal que se asemeja a un hurón desproporcionado, extremadamente territorial y agresivo.
damage: [1, 12, 24]
hp: 9
ac: 14
stress: 9
attacks: 
- name: Mordisco
  desc: "Cuerpo a Cuerpo `dice: 1d20+3` Daño `dice: 3d6+6` (físico)"
moves: 
- name: Fuerza Abrumadora (pasiva)
  desc: "Criaturas que marcan puntos de vida tras un ataque del oso son empujados a rango cercano."
- name: Implacable
  desc: "Esta criatura puede activarse hasta 2 veces por ronda, gasta un marcador por cada una."
- name: Frenesí
  desc: "Gasta 1 punto de miedo para agarrar a una criatura que fuese atacada previamente por el Hurón. Ha de superar una tirada de Destreza con DC 16 o recibir `dice: 1d20+6` de daño físico"
```