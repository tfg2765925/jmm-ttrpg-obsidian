# `=this.file.name`
Cuando se produce la muerte se separa el alma del cuerpo. Cuando el alma es incapaz de disiparse se queda estancada en el mudo físico, creado un espíritu o aparición. No todos los espíritus son el alma de una criatura viva, algunos son habitantes de planos distintos al plano material y al acercarse a nuestro mundo abandonan su previa forma física por una incorpórea.

Otros espíritus son simplemente concentraciones de mana con conciencia, este tipo de espíritus se manifiesta en mazmorras artificiales. Estos espíritus de mana puede ser invocados mediante magia por un conjuro avanzado.

## Enemigos en esta categoría
- [[Espectro de Escarcha]]
- [[Espíritu Menor]]
- [[Fuegos Fatuos]]