
```statblock
name: Espíritu Menor
layout: Daggerheart Layout
description: Una amalgama de magia con forma  de esfera translúcida, Son creados por la presencia de magia descontrolada o por hechiceros experimentados concentrando su maná.
damage: [1, 6, 12]
hp: 4
ac: 12
attacks: 
- name: Drenar Energía
  desc: "Rango lejano `dice: 1d20+3` Daño `dice: 1d10+2` (mágico)."
moves: 
- name: Incorpóreo
  desc: "Los espíritus tienen resistencia al daño físico."
- name: Nostalgia Paralizante
  desc: "Gasta un punto de miedo, el fuego fatuo se lanza hacia un objetivo a rango cercano y le obliga a recordar una experiencia traumática. Reciben `dice: 2d10` de daño magico y quedan vulnerables hasta el fin de su turno."
```