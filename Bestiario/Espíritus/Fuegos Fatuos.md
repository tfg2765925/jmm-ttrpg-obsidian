
```statblock
name: Fuegos Fatuos
layout: Daggerheart Layout
description: Una concentración de espíritus menores que toma la forma de una llama espectral de diversos colores.
damage: [1, 10, 20]
hp: 6
ac: 13
attacks: 
- name: Drenar Energía
  desc: "Rango lejano `dice: 1d20+3` Daño `dice: 1d10+4` (mágico)."
moves: 
- name: Incorpóreo
  desc: "Los fuegos fatuos tienen resistencia al daño físico."
- name: Nostalgia Paralizante
  desc: "Gasta un punto de miedo, el fuego fatuo se lanza hacia un objetivo a rango cercano y le obliga a recordar una experiencia traumática. Reciben `dice: 2d10` de daño magico y quedan vulnerables hasta el fin de su turno."
- name: Horda
  desc: "Cuando se marcan 3 puntos de vida de la criatura, sus ataques pasan a realizar `dice: 2d4`"
- name: Atravesar
  desc: "Gasta 2 puntos de miedo y realiza un ataque contra un objetivo cuerpo a cuerpo. En caso de golpear el Fuego Fatuo lo atraviesa separando su alma de su cuerpo impidiendo que tome acciones durante un turno."
```