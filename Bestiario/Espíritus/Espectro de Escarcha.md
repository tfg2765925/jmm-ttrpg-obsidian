
```statblock
name: Espectros de Escarcha
layout: Daggerheart Layout
description: Almas heladas engendradas por la Daga del Invierno, normalmente de tamañano minúsculos se requiere una congregación para suponer una amenaza.
damage: [1, 8, 17]
hp: 5
ac: 14
attacks: 
- name: Drenar Energía
  desc: "Rango lejano `dice: 1d20+2` Daño `dice: 2d10+4` (heilo)."
moves: 
- name: Incorpóreo
  desc: "Los espectros tienen resistencia al daño físico."
- name: Debilidad a las llamas
  desc: "Los espectros tienen debilidad al daño de fuego."
- name: Atravesar
  desc: "Gasta 2 puntos de mieod y realiza un ataque contra un objetivo cuerpo a cuerpo. En caso de golpear el epectro lo atraviesa separando su alma de su cuerpo impidiendo que tome acciones durante un turno."
```