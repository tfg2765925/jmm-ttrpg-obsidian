
```statblock
name: Mago
layout: Daggerheart Layout
description: Mago principiante, a pesar de su juventud blande maldiciones ofensivas con gran maestría. 
damage: [1, 5, 9]
hp: 4
ac: 13
stress: 3
attacks: 
- name: Bastón
  desc: "Larga distancia `dice: 1d20+2` Daño `dice: 1d10` (mágico)"
moves: 
- name: Maldición
  desc: "Gasta 1 punto de miedo para lanzar una maldición contra un objetivo a larga distancia. El objetivo se encuentra maldito hasta que el mago muera o marque un estrés para terminar esta condición. Los objetivos malditos se consideran vulnerables y cualquier tirada que realizen con esperanza se considera con miedo hasta que finalize el efecto."
- name: Arco Caótico
  desc: "Realiza una tirada de ataque contra 3 objetivos que se enceuntren a rango cuerpo a cuerpo entre sí. En caso de golpear marca un estrés y reciben `dice: 2d6` de daño mágico cada uno"
```