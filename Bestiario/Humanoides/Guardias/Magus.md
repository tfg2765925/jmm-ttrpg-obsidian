
```statblock
name: Magus
layout: Daggerheart Layout
description: Líder de una banda de mercenarios que combina sus habilidades físicas con mágica arcana.
damage: [1, 8, 15]
hp: 6
ac: 14
stress: 3
exp: Conocimientos mágicos +2
attacks: 
- name: Hoja Encantada
  desc: "Muy cercano `dice: 1d20+3` Daño `dice: 2d8+2` (físico y mágico)"
moves:
- name: Estratega
  desc: "Gasta 1 punto de estrés para activar a esta criatrua y a dos aliados en rango cercano."
- name: Arco Eléctrico
  desc: "Gasta un punto de miedo para selecionar hasta a 3 objetivos que se encuentren agrupados en rango cercano, reciben `dice: 2d12` de daño mágico. Si de superar una tirada de agilidad(13), reciben la mitad. Por cada criatura que marque puntos de daño se añaden un marcador al rastreador de combate."
```