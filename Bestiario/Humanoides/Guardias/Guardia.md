
```statblock
name: Guardia
image: img/Guardia.png
layout: Daggerheart Layout
description: Militar entrenado y ataviado con la equipación estándart de infantería. 
damage: [1, 6, 12]
hp: 5
ac: 12
stress: 0
attacks: 
- name: Espada Larga
  desc: "Cuerpo a Cuerpo `dice: 1d20` Daño `dice: 2d6` (físico)"
moves: 
- name: Muro de Escudos
  desc: "La dificultad para atravesar a este enemigo aumenta en 1 por cada guardia a rango cuerpo a cuerpo del original. Este efecto se acumula si los guardias adyacentes tienen otros guardias en rango cuerpo a cuerpo de ellos mismos."
- name: Detener
  desc: "Gasta un estrés para realizar un ataque a un enemigo cuerpo a cuerpo. En caso de golpear el objetivo se encuentra atrapado y no puede liberarse a menos que se realize una tirada de ataque fructífera contra el guardia o se supere una tirada de fuerza o destreza con DC 12."
```