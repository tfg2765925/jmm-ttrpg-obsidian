
```statblock
name: Mastero de Batalla
image: img/Guardia.png
layout: Daggerheart Layout
description: Mercenario curtido en mil batallas tras un vida de combate.
damage: [1, 8, 15]
hp: 6
ac: 14
stress: 3
exp: 
attacks: 
- name: Espadón
  desc: "Muy cercano `dice: 1d20+2` Daño `dice: 2d8+2` (físico)"
moves: 
- name: Ataque con señuelo
  desc: "Gasta un punto de estrés para realizar un ataque de espadón y lanzar una pulla contra el objetivo. En su sigueinte ataque tiene desventaja contra cualquier criatrua que no sea el Maestro."
- name: ASegundo aliento (1 vez por escena)
  desc: "Gasta un punto de miendo para recuperar 2 puntos de vida y dos puntos de estrés."
```
