
```statblock
name: Halconero
image: img/Guardia.png
layout: Daggerheart Layout
description: Ágil luchador armado con un arsenal de javalinas
damage: [1, 5, 9]
hp: 3
ac: 12
stress: 3
exp: Camuflaje +2
attacks: 
- name: Javalina
  desc: "Cercano `dice: 1d20+1` Daño `dice: 1d8+2` (físico)"
moves: 
- name: Mantener las distancias (pasiva)
  desc: "Cuando el halconero realiza una tirada de ataque exitosa, puede moverse a rango lejano."
- name: Retroceder (reacción)
  desc: "Cuando un enemigo se acerca a rango cuerpo a cuerpo, el halconero puede marcar un punto de estrés para moverse a cualquier casilla en rango cercano y realizar un ataque de javalina."
```