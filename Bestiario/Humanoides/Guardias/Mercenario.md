
```statblock
name: Mercenario
image: img/Guardia.png
layout: Daggerheart Layout
description: Un intrépido cazafortunas probando su suerte
damage: [1, 1, 1]
hp: 1
ac: 10
stress: 1
attacks: 
- name: Espada larga
  desc: "Cuerpo a Cuerpo `dice: 1d20` Daño 4 (físico)"
moves: 
- name: Esbirro (pasiva)
  desc: "Por cada 4 puntos de daño que reciba esta criatura, el atacante puede derrotar a otro esbirro a rango de su ataque."
- name: Ataque de Grupo
  desc: "Selecciona a un objetivo y ativa a todos los Bandidos (Esbirro) a rango cercano de él. Realizan una tirada de ataque conjunta `dice: 1d20-2` en caso de golpear cada uno realiza `dice: 1d8+2` puntos de daño físico."
```