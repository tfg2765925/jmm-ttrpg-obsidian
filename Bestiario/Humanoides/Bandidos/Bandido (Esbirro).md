
```statblock
name: Bandido (Esbirro)
image: img/Bandido.png
layout: Daggerheart Layout
description: Criminal de poca monta equipado con una daga roñosa y mucha confianza en su superioridad numérica.
damage: [1, 1, 1]
hp: 1
ac: 9
stress: 1
exp: Bandido +2
attacks: 
- name: Cuchillo
  desc: "Cuerpo a Cuerpo `dice: 1d20-3` Daño 3 (físico)"
moves: 
- name: Esbirro (pasiva)
  desc: "Por cada 3 puntos de daño que reciba esta criatura, el atacante puede derrotar a otro esbirro a rango de su ataque."
- name: Ataque de Grupo
  desc: "Selecciona a un objetivo y ativa a todos los Bandidos (Esbirro) a rango cercano de él. Realizan una tirada de ataque conjunta `dice: 1d20-2` en caso de golpear cada uno realiza 3 puntos de daño físico."
```