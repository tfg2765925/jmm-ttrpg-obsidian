
```statblock
name: Bandido (Sombra)
image: img/Bandido.png
layout: Daggerheart Layout
description: Criminal equipado con una daga dentada y versado en magia sombría.
damage: [1, 4, 8]
hp: 3
ac: 12
stress: 3
exp: Allanar +3
attacks: 
- name: Cuchillo
  desc: "Cuerpo a Cuerpo `dice: 1d20+1` Daño `dice: 2d6` (físico)"
moves: 
- name: Por la espalda (pasiva)
  desc: "Cuando un ataque con ventaja golpea realiza un `dice: 2d10` de daño físico en vez del habitual."
- name: Desaparición
  desc: "El adversario se desvanece hasta temrinar su siguiente acción. El bandido se considera oculto durante este tiempo."
```