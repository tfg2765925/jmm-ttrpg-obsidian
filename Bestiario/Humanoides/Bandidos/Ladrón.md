
```statblock
name: Ladrón
image: img/Bandido.png
layout: Daggerheart Layout
description: Experto del sigilo y familiar con el arte del engaño, estos individuos prefieren mantener las distancias y obtener su botín de forma incospicua, pero no dudarán en recurrir a la violencia si se ven acorralados. 
damage: [1, 10, 15]
hp: 3
ac: 14
stress: 0
attacks: 
- name: Daga serrada
  desc: "Cuerpo a Cuerpo `dice: 1d20+3` Daño `dice: 3d6` (físico)"
moves: 
- name: Apártate
  desc: "Gasta 1 punto de miedo para realizar una tirada de ataque `dice: 1d20+3` contra una criatura a rango cercano, en caso de golpear reciben `dice: 2d8` de daño mágico y salen despedidos a rango lejano."
```