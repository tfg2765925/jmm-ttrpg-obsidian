
```statblock
name: Bruto
image: img/Bandido.png
layout: Daggerheart Layout
description: Una imponente mole de carne equipada con un garrote.
damage: [1, 7, 14]
hp: 7
ac: 12
stress: 4
exp: Bandido +2, Amenazas +3
attacks: 
- name: Garrote
  desc: "Cuerpo a Cuerpo `dice: 1d20-3` Daño `dice: 2d6+2` (físico)"
moves: 
- name: Yo te lo sujeto (pasiva)
  desc: "Criaturas atrapadas por el Bruto reciben el doble de daño."
- name: Ven aquí
  desc: "Realiza una tirada de ataque contra un objetivo, en caso de éxito se encuentra atrapada y vulnerable. El objetivo puede liberarse superando una tirada de fuerza(12), o el bruto recibe daño severo."
```