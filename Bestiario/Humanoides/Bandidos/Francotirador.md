
```statblock
name: Francotirador
image: img/Bandido.png
layout: Daggerheart Layout
description: Bandido, diestro con el arco que ataca desde la distancia con poca precisión.
damage: [1, 4, 7]
hp: 3
ac: 13
stress: 2
exp: Sigilo +3
attacks: 
- name: Arco
  desc: "Lejano `dice: 1d20-1` Daño `dice: 2d6` (físico)"
moves: 
- name: Ataque sorpresa (pasiva)
  desc: "Los ataques del francotirador mientras se encuentra oculto realizan un `dice: 1d20` de daño físico en vez del habitual."
```