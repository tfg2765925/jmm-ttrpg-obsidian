
```statblock
name: Bandido (Líder)
image: img/Bandido.png
layout: Daggerheart Layout
description: Criminal equipado con una daga dentada y versado en magia sombría.
damage: [1, 7, 14]
hp: 6
ac: 13
stress: 3
exp: Conocimeinto del terreno +2
attacks: 
- name: Javalina
  desc: "Cercano `dice: 1d20+2` Daño `dice: 1d8+2` (físico)"
moves: 
- name: Estratega
  desc: "Gasta 1 punto de estrés para activar a esta criatrua y a dos aliados en rango cercano."
- name: Refuerzos
  desc: "Invoca hasta a 3 Bandido (Esbirro)."
- name: Golpe de gracia
  desc: "Grasta un punto de miedo para realiza run ataque contra una criautra vulnerable en rango cercano `dice: 1d20+4`. Si es exitoso, realiza `dice: 4d8` de daño físico y marca 1 punto de estrés."
```