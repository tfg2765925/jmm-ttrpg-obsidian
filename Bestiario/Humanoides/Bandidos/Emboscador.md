
```statblock
name: Acechador
image: img/Bandido.png
layout: Daggerheart Layout
description: Un forajido normalmente perteneciente a una banda de indeseables liderados por un embaucador. Sirven de distracción mientras su lider roba a las víctimas.
damage: [1, 6, 10]
hp: 3
ac: 14
stress: 0
attacks: 
- name: Daga serrada
  desc: "Cuerpo a Cuerpo `dice: 1d20+1` Daño `dice: 2d8+1` (físico)"
moves: 
- name: Emboscada
  desc: "Si la criatura no ha sido detectada antes de entrar a al escena, el emboscador ataca antes. En su primer turno sus ataques realizan `dice: 2d8+4` puntos de daño físico"
```