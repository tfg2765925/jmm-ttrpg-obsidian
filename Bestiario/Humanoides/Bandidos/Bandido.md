
```statblock
name: Bandido
image: img/Bandido.png
layout: Daggerheart Layout
description: Criminal experimentado equipado con una daga robada y preferencia por las emboscadas.
damage: [1, 8, 14]
hp: 5
ac: 12
stress: 3
exp: Bandido +2
attacks: 
- name: Cuchillo
  desc: "Cuerpo a Cuerpo `dice: 1d20+1` Daño `dice: 2d6` (físico)"
moves: 
- name: Escalador (pasiva)
  desc: "El bandido tiene tanto movimiento vertical como horizontal."
- name: Muerte desde arriba
  desc: "Cuando un bandido ataca desde una posición elevada y golpea realiza `dice: 3d10` de daño físico en lugar del daño normal."
```