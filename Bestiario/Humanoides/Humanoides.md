# `=this.file.name`
Las criaturas bípedas con suficiente inteligencia para fabricar herramientas y comunicarse mediante un lenguaje común se clasifican como humanoides. No es una categoría estricta ya que diversos monstruos han demostrado ser capaces de mostrar estas características pero son categorizadas de forma distinta. Normalmente se denomina a una criatura como humanoide cuando no existe otra categoría que la describa mejor. Este motivo y discriminación hacia las razas menos "civilizadas" es el motivo por el cuál los Diablos y [[Goblins]] no son considerados humanoides.

## Enemigos en esta categoría
- [[Bandido]]
- [[Guardia]]
- [[Mago]]
- [[Invocador]]