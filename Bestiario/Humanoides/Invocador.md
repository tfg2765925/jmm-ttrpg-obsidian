
```statblock
name: Invocador
layout: Daggerheart Layout
description: Mago especializado en la creación de familiares capaz de convocar criaturas para ayudarse tanto fuera como dentro del combate
damage: [1, 7, 15]
hp: 5
ac: 13
stress: 4
attacks: 
- name: Varita relámpago
  desc: "Larga distancia `dice: 1d20+2` Daño `dice: 2d10+2` (mágico)"
moves: 
- name: Ordenar Creaciones
  desc: "Marca un punto de estrés para activar a `dice: 1d4+1` aliados a rango de un objetivo que pueda golpear. Sus ataques realizan la mitad de daño."
- name: Retraer criatura (2 veces por escena)
  desc: "Si el invocador se encuentra en distancia cercana a una criatura convocada puede destruirla para recuperar 1 punto de vida"
- name: Convocar Diablillo
  desc: "Gasta 1 punto de miedo para invocar `dice: 1d4` diablillos."
- name: Convocar Espíritu
  desc: "Gasta 1 punto de miedo para invocar un `dice: 1d4` espíritus menores."
- name: Convocar Constructo"
  desc: "Gasta 2 puntos de miedo para invocar un Constructo-modelo pequeño con 2 puntos de vida."
```