
```statblock
name: Hidra
layout: Daggerheart Layout
description: Reptil cuadrúpedo policéfalo, de su cuerpo emergen una sinfín de cabezas de serpientes.
damage: [1, 19, 37]
hp: 7
ac: 18
stress: 5
exp: Sentidos Agudos +3
attacks: 
- name: Mordisco
  desc: "Cercano `dice: 1d20+3` Daño `dice: 3d10` (físico)"
moves: 
- name: Amenaza Policéfala (pasiva)
  desc: "La hidra comienza con 3 cabezas, cuando recibe daño severo o grave pierde una."
- name: Implacable
  desc: "Esta criatura puede activarse un número de veces igual a las cabezas que tenga por ronda, gasta un marcador por cada una."
- name: Regeneración
  desc: "Si la hidra ha perdido algún punto de vida, gasta un punto de miedo para recuperar 2HP y regenerar una cabeza."
- name: Orfeón Escalofriante (2 Acciones)
  desc: "Todas las criaturas a rango cercano de la hidra pierden dos de esperanza."
- name: Debilidad Mágica (pasiva)
  desc: "Cuando la hidra recibe daño mágico se encuentra aturdida. Mientras está artudida no puede regenerar cabezas pero es inmune al daño mágico. Siempre que la hidra se active mientras se encuentra aturdida tira un `dice: 1d8` de un 7 o más deja de estar aturdida."
```