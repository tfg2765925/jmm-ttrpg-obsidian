
```statblock
name: Dragón de Hierro Anciano
image: img/Dragon-metalico.png
layout: Daggerheart Layout
description: Colosal dragón de hasta 40 metros de alto cubierto en escamas negras brillantes, 3 cuernos se alzan tras su cabeza formando una corona natural
damage: [1, 40, 71]
hp: 15
ac: 20
stress: 9
attacks: 
- name: Garras
  desc: "Cercano `dice: 1d20+7` Daño `dice: 10d6+5` (físico)"
moves: 
- name: Incansable (3)
  desc: "Se puede activar hasta 3 veces en un mismo turno"
- name: Aterrador
  desc: "Cualquier criatura que tire con miedo a rango cercano del dragón pierde una esperanza."
- name: Aliento de Sueño
  desc: "Toda las criaturas en frente del dragón en distancia corta deben superar una tirada de agilidad(18). En caso de fallo deben tirar un d4, ese es el número de rondas que se mantendrán despiertos antes de caer inconscientes. Este ataque elimina todas las activaciones del dragón."
- name: Aliento de Chispas
  desc: "Toda las criaturas en frente del dragón en distancia corta deben superar una tirada de agilidad(18). En caso de fallo reciben `dice: 3d20` de daño mágico, además se encuentran en llamas, al final de cada turno reciben `dice: 2d6` de daño mágico."
- name: Carne a Metal
  desc: "Gasta un punto de miedo una criatura a rango cercano del dragón debe supera runa tirada de instinto(16), o transformarse en una estatua de hierro. Al final de cada uno de sus turnos puede tratar de repetir la tirada si fallase 3 veces la transformación es permanente"
```