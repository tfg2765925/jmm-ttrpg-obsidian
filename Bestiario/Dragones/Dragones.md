# `=this.file.name`
Reptiles de gran tamaño creados por las divinidades de ambos bandos para servir como generales durante la Guerra. Con la intención de evitar traiciones ambos bandos inculcaron un odio innato entre sí. Originalmente tan sólo existían dragones elementales y metálicos, pero el paso de los años las necesidades de la guerra y las mutaciones con cada generación crearon un espectro de nuevos dragones y pseudodragones.

Actualmente el término metálico y elemental sólo se emplea para agrupar a las diferentes subespecies de dragón.

## Enemigos en esta categoría
- [[Dragón de Hielo Juvenil]]
- [[Dragón de Hierro Anciano]]
- [[Dragón Hechizado Juvenil]]
- [[Hidra]]