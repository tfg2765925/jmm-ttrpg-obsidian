
```statblock
name: Dragón Hechizado Juvenil
layout: Daggerheart Layout
description: Criatura dracónica transformada por un oscuro ritual mágico. Un maléfico ojo naranja ha emergido de su frente y sus escamas han perdido su color y robustez.
damage: [1, 14, 22]
hp: 9
ac: 15
stress: 7
attacks: 
- name: Garras
  desc: "Cuerpo a Cuerpo `dice: 1d20+1` Daño `dice: 2d8+4` (físico)"
moves: 
- name: Incansable (2)
  desc: "Se puede activar hasta 2 veces en un mismo turno"
- name: Aterrador
  desc: "Cualquier criatura que tire con miedo a rango cercano del dragón pierde una esperanza."
- name: Aliento Temporal
  desc: "Toda las criaturas en frente del dragón en distancia corta deben superar una tirada de agilidad(12). En caso de fallo deben tirar un d20, ese este es el número de años de vida que envejecen. Este ataque elimina todas las activaciones del dragón."
- name: Eliminar conocimiento
  desc: "Gasta un punto de miedo una criatura a rango cercano del dragón debe supera runa tirada de instinto(12), la criatura recibe `dice: 3d6` de daño, la mitad en caso de superar la tirada mágico y además olvida un recuerdo importante de forma permanente."
```