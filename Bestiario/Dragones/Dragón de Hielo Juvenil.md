
```statblock
name: Dragón de Hielo Juvenil
image: img/Dragon-elemental.png
layout: Daggerheart Layout
description: Dragón elemntal de cubierto por un grueso plumaje blanco en el pecho y las exteremidades, el resto del cuerpo está protegido por una cota de escamas que reflejan la luz del sol como la nieve.
damage: [1, 23, 47]
hp: 11
ac: 19
stress: 6
exp: Arsitócrata +3, Colosal +3
attacks: 
- name: Garras
  desc: "Cercano `dice: 1d20+7` Daño `dice: 6d6` (físico)"
moves: 
- name: Desgarrar (pasiva)
  desc: "Los jugadores que marquen puntos de vida y no hayan empleado puntos de armadura para reducir el daño de esta criatura han de marcar 1 punto de estrés"
- name: Escamas Heladas (reacción)
  desc: "Cualquier criatura que dañe al dragón con un ataque cuerpo a cuerpo ha de superar una tirada de agilidad(16) o encontrarse vulnerable hasta salir del ragon cercano del dragón."
- name: Incansable (3)
  desc: "Se puede activar hasta 3 veces en un mismo turno"
- name: Aterrador (pasiva)
  desc: "Cualquier criatura que tire con miedo a rango cercano de la criatura pierde una esperanza."
- name: Aliento de Hielo
  desc: "Toda las criaturas en frente del dragón en distancia corta deben superar una tirada de agilidad(18). En caso de fallo deben reciben `dice: 3d20` de daño mágico, y se encuentran atrapados en los carámbanos hasta superar una tirada de fuerza(18). Criaturas que superen la tirada deben marcar dos puntos de estrés o recibir la mitad de daño. Este ataque elimina todas las activaciones del dragón."
- name: Avalancha (1 pro escena)
  desc: "Gasta un punto de miedo para invocar un alud que afecta a todas las criaturas a rango lejano del dragón, Las criaturas han de superar una tirada de agilidad(18) so ser enterradas bajo la nieve y encontrarse vulnerables."
```