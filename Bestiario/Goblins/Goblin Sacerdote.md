
```statblock
name: Goblin Sacerdote
image: img/Goblin.png
layout: Daggerheart Layout
description: Goblin devoto de Lamastu, su fe le ha otorgado una capacidad inata para la magia y es capaz de pedir el favor de su diosa en batalla
damage: [2, 6, 10]
hp: 4
stress: 4
ac: 14
attacks: 
- name: Cetro de alimañana
  desc: "Lejano `dice: 1d20+2` Daño `dice: 2d6` (mágico)"
moves: 
- name: Bendición de la Naturaleza
  desc: "Gasta 1 punto de estrés para curar a todos los aliados en rango cercano un número de puntos de vida igual al número de punts de estrés sin marcar."
- name: Guardián Espiritual
  desc: "Gasta 1 punto de miedo para invocar un huargo espiritual que ataca `dice: 1d20+3` a todas las critauras en rango cuerpo a cuerpo. En caso de golpear estas reciben `dice: 1d8` de daño mágico y son empujadas a rango cercano."
```