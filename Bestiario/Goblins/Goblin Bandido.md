
```statblock
name: Goblin Bandido
image: img/Goblin.png
layout: Daggerheart Layout
description: Goblin experimentado armado con dagas de hueso y cuchillos envenenados.
damage: [1, 8, 12]
hp: 4
ac: 14
stress: 3
attacks: 
- name: Cuchillo de hueso
  desc: "Cuerpo a Cuerpo `dice: 1d20+2` Daño `dice: 2d6` (físico)"
moves: 
- name: Tácticas de Equipo (pasiva)
  desc: "Cuando el Goblin golpea a un jugador y otro Goblin se encuentra a distancia cuerpo a cuerpo del objetivo realiza `dice: 2d10` de daño físico en su lugar." 
- name: Veneno de Goblin
  desc: "Gasta 1 punto de miedo para lazar un cuchillo envenenado hasta a dos objetivos en rango lejano `dice: 1d20+2`. En caso de golpear los objetivos quedan vulnerables hasta el fin de su turno"
```