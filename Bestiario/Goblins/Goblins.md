# `=this.file.name`
Humanoides de baja estatura, piel verdosa piernas y orejas de liebre. La mayoría viven en bosques y montañas alejados de la civilización aunque en las últimas décadas algunas bandas goblins se han unido a asentamientos alejados de los focos de población. Tienen una estrecha relación con la madre de bestias [[Lamastu]] y algunos son capaces de invocar sus poderes.
Durante siglos se ha considerado a los goblins como una amenaza para la civilización y en algunas regiones se han llegado a cazar hasta la extinción, esta historia sangrienta ha provocado que bandas goblins sientan recelo por la población de [[Marquis]].
## Enemigos en esta categoría
- [[Goblin Sacerdote]]
- [[Goblin Bandido]]