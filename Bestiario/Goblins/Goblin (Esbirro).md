
```statblock
name: Goblin (Esbirro)
image: img/Goblin.png
layout: Daggerheart Layout
description: Goblin raso armado con equipamento de hueso.
damage: [1, 1, 1]
hp: 1
ac: 14
stress: 1
attacks: 
- name: Cuchillo de hueso
  desc: "Cuerpo a Cuerpo `dice: 1d20` Daño 4 (físico)"
moves: 
- name: Esbirro (pasiva)
  desc: "Por cada 4 puntos de daño que reciba esta criatura, el atacante puede derrotar a otro esbirro a rango de su ataque."
- name: Ataque de Grupo
  desc: "Selecciona a un objetivo y ativa a todos los Goblin (Esbirro) a rango cercano de él. Realizan una tirada de ataque conjunta `dice: 1d20` en caso de golpear cada uno realiza `dice: 1d4+2` puntos de daño físico."
```