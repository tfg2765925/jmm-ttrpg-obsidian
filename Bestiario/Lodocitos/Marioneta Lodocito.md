
```statblock
name: Marioneta Lodocito
layout: Daggerheart Layout
description: Un humanoide poseído por un lodocito. El monstruo se ha apropiado de sus extremidades y se mueve forma errática y confusa.
damage: [1, 5, 12]
hp: 6
ac: 10
attacks: 
- name: Herramienta de Campo
  desc: "Cuerpo a Cuerpo `dice: 1d20` Daño `dice: 3d6` (físico)"
moves: 
- name: Emerger
  desc: "Gasta 1 punto de miedo para que el lodocito emerja del cuerpo poseído extendiendo sus extremidades, realiza un ataque contra un criatura en rango cercano."
```