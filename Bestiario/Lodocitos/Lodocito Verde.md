
```statblock
name: Lodocito Verde
layout: Daggerheart Layout
description: Una masa amorfa de líquido translúcido verde
damage: [1, 5, 10]
hp: 5
ac: 8
stress: 2
exp: Camuflaje +3
attacks: 
- name: Tentáculo
  desc: "Cuerpo a Cuerpo `dice: 1d20+1` Daño `dice: 2d8` (mágico)"
moves: 
- name: Lentitud
  desc: "Es necesario gastar 2 token para activar a este monstruo."
- name: Cuerpo acídico
  desc: "Cada ataque efectivo contra el monstruo marca también un punto de armadura."
- name: Consumir
  desc: "Gasta un punto de miedo, realiza un ataque 'dice: 1d20+2' contra una criatura. En caso de golpear marcan 2 puntos de estrés y están atrapados  por el cubo, por cada punto de acción que se añada al contar marca un punto de estrés. La criatura se libera si el cubo recibe daño severo en algún momento."
- name: División (pasiva)
  desc: "Cuando el lodocito marca su tercer punto de vida puede gastar un punto de miedo para dividirlo en dos Lodocitos verdes menores, cada uno con sus puntos de vida y estrés correspondientes. Se activan automáticamente sin gastar marcadores."
```