
```statblock
name: Lodocito Rojo
layout: Daggerheart Layout
description: Una masa amorfa emvuelta en llamas de brillantes colores
damage: [1, 6, 11]
hp: 5
ac: 10
stress: 3
exp: Camuflaje +3
attacks: 
- name: Tentáculo
  desc: "Cuerpo a Cuerpo `dice: 1d20+1` Daño `dice: 2d8` (mágico)"
moves: 
- name: Incendio reptante
  desc: "Esta criatura sólo puede moverse a rango muy cercano al activar esta habilidad. Todos los objetos flamabes a rango cuerpo a cuerpo del lodocito comeinzan a arder."
- name: Prender
  desc: "Realiza una ataque de tentáculo contra un objetivo a rango muy cercano, en caso de éxito comienzan a arder, recibiendo `dice: 1d6` de daño mágico por cada marcador que se añada a la iniciativa."
- name: División (pasiva)
  desc: "Cuando el cubo marca su tercer punto de vida puede gastar un punto de miedo para dividirlo en dos Lodocitos Rojos Menores, cada uno con sus puntos de vida y estrés correspondientes. Se activan automáticamente sin gastar marcadores."
```