
```statblock
name: Lodocito Menor
layout: Daggerheart Layout
description: Joven individuo lodocito, sorprendetemente rápido ágil son capaces de trepar por las paredes y emboscar a sus presas.
damage: [1, 8, 8]
hp: 2
ac: 14
attacks: 
- name: Tentáculo
  desc: "Cerca `dice: 1d20` Daño `dice: 2d6` (físico)"
moves:
- name: Dispersar
  desc: "Gasta 1 punto de miedo para separar el cuerpo y desaparecer. Todas als criaturas enemigas deben superar una tirada de percepción con dificultad 15 o ser vulnerables a su siguiente ataque."
```