
# `=this.file.name`
Monstruos amorfos que viven en colonias en las profundidades de la tierra y algunas mazmorras. Tienen una aversión natural a la luz del sol por lo que prefieren mantenerse bajo tierra o en regiones oscuras. Los lodocitos se reproducen asexualmente formando una colonia de individuos creados a partir de lo que se denomina un [[Gran Lodocito]]. Cuando un lodocito madura lo suficiente puede llegar a dejar la colonia para formar la suya propia.

Algunas especies de lodocitos pueden poseer cadáveres animales, permitiéndoles adentrarse en la luz solar.
## Enemigos en esta categoría
- [[Caballo Lodocito]]
- [[Gran Lodocito]]
- [[Lodocito Menor]]
- [[Lodocito Rojo]]
- [[Lodocito Rojo Menor]]
- [[Lodocito Verde]]
- [[Lodocito Verde Menor]]
- [[Malformación]]
- [[Marioneta Lodocito]]
- [[Soldado Lodocito]]