
```statblock
name: Gran Lodocito
layout: Daggerheart Layout
description: Corazón de la colonia y el único lodocito que puede generar nuevos individuos. Alcanzan un tamaño descomunal loque les obliga a mantenerse quietos en una estancia, sus hijos se encargan de suministrarles alimento.
damage: [3, 10, 18]
hp: 9
ac: 12
attacks: 
- name: Tentáculo
  desc: "Cerca `dice: 1d20+3` Daño `dice: 3d6` (físico)"
moves: 
- name: Convocar Lodocito
  desc: "Gasta 2 puntos de miedo para dividir el cuerpo principal y convocar 2 [[Lodocito Menor]]."
- name: Lanzar Escombros
  desc: "Gasta 1 punto de miedo para arrancar objetos de las estancia o criaturas y lanzarlas contra un objetivo (`dice: 1d20`). En caso de golpear las criaturas reciben `dice: 2d8` e daño físico."
```