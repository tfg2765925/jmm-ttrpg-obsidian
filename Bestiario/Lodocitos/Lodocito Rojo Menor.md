
```statblock
name: Lodocito Rojo Menor
layout: Daggerheart Layout
description: Una pequeña masa amorfa emvuelta en llamas de brillantes colores
damage: [1, 5, 5]
hp: 2
ac: 11
stress: 2
exp: Camuflaje +3
attacks: 
- name: Tentáculo
  desc: "Cuerpo a Cuerpo `dice: 1d20-1` Daño `dice: 1d6` (mágico)"
moves: 
- name: Ardiente (reacción)
  desc: "Cuando esta criatura recibe daño todas las criaturas en rango cuerpo a cuerpo reciben `dice: 2d6` de daño mágico."
```