
```statblock
name: Lodocito Menor
layout: Daggerheart Layout
description: Caballo poseído por uno o más lodocitos. Los individuos más inteligentes suelen tener preferencias por las bestias más grandes
damage: [1, 10, 15]
hp: 6
ac: 12
attacks: 
- name: Coz
  desc: "Cerca `dice: 1d20+3` Daño `dice: 3d6` (físico)"
moves:
- name: Apresar
  desc: "Gasta 1 punto de miedo para alzarse sobre dos patas, el cuerpo del lodocito emerge bajo la tripa y trata de apresar a una criatura en rango cercano `dice: 1d20+3`. La criatura queda atrapada hasta superar una tirada de fuerza con DC 14, por cada turno atrapada recibe `dice: 1d6+3` de daño físico"
```