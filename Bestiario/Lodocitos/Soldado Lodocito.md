
```statblock
name: Soldado Lodocito
layout: Daggerheart Layout
description: Una agrupación de lodocitos se han fusionado a un mismo cuerpo otorgándole una armadura natural formada por sus cuerpo gelationoso endurecidos. Porta un arma de verdad a pesar de carecer de la destreza de un guerrero de verdad lo compensa con una fuerza sobre humana
damage: [1, 8, 15]
hp: 6
ac: 14
attacks: 
- name: Bola de pinchos
  desc: "rango cercano `dice: 1d20+3` Daño `dice: 2d8+2` (físico)"
moves: 
- name: Reforjar
  desc: "Gasta 1 punto de miedo para que el soldado lodocito repare su armadura recuperando 1 punto de vida (máximo 1 vez)"
```