
```statblock
name: Lodocito Verde Menor
layout: Daggerheart Layout
description: Una pequeña masa amorfa de color verdoso
damage: [1, 4, 4]
hp: 2
ac: 14
stress: 1
exp: Camuflaje +3
attacks: 
- name: Tentáculo
  desc: "Cuerpo a Cuerpo `dice: 1d20-1` Daño `dice: 1d6` (mágico)"
moves: 
- name: Forma acídica (reacción)
  desc: "Cuando esta criatura recibe daño el atacante marca un punto de armadura"
```