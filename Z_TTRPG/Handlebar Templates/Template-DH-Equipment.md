---
price:
trait: 
range: 
damage_type: 
burden: 
score:
---
# `=this.file.name`

> [!infobox|wfit center]
> 
> | |
> ---|---|---|---|
> **Precio** | `=this.price` | **Tipo de Daño** | `=this.damage_type` |
> **Habilidad** | `=this.trait` | **Rango** | `=this.range` |
> **Manejo** | `=this.burden` | **Dado/Armadura** | `=this.score` |

## Descripción

## Rasgo