---
class: Bardo
pronouns: 
race: 
coumunity: 
level: 1
armor: "3"
evasion: "2"
hp:
  slot1: true
  slot2: true
  slot3: true
  slot4: true
  slot5: true
  slot6: true
  slot7: true
---
> [!column| flex 3 no-title]
>> [!infobox|wsmall left text-center]
> >## Clase
>> `INPUT[inlineSelect(option(Bardo), option(Druida), option(Guardián), option(Explorador), option(Pícaro), option(Serafín), option(Hechicero), option(Guerrero), option(Mago)):class]`
>> ## Dominios
>> -
>> -
>
>> [!infobox|wfit left text-center]
>>||||
>> :---:|:---:|:---:|:---:|
>> **Nombre** | `=this.file.name`|**Pronombres** | `INPUT[text:pronouns]` |
>> **Herencia** | `INPUT[text:race]` y `INPUT[text:comunity]` |**Subclase** | `INPUT[text:subclass]` |
>
>> [!infobox|wfit left text-center]
>> ## Nivel
>> `INPUT[inlineSelect(option(1), option(2), option(3), option(4), option(5), option(6), option(7), option(8), option(9), option(10)):level]`

> [!column| flex 2 no-title]
> > [!infobox|wfit left]
> > ||
> > :---:|:---:|
> > **Evasión** | `INPUT[number:evasion]` |
> > **Armadura** | `INPUT[number:armor]` |
> > ### Puntos de Armadura
> > |||
> > :---:|:---:|:---:|
> > `INPUT[toggle:armor.slot1]`|`INPUT[toggle:armor.slot2]`|`INPUT[toggle:armor.slot3]`|
> > `INPUT[toggle:armor.slot4]`|`INPUT[toggle:armor.slot5]`|`INPUT[toggle:armor.slot6]`|
> > `INPUT[toggle:armor.slot7]`|`INPUT[toggle:armor.slot8]`|`INPUT[toggle:armor.slot9]`|
>  
> > [!infobox|wfull left]
> > ### Estadísticas
> > Agilidad|Fuerza|Destreza|
> > :---:|:---:|:---:|
> > `INPUT[number:agility]` |`INPUT[number:strengh]` |`INPUT[number:finesse]` |
> > <ul><li>Correr</li><li>Saltar</li><li>Maniobrar</li></ul>|<ul><li>Levantar</li><li>Romper</li><li>Agarrar</li></ul>|<ul><li>Controlar</li><li>Esconder</li><li>Sabotear</li></ul>|
> > 
> > Instinto|Presencia|Conocimiento|
> > :---:|:---:|:---:|
> > `INPUT[number:instinct]` |`INPUT[number:pressence]` |`INPUT[number:knowledge]` |
> >  <ul><li>Percibir</li><li>Sentir</li><li>Guiar</li></ul>|<ul><li>Gracia</li><li>Actuar</li><li>Mentir</li></ul>|<ul><li>Recordar</li><li>Analizar</li><li>Comprender</li></ul>|

> [!column| flex 2 no-title]
> >[!infobox|wfit left]
> >## Umbrales de Daño
> >|||
> > :---:|:---:|:---:|
> >**Leve** `INPUT[number:minorDmg]` |**Graves**`INPUT[number:majorDmg]` |**Severo**`INPUT[number:severeDmg]` |
> > 1PV| 2PV | 3PV|
> > 
> > >[!infobox|wfull center text-center] Estadísticas
> > >### Puntos de Vida
> >  >`INPUT[toggle:hp.slot1]` `INPUT[toggle:hp.slot2]` `INPUT[toggle:hp.slot3]` `INPUT[toggle:hp.slot4]` `INPUT[toggle:hp.slot5]` `INPUT[toggle:hp.slot6]` `INPUT[toggle:hp.slot7]` `INPUT[toggle:hp.slot8]` `INPUT[toggle:hp.slot9]` `INPUT[toggle:hp.slot10]` `INPUT[toggle:hp.slot11]` `INPUT[toggle:hp.slot12]` 
> > >### Estrés
> > >`INPUT[toggle:stress.slot1]` `INPUT[toggle:stress.slot2]` `INPUT[toggle:stress.slot3]` `INPUT[toggle:stress.slot4]` `INPUT[toggle:stress.slot5]` `INPUT[toggle:stress.slot6]` `INPUT[toggle:stress.slot7]` `INPUT[toggle:stress.slot8]` `INPUT[toggle:stress.slot9]` 
> > >## Esperanza
> > >Gasta un punto de esperanza para usar una experiencia o ayudar a un aliado
> > >`INPUT[toggle:hope.slot1]` `INPUT[toggle:hope.slot2]` `INPUT[toggle:hope.slot3]` `INPUT[toggle:hope.slot4]` `INPUT[toggle:hope.slot5]`
> > >## Experiencia
> > >||
> > >:---:|:---:|
> > >`INPUT[text:exp1]` | `INPUT[number:exp1Mod]`|
> > >`INPUT[text:exp2]` | `INPUT[number:exp2Mod]`|
> > >`INPUT[text:exp3]` | `INPUT[number:exp3Mod]`|
> > >`INPUT[text:exp4]` | `INPUT[number:exp4Mod]`|
> > >`INPUT[text:exp5]` | `INPUT[number:exp5Mod]`|
> >
> > >[!infobox|wfull center text-center] Dinero
> > >
> > >Puñados|Bolsas|Cofres|Motones|Fortuna|
> > >:---:|:---:|:---:|:---:|:---:|
> > >`INPUT[toggle:money.slot1]` `INPUT[toggle:money.slot2]` `INPUT[toggle:money.slot3]` `INPUT[toggle:money.slot4]` `INPUT[toggle:money.slot5]` |`INPUT[toggle:money.slot6]` `INPUT[toggle:money.slot7]` `INPUT[toggle:money.slot8]` `INPUT[toggle:money.slot9]`| `INPUT[toggle:money.slot10]` `INPUT[toggle:money.slot12]` `INPUT[toggle:money.slot13]`|`INPUT[toggle:money.slot14]` `INPUT[toggle:money.slot15]` | `INPUT[toggle:money.slot16]`|
> > 
> > >[!infobox|wfull center text-center] Característica de Clase
> 
> >[!infobox|wfull left]
> > >[!infobox|wfull center text-center] Armas
> > > ## Proficiencia
> > >`INPUT[toggle:weapon.prof1]` `INPUT[toggle:weapon.prof2]` `INPUT[toggle:weapon.prof3]` `INPUT[toggle:weapon.prof4]` `INPUT[toggle:weapon.prof5]` `INPUT[toggle:weapon.prof6]`
> > >### Principal
> > >| | |
> > >:---:|:---:|:---:|
> > >`INPUT[text:primaryWeapon]`|`INPUT[text:traitsAndRange]`|`INPUT[text:damageDice]`|
> > >
> > >`INPUT[text:featureWeapon1]`
> > >### Secundaria
> > >| | |
> > >:---:|:---:|:---:|
> > >`INPUT[text:primaryWeapon]`|`INPUT[text:traitsAndRange]`|`INPUT[text:damageDice]`|
> > >
> > >`INPUT[text:featureWeapon2]`
> > 
> > >[!infobox|wfull center text-center] Armadura
> > >
> > >| | 
> > >:---:|:---:|
> > >`INPUT[text:armorName]`|`INPUT[number:armorBaseScore]`|
> > >`INPUT[text:featureArmor1]`
> >
> > >[!infobox|wfull center] Inventario
> > >`INPUT[text:inventory1]`
> > >`INPUT[text:inventory2]`
> > >`INPUT[text:inventory3]`
> > >`INPUT[text:inventory4]`
> > >`INPUT[text:inventory5]`



