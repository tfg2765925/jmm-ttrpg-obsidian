---
type: 
domain: 
clasification:
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Tipo** | `=this.type` |
> **Dominio** | `=this.domain` |
> **Clasificación** | `=this.clasification` |

# Información
