# Historia de Athas
Un milenio de guerra ha devastado el órden natural del planeta, el antaño próspero planeta azul de Athas es ahora un desolado desierto bañado por la luz de un sol enfermizo. Los pueblos de Athas viven bajo el yugo de los Lores de la Hechicería, poderosos pisonicos belicistas que durante siglos libraron campañas de exterminio por todo el planeta con bajo el servicio de un semidiós enloquecido.

Los lores, instigados por el más poderoso de todos Borys El Carnicero de Enanos, traicionaron a su señor e hicieron uso de magia profana para alzarse ellos mismos como dioses. El ritual fracasó y Borys se transformó en una bestia enloquecida de proporciones gigantes, ahora conocido como el Primer Dragón de Athas vagó durante décadas por el continente masacrando a enemigos y aliados por igual. Sus antiguos camaradas no pudieron hacer más que esconderse en sus ciudades de la furia del dragón.

La amenaza del retorno del del hechicero fue suficiente para devolver la cordura a Borys que junto a los lores supervivientes desarrolló un ritual para mantener el sello, un millar de esclavos son sacrificados cada 10 años en las puertas sombrías para conservar la prisión de su antiguo mentor. En un mundo moribundo donde el agua, la comida y el metal escasean incluso los poderosos señores luchan por aferrase a los restos de su poder.

Han transcurrido 50 años tras el regreso a la cordura del Primer Dragón, Athas sigue siendo un mundo abandonado por los dioses, devastado por magia profana y siglos de carnicería. La avaricia desmedida de unos pocos ha condenado a millones a una vida de penurias y conflictos. Pese a ello sus gentes aún se aferran a la vida, muchas soñando con la esperanza de un mundo mejor.

Hace diez años una revolución en la ciudad de Tyr acabó con la vida de un Lord de la Hechicería, se trata de la primera vez en la historia que un mortal ha dado muerte a un de estos lores. Las hazañas de los héroes que han traído la libertad a los esclavos han volado por el continente, despertando el interés de los misteriosos druidas de  la conservación.

### Los Dioses están callados
En Athas el poder divino no existe, tras la ascensión de Borys la conexión del planeta con la divinidad fue cercenada. Los cléricos y paladines de Athas adoran a las fuerzas elementales o a los propios señores de la hechicería y de su fe nace su poder.

### Un mundo desolado
Hechizos que en otros planetas funcionaría correctamente no puede realizarse en Athas sin un coste, crear y destruir agua requiere de seres vivos de la cual extraerla para generar agua potable en los niveles bajos. Otros hechizo que crearían comida y bebida tienen asociados componentes consumidos al final del mismo para poder realizar el ritual de forma satisfactoria.
Los viajes por el desierto toman la mayoría de las vidas del planeta. En un mundo donde el agua potable escasea y los accesos a la misma están guardados por fuerzas poderosas es necesaria una gran preparación antes de adentrarse en el desierto.

### Oposición a la tiranía
Aunque la tierra es cruel y las peleas internas son comunes, aún existen personas dispuestas a enfrentarse a los lores. Héroes se han alzado en el pasado para plantarle cara a la injusticia de los dragones y reclamar un mundo más justo.

### Fauna
La mayoría de las bestias que se pueden ver en otros planetas (caballos, vacas, peces, etc...) se han extinguido en Athas. La población hace uso de insectos y reptiles gigantes como bestias de carga y monturas. La mayoría de los habitantes del desierto son abominaciones cubiertas de patas y ojos siempre buscando una nueva comida. En los cielos mantas gigantes acechan desde las alturas a sus posibles presas.

# Sociedad
Athas es un planeta desértico y a las puertas de la muerte, el agua, la comida y el metal escasea. Muchos han adoptado una mentalidad egoísta para sobrevivir, buscando siempre nuevas víctimas a las que robar con tal de vivir otro día. La mayoría de las población se concentra en las ciudades estado de los Lores, antiguamente 12, muchas han caído debido a revueltas de esclavos, traiciones y la mano de otros lores.

## [[Balic]]
Inspirada en la Roma de la república, se trata de la ciudad situada al sur del Athas, junto al mar de Arena. Protegida por Andropinis, y gobernada por un grupo de sus templarios elegidos democráticamente. Aunque son comunes las desapariciones de aquellos que hablen contra el Lord desaparezcan.

## [[Draj]]
Inspirada en el imperio Azteca, situada en el noreste del continente, en los páramos del barro. Actualmente gobernada por la señora de las dos lunas y la hechicera ascendida Tectuktitlay. En una guerra perpetua contra las otras ciudades y los Liberos, buscando hordas de esclavos pasa satisfacer los sacrificios demandados por Borys.

## [[Giustenal ]]
Inspirada en la cultura griega, liderada por el demente lord dragón Dregoth. Durante su alzamiento Hammal Mu y Andropinis asediaron la ciudad dieron muerte a su lord. Sin embargo sus creaciones, los Dray, se adentraron en las entrañas de la tierra donde encontraron a los Drow y Duergar. Formaron una alianza y juntos resucitaron a Dregoth, ahora con la forma de un dracoliche experimenta sobre sus súbitos, formando un ejército de aberraciones con las que reclamar la superficie.

## [[Gulg]]
Inspirada en la cultura Kush, se encuentra junto a las selvas de la región en el sureste del continente. Gobernada por la hechicera Lalay-Pului, adoptando el pseudónimo de la diosa de los bosques Lalay-Pui. Es la única ciudad que hace uso de la magia antigua para construir sus edificios, la ciudad está compuesta por tejido vivo y es una extensión de la hechicera.

## [[Nibenay]]
Inspirada por Angkor, capital del imperio de Khme, ubicada en el centro de la region se encuentra bajo la tiranía de Nibenay, la perdición de los Gnomos. Es un foco de comercio de la región debido a su ubicación, aunque las constantes desapariciones de su rey han atraído a nobles y guerrilleros buscando formas de hacerse con el trono.

## [[Raam]]
Inspirada por el imperio Mugal es la ciudad estado más grande del continente. Se encuentra al este de la región y está gobernada por la hechicera Abalach-Re, cuyo desinteres por la política ha instigado incontables hambrunas y traído un sin fin de revueltas a su ciudad. En consecuencia sus templarios se han convertido en espías para su reina, buscando revolucionarios o disidentes. Este clima de desconfianza y decadencia mantiene a la ciudad al borde de la anarquía.

## [[Tyr]]
Inspirada en el tardío imperio egipcio es la única ciudad libre de Athas. Gobernada por el consejo de Tyr tras el golpe de estado de Tithian, que junto a su ejército de gladiadores asesinaron al rey hechicero Kalak y tomaron la ciudad. Desde hace 10 años Tyr es libre y la única de las ciudades estado que ha abolido la esclavitud.

## [[Ur Draxa]]
La ciudad de Borys se trata de una macroprisión que mantiene el sello sobre las puertas sombrías consiste en una descomunal pirámide que alberga a la corte del Dragón de Athas y sirve como conducto para sacrificar las vidas de millones para mantener a raya al hechicero Rajaat. El resto de la ciudad son barracones donde hospedar a las legiones de templarios de Borys.

Además de las ciudades existent pequeños asentamientos de esclavos liberados. Estos grupos conocidos como liberos, viven en el desierto en los diminutos oasis que aún no han sido descubiertos por los cazadores de esclavos. Han forjado una comunidad basada en el apoyo mutuo entre esclavos y tienden a asaltar caravanas mercantes con el fin de liberar a sus hermanos. Los grupos situados al noroeste han recibido la ayuda de las tribus de Golomas y ahora gozan de su protección.

En la selva se encuentra la Ciudad de Kalidnay gobernada por el último de los Rhulisti, una antigua raza de medianos que se decía que gobernaba el planeta cuando este aún era azul. Kalidnay sirve de refugio para los medianos de Athas y los altos elfos.

# Geografía

# Habitantes

> [!column| flex 2 no-title]
> > [!infobox|wfull center]
> >| Especie | Subespecies/Culturas | Especie | Subespecies/Culturas
> >| --- | --- |--- | ---  
> >|**Elfos**| <ul><li>Alto</li><li>Athas</li><li>Oscuro</li></ul> |**Enanos**| <ul><li>Estoico</li><li>Montaña</li><li>Oscuro</li></ul>
> >|**Reptilianos**| <ul><li>Dray</li><li>Petran</li><li>Nakkai</li></ul> |**Humanos**| <ul><li>Goliath</li><li>Semi-elfo</li><li>Semi-orco</li></ul> 
> >|**Hombres-Bestia**| <ul><li>Aaracockra</li><li>Lupin</li><li>Leónido</li></ul>| **Goblins** |<ul><li>Hobgoblin</li><li>Maglubiyet</li><li>Vrill</li></ul>
> >| **Goloma**| <ul><li>Aterrador</li><li>Gladiatorial</li><li>Libero</li></ul> | **Orcos** | <ul><li>Athas</li><li>Gruumush</li></ul>
> >|**Gnolls**|<ul><li>Athas</li><li>Gladiatorial</li><li>Enfurecido</li></ul>
> 
> > [!infobox|wfull center] 
> > | Herencias |
> > | --- |
> > | Aasimar |
> > | Híbrido |
> > | Tiefling |
> > | Suli |
> > | Renacido |
> > 

## Elfos

### Rasgos Raciales
Velocidad: 9 metros
Tamaño: Mediano

| Rasgos |
| --- |
|**Linaje feérico**. Tienes ventaja en las tiradas de salvación para no quedar hechizado y no puedes quedarte dormido por ningún efecto mágico. |
|**Trance**. Los elfos no necesitan dormir, en lugar de ello meditan profundamente y permanecen semiinconscientes durante cuatro horas al día (conocido como «trance»). Descansar de este modo te otorga los mismos beneficios que dormir ocho horas a un humano. |
|**Visión en la oscuridad**. Estás acostumbrado a la luz crepuscular de los bosques y al cielo nocturno. Puedes ver en la penumbra a una distancia de 19.5 metros como si fuera luz brillante y, en la oscuridad como si fuera penumbra. En la oscuridad no puedes distinguir colores, solo tonos de gris.|

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> > # Alto
> > Los altos elfos son los descendientes de una nación élfica perdida en el tiempo, al contrario que sus contrapartes de Athas han optado por vivir asentados junto con los medianos en las selvas del [[Verdadero Bosque]]. Se han convertido en una sociedad de cazadores recolectores extremadamente prejuiciosa de la magia profana y los humanos, considerándolos culpables del actual estado del planeta.
> > Los Altos elfos colaboran con los Rhulisti supervivientes y los Avangion para tratar de preservar y expandir los bosques del planeta.
>
> > [!infobox|wfull left]
> > # Athas
> > Se trata de un pueblo nomadico cuya perspectiva nihilista de la vida no ha apagado el ferviente deseo de libertad de los elfos antiguos, la mayoría viven en el desierto viajando de ciudad en ciudad, nunca asentándose en un sitio durante más de unas semanas. No se comúnmente empleados como esclavos debido que su naturaleza libre les ha generado una claustrofobia innata. Elfos capturados aguantan pocas semanas antes de enloquecer o tratar una huida desesperada, para su pueblo la muerte es mejor que la vida en cadenas.
> > Se han adaptado a la vida en Athas desarrollando esbeltos cuerpos especializados en la velocidad, su complexión ligera les ayuda a atravesar el desierto y evitar la mayoría de las arenas movedizas.
> 
> > [!infobox|wfull left]
> > # Oscuro
> > Los elfos oscuros son descendientes de aquellos que abandonaron la superficie al comenzar las cruzadas, construyendo un nuevo reino en las profundidades de la tierra. Tras el asesinato del lord hechicero [[Dergoth, Estrago de los Gigantes]] y su correspondiente resurrección en las entrañas de la tierra, este pueblo ha resurgido alabando al rey renacido como su nuevo señor.

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> >| Rasgos |
> >| --- |
> >|**Truco**. Aprendes un truco de tu elección de la lista de conjuros de Druida |
> >|**Entrenamiento con armas élficas**. Eres proficiente con las armas élficas |
>
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Movimiento mejorado**. Tu velocidad aumenta 12 pies. |
> > |**Ligero**. Para cualquier efecto que requiera levantarte cuentas como una criatura pequeña |
> 
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Sensibilidad Solar**. Mientras estés expuesto a luz solar directa tienes desventaja en las tiradas de ataque y percepción|
> > |**Visión en la oscuridad Superior**. Estás acostumbrado a la luz crepuscular de los bosques y al cielo nocturno. Puedes ver en la penumbra a una distancia de 24 metros como si fuera luz brillante y, en la oscuridad como si fuera penumbra. En la oscuridad no puedes distinguir colores, solo tonos de gris.|
> > |**Sentidos Agudizados**. Ganas Proficiencia en la habilidad de percepción |
> > |**Entrenamiento militar**. Ganas proficiencia con dos armas o armaduras a tu elección |
> > |**Entrenamiento arcano**. Aprendes los trucos golpe certero y salpicadura ácida, empleas cualquier característica a tu elección para lanzar el conjuro |

## Enanos
Velocidad: 7.5 metros
Tamaño: Mediano

| Rasgos |
| --- |
|**Fortaleza Enana**. Tienes ventaja en las tiradas de salvación para contra venenos y al tomar comida de dudosa procedencia eres también resistente al daño de veneno. |
|**Velocidad Enana**. Tu velocidad no se reduce al portar armadura |
|**Visión en la oscuridad**. Estás acostumbrado a la luz crepuscular de las cuevas. Puedes ver en la penumbra a una distancia de 19.5 metros como si fuera luz brillante y, en la oscuridad como si fuera penumbra. En la oscuridad no puedes distinguir colores, solo tonos de gris.|

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> > # Estoico
> > Los enanos estoicos son un híbrido inicialmente creado con magia profana mezclando un enano con un humano. Los estoicos conservan el tamaño y velocidad de los humanos peor la resistencia característica de los enanos. Sus capacidades físicas han provocado que sean frecuentemente perseguidos para ser vendido como esclavos o entregados a las arenas de gladiadores.
>
> > [!infobox|wfull left]
> > # Montañas
> > Los descendientes del último reino enano de Rkaard. Ahora perdido en el tiempo sus gentes se han adaptado a la nueva vida en Athas ocupando los puestos de artesanos en las ciudades de los lores. Es común entre los enanos de Athas abandonar su hogar en busca de su Deseo, una tradición enana en la cual un joven viaja por el continente buscando cumplir una tarea de gran envergadura autoimpuesta. Es común entre los enanos no regresar del deseo, pero pocos se atreven a retornar a sus familias sin cumplirlo.
> 
> > [!infobox|wfull left]
> > # Oscuro
> > Junto con los Elfos de la segunda era, un reino enano descendió a las profundidades para huir de las guerras de extinción. Los milenios que pasaron en las entrañas del planeta los cambiaron de forma permanente, su piel tornándose de un color grisáceo y sus barbas de un blanco fantasmal. Han acudido a la llamada del Dracoliche y la mayoría se han unido a las  huestes del antiguo lord.

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> >| Rasgos |
> >| --- |
> >|**Fortaleza**. Ganas dos puntos de vida extra al subir de nivel y a nivel 1 |
> >|**Incansable**. Dormir 6 horas cuenta como un descanso largo para ti |
> >|**Altura**  Tu velocidad base es de 9 m| 
>
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Intuición del Artesano**. Cuando realiza una triada de inteligencia puede añadir un 1d4 a las tiradas. |
> > |**Guardas y Sellos**. Una vez al día puedes lanzar el escudo de Cerradura Arcana o Abrir, tu habilidad para este conjuro es inteligencia |
> 
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Sensibilidad solar**. Mientras estés expuesto a luz solar directa tienes desventaja en las tiradas de ataque y percepción|
> > |**Visión en la oscuridad Superior**. Estás acostumbrado a la luz crepuscular de los bosques y al cielo nocturno. Puedes ver en la penumbra a una distancia de 24 metros como si fuera luz brillante y, en la oscuridad como si fuera penumbra. En la oscuridad no puedes distinguir colores, solo tonos de gris.|
> > |**Portento mágico**. Puedes lanzar el hechizo de Agrandar/Reducir una vez por descanso largo, tu eliges que característica empleas para lanzar este conjunto al seleccionar esta subespecie.|

## Gnolls

Velocidad: 9 metros
Tamaño: Mediano

| Rasgos |
| --- |
|**Mordisco**. Tus fauces son un arma natural y te permiten lanzar un ataque desarmado. Inflige 1d6 + tu modificador de fuerza como daño cortante |
|**Frenesí**. Una vez por descanso largo, como acción bonus puedes realizar un ataque cuerpo a cuerpo con tus fauces. Si golpeas a una criatura con este ataque ganas el daño infligido como puntos de vida temporales. |

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> > # Athas
> > Debido a las guerras de extinción la población de gnolls de Athas se vió reducida a una serie de pequeñas bandas vagando por las antiguas sabanas del continente. Fue gracias a la intervención de Yeenoghu que los Gnolls pudieron sobrevivir. Sin embargo tras el fin de la guerra las tropas ofrecidas por el Yeenoghu se volvieron contra los Gnolls originales.
> > Desamparados se los líderes de los diferentes clanes buscaron ayuda en los primero druidas, junto a ellos formaron una coalición para protegerse de la devastación provocada por la ascensión de Borys y las fuerzas del dios de los Gnolls. A día de hoy los Gnolls Athathianos viven en harmonía con la naturaleza la mayoría en comunidades de druidas formadas alrededor de los oasis del continente.
> 
> > [!infobox|wfull left]
> > # Gladiatorial
> > Cuando Borys regresó a la cordura, cientos de gnolls habían pasado años cautivos en las ciudades, forzados a luchar como bestias en las arenas. Durante estos años los Gnolls pasaron de ser vistos como bestias sedientas de sangre a gladiadores experimentados. Algunas familias ganando su libertad y ascendiendo a la nobleza de las ciudades. Sin embargo la mayoría siguen trabajando como esclavos en las Arenas, ejerciendo ahora como gladiadores.
>
> > [!infobox|wfull left]
> > # Yeenoghu
> > Los restos de las hordas de Gnolls que Yeenogu envió a Athas para ayudas a sus hermanos, dotados de una insaciable sed de sangre que no se apaciguó tras la guerra, ni el frenesí del Dragón de Athas. Los Gnolls de Yeenoghu viver por y para la matanza, mostrar debilidad es visto como una afrenta al dios de las hienas y aquellos que no siguen la ley de la manada son brutalizados y abandonados en las dunas para recibir una muerte lenta y dolorosa.

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> >| Rasgos |
> >| --- |
> >|**Diminutivo**. Tu tamaño se reduce a pequeño en lugar de mediano, cuando no llevas armadura pesada ni media añades un +2 a tu AC.|
> >|**Visión en la oscuridad**. Estás acostumbrado a la luz crepuscular de los bosques y al cielo nocturno. Puedes ver en la penumbra a una distancia de 19.5 metros como si fuera luz brillante y, en la oscuridad como si fuera penumbra. En la oscuridad no puedes distinguir colores, solo tonos de gris.|
> 
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Corpulento**. Ganas 5 puntos de vida extra a nivel 1, para cualquier efecto que requiera moverte cuentas como una criatura grande|
> > |**Intimidante**. Tu poderoso tamaño te ha dotado de una apariencia aterradora, obtienes proficiencia en la habilidad de intimidación, y ventaja para empujar y agarrar objetivos.|
>
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Bendición de Yeenoghu**. Restos de sangre divina, aunque diluida corren por tus venas, aprendes el truco Risa Horrible de Tasha, cuando seleccionas esta especie tú eliges que características empelas para lanzar este conjuro. |
> > |**Furia**. Una vez por descanso largo puedes entrar en un estado de furia similar al de la un bárbaro. |

## Goblins
Velocidad: 9 metros
Tamaño: Pequeño

| Rasgos |
| --- |
|**Ágil**. Puedes usar las acciones de retirada y correr como acción bonus |
|**Furia de los chiquitos**. Una vez por descanso largo, si golpeas a una criatura más grande que tú puedes añadir tu nivel al daño. |


> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> > # Hobgoblin
> > Subespecie de Goblin que habitaba el subsuelo durante las guerras de extinción. De considerable tamaño y mayor fuerza que los goblins terrestres presentan un piel rojiza y melenas de pelo oscuro. Fueron los enanos y los elfos en su búsqueda de un nuevo hogar quienes descubrieron por primera vez a los Hobgoblins, desde el fin de las campañas de exterminio, se han hospedado en la superficie. 
> > El metal se había convertido en un preciado recurso en el planeta, precidado recurso que no escaseaba en las grutas subterráneas de los Hobgoblins,no tardaron en aprovechar esta oportunidad para llegar a un trato con los lores de la hechicería. Las últimas generaciones de hobgoblins viven en las ciudades, la mayoría han hincado la rodilla ante su respectivo señor y ocupan altos puestos en la nobleza, aunque también se encuentran aquellos que han optado por ejercer de templario para su señor.
>
> > [!infobox|wfull left]
> > # Maglubiyet
> > Durante las cruzadas de la humanidad los goblins junto a las hadas fueron perseguidos hasta la extición. El dios de los Goblins, Maglubiyet, enfurecido por esta falta de respeto siguió los pasos de Yeenoghu y Gruumush y trajos sus propias huestes de Goblins desde otros planetas. Hace cientos de años desde que se produjo esta invasión, los poderes de los dioses se han disminuido y Maglubyet no es una excepción.
> > Sus lacayos siguen en Athas, con el paso de los años las nuevas generaciones han llegado a llamar a este planeta su hogar, muchos viven en ciudades ejerciendo de artesanos en talleres enanos, pero no es extraño encontrar Goblins entre los cazadores de esclavos.
> 
> > [!infobox|wfull left]
> > # Vrill
> > Los Goblins que siguieron los pasos de los elfos y enanos no tuvieron tanta suerte como sus compañeros de viaje. Su pueblo fue rápidamente sometido por los hobgoblins, y tras milenios de guerra, vendidos a la realiza drow. La cuál experimento sobre los mismos para incrementar sus capacidades ofensivas, los Vrill se asemejan a un goblin morado de gran tamaño con brazos alargados. Los Goblins de Athas ya no existen, pero los Vrill son su descendencia.
> > Originalmente creados para servir como carne de cañón para los nobles oscuros, una Vrill se reveló contra su maestro durante una campaña de caza en el subsuelo. Se trataba de un acto nunca visto puesto que años de selección artificial había diluido completamente el sentido de la independencia de los Vrill. Esta declaración de libertad  se extendió por todo su pueblo y pronto la revolución libero a los Goblins oscuros.
> > Ahora en la superficie actúan como una órden de caballeros contra la injusticia y la esclavitud liberando a los oprimidos y enfrentándose a las huestes de los lores de la hechicería.

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> >| Rasgos |
> >| --- |
> >|**Mediano**. Ganas el tamaño mediano, pierdes acceso a Furia de los chiquitos |
> >|**Visión en la oscuridad**. Estás acostumbrado a la luz crepuscular de los bosques y al cielo nocturno. Puedes ver en la penumbra a una distancia de 19.5 metros como si fuera luz brillante y, en la oscuridad como si fuera penumbra. En la oscuridad no puedes distinguir colores, solo tonos de gris.|
> >|**Conservar las apariencias**. Cuando fallas una triada de ataque o de salvación, puede añadir el número de criaturas aliadas (máximo 5) que te estén viendo a la tirada. |
>
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Estómago de acero**. Tienes resistencia al veneno y tiras con ventaja en las tiradas de salvación contra veneno. La comida en mal estado no te puede hacer enfermar y cuenta como si fuese una comida normal. |
> > |**Sigiloso**. Ganas proficiencia en sigilo y en juego de manos. |
> 
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Sensibilidad Solar**. Mientras estés expuesto a luz solar directa tienes desventaja en las tiradas de ataque y percepción|
> > |**Visión en la oscuridad Superior**. Estás acostumbrado a la luz crepuscular de los bosques y al cielo nocturno. Puedes ver en la penumbra a una distancia de 24 metros como si fuera luz brillante y, en la oscuridad como si fuera penumbra. En la oscuridad no puedes distinguir colores, solo tonos de gris.|
> > |**Sentidos Agudizados**. Ganas Proficiencia en la habilidad de percepción |
> > |**Chillido**. Puedes sustituir uno de tus ataques por un chillido ensordecedor que inflige 1d8 daño de trueno a las criatura a tu alrededor.  EL área de efecto es un círculo centrado en ti con un radio de 4.5 metros. Criaturas en el área han de superar una tirada de salvación igual a 8 más tu modificador de constitución + proficiencia, en caso de superarlo reciben la mitad de daño. A nivel 6 el daño aumenta a 2d8 y a nivel 11 a 3d8. |

## Goloma
Los Goloma son un pueblo de humanoides, con cabeza de caballo, el resultado de magia profana sobre las bestias de carga durante las cruzadas. Una mezcla entre Humano, caballo y arañas gigantes ha creado una raza de bestias nacidas para el conflicto.

Velocidad: 9 metros
Tamaño: Grande

| Rasgos |
| --- |
|**Fortaleza**. Ganas dos puntos de vida extra al subir de nivel y a nivel 1 |
|**Ojos en la nuca**. Eres proficiente en percepción y no es posible sorprenderte. |
|**Descomunal**. Requieres el doble de sustento que una criatura normal para sobrevivir. Todos tus ataques tienen el rango extendido y las amras y armaduras que empelas tienen que estar hechas a medidas. Estas ganan también un 1d4 de daño al golpear |

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> > # Aterrador
> > Conocidos también como "puros", son aquellos que mas se asemejan a los Golomas de guerra, de aspecto imponente, tienden a ocupar puestos altos dentro de los templarios, o como guarda espaldas de caravanas
>
> > [!infobox|wfull left]
> > # Gladitorial
> > Golomas que han crecido en las arenas de combate de las diferentes ciudades, siendo los favoritos del público muchos han optado por el uso de máscaras y disfraces que ocultan sus extraños cuerpos con la intención de ganarse la admiración de los humanos.
> 
> > [!infobox|wfull left]
> > # Libero
> > Algunos Golomas han huido del ambiente opresivo de las ciudades y las constantes amenazas de acabar esclavizados por su portentoso físico. Los Golomas se han unido a los Liberos y emplean su descomunal fuerza en luchar contra la opresión a su pueblo.

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> >| Rasgos |
> >| --- |
> >|**Mirada Paralizante**. Una vez por descanso largo, puedes realizar una triada de intimidación como acción bonus contra una criatura que pueda verte, en caso de éxito esta criatura se encuentra bajo los efectos del hechizo de miedo. |
>
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Floritura**. Cuando asestas un crítico a una criatura, puedes usar la acción de salto como acción gratuita |
> 
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Vigilante**. Obtienes proficiencia en supervivencia y perspicacia, además puedes lanzar el conjuro detectar magia a voluntad. |

## Humanos
La raza predomiante durante las guerras de exterminio, el caos provocado pro Borys y la cantidad de sacrificios demandada por los lores de la hechicería han provocado que el número de humanos de Athas se haya reducido considerablemente. La gran mayoría habitan en las ciudades muchos sirviendo como esclavos o templarios.

Velocidad: 9 metros
Tamaño: Mediano

| Rasgos |
| --- |
|**Milicia Civil**. Eres proficiente con lanzas, picas, alabardas, glaives, armadura ligera y escudos |
|**Versatilidad Humana**. Ganas proficiencia en una habilidad a tu elección. |
|**Tenacidad Humana**. Si no seleccionas ninguna subespecie, obtienes un +1 a una característica a tu elección. |

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> > # Semi-elfo
> > Nacidos de la unión de un humano y un elfo, estos mestizos son comúnmente abandonados a su suerte si la madre es una elfa Athasiana, al ser vistos como una abominación.
> 
> > [!infobox|wfull left]
> > # Semi-gigante
> > Híbridos creados con magia profana. Con el alzamiento de las ciudades-estado los lores comenzaron a experimentar sobre sus súbditos. Buscando una forma de apoderarse de la indomable fuerza de los gigantes, nacieron estos seres, también conocidos como Goliaths.
>
> > [!infobox|wfull left]
> > # Semi-orco
> > Nacidos de la unión de un humano y un orco, estos mestizos son empelados por las milicias o los templarios como soldados rasos si da la casualidad de que nacen dentro de las ciudades. En el exterior, las tribus de orcos los ven como una criatura endeble a la que proteger.

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> >| Rasgos |
> > | --- |
> > |**Linaje feérico**. Tienes ventaja en las tiradas de salvación para no quedar hechizado y no puedes quedarte dormido por ningún efecto mágico. |
> > |**Visión en la oscuridad**. Estás acostumbrado a la luz crepuscular de los bosques y al cielo nocturno. Puedes ver en la penumbra a una distancia de 19.5 metros como si fuera luz brillante y, en la oscuridad como si fuera penumbra. En la oscuridad no puedes distinguir colores, solo tonos de gris.|
>
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Fortaleza**. Ganas dos puntos de vida extra al subir de nivel y a nivel 1 |
> > |**Descomunal**. Requieres el doble de sustento que una criatura normal para sobrevivir. Todos tus ataques tienen el rango extendido y las armas y armaduras que empelas tienen que estar hechas a medidas. Estas ganan también un 1d4 de daño al golpear, tu tamaño aumenta a grande |
> 
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Indomable**. La primea vez en un descanso largo que un golpe te dejaría inconsciente te recuperas con 1 punto de vida. |
> > |**Visión en la oscuridad**. Estás acostumbrado a la luz crepuscular de los bosques y al cielo nocturno. Puedes ver en la penumbra a una distancia de 19.5 metros como si fuera luz brillante y, en la oscuridad como si fuera penumbra. En la oscuridad no puedes distinguir colores, solo tonos de gris.|

## Hombres-Bestia

Velocidad: 9 metros
Tamaño: mediano


| Rasgos |
| --- |
|**Estómago de acero**. Tienes resistencia al veneno y tiras con ventaja en las tiradas de salvación contra veneno. La comida en mal estado no te puede hacer enfermar y cuenta como si fuese una comida normal. |
|**Garras**. Puedes realizar un ataque cuerpo a cuerpo con tus garras estas inflijen un 1d6+ tu modificador de fuerza como daño cortante.|

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> > # Aaracockra
> > Especie de pájaros humanoides que habitan las montañas de Athas, ejerciendo en ocasiones como mensajeros entre los lores. Antiguamente los Aaracockra tomaban la forma de un sinfín de especies de ave, sin embargo la guerra y el cambio climático ha provocado que sólo sobrevivan aquellos que se asemejan a buitres, halcones y gorrinones.
>
> > [!infobox|wfull left]
> > # Lupin
> > Una de las razas bestiales de Athas, se asemejan a perros humanoides de baja estatura. Durante las cruzadas se produjo un éxodo masivo de Lupins hacia la zona desértica del continente con la esperanza de dejar atrás el conflicto, cuando el sol cambió de color los antiguos desiertos de Athas se volvieron demasiado inhóspitos como para albergar vida y los Lupin tuvieron que volver a emigrar.
> 
> > [!infobox|wfull left]
> > # Leónido
> > Una raza de humanoides con cabeza de león que habitaban las selvas de Athas. Durante las cruzadas fueron de las únicas razas que consiguieron mantener a raya a la humanidad. Cuando Borys perdió el control desoló el antiguo reino leónido y sus refugiados buscaron cobijo en la selva de Athas.
> > Descontentos con la forma de vida de los druidas y los altos elfos, muchos se han establecido en las ciudades donde ejercen de gladiadores y templarios.

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> >| Rasgos |
> >| --- |
> >|**Vuelo**. Presentas un par de alas plumadas que te otorga una velocidad de vuelo de 9m.  |
>
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Olfato**. Obtienes ventaja en las tiradas de percepción que requieran de oler y ganas visión verdadera a 3 metros siempre que puedas oler. |
> > |**Tácticas de manada**. Siempre que atacas a un objetivo y un aliado se encuentra a menos de 1.5 metros de él y ambos son hostiles lo haces con ventaja. |
> 
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Movimiento mejorado**. Tu velocidad aumenta 10.5 metros. |
> > |**Rugido**. Una vez por descanso corto puedes gastar una acción adicional para emitir un poderoso rugido centrado en tí y con radio de 6 metros y elegir una de las siguiente opciones <ul><li>Todos los enemigos a rango han de superar una tirada de salvación igual a 8 más tu modificador de constitución + proficiencia o recibir desventaja en sus ataques hasta el final de tu siguiente turno.</li><li>Inspiras a tus aliados, todos los objetivos a rango obtienen ventaja en sus ataques hasta el final de tu siguiente turno.</li></ul>|

## Reptilianos
Velocidad: 9 metros
Tamaño: Mediano

| Rasgos |
| --- |
|**Sangre fría**. Tienes una innata resistencia al calor, cuando haces tiradas de salvación contra la extenuación por viajar por el desierto las haces con ventaja. Tu sangre fría implica que no estás preparado para las bajas temperaturas eres débil al daño de hielo |

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> > # Dray
> > También conocidos como dracónidos son el resultado de las investigaciones del lord de la hechicería Dregoth sobre el proceso de draconificación. La primera generación de Dray no era más que un miserable de músculos con escamas descoloridas. Para la segunda generación el ritual había sido perfeccionado con magia Drow y ahora muestran un cuerpo esbelto cubierto de escamas y alzandose hasta los 2 metros. Algunos Dray son conscientes de la demencia de su creador y han optado por escapar a la superficie en un busca de un futuro mejor
>
> > [!infobox|wfull left]
> > # Petran
> > Hombres petrodáctilo que habitan las montañas pasadas las selvas de Athas. Los clanes principales viven en harmonía con la naturaleza, aunque son muchos los que han abandonado una vida de meditación en busca de bienes materiales. Ofreciendo sus servicios a lores y liberos por igual estos Petran codiciosos actúan tanto de mensajeros como de asesinos
> 
> > [!infobox|wfull left]
> > # Nakkai
> > Antiguamente los Nakkai era una de las muchas subespecies de reptilianos que habitaban los bosques verdes de Athas, las cruzadas y las persecución de Borys ha reducido la población de reptilianos naturales a los Nakkai y los Petran. Este pueblo presenta la forma de de un cocodrilo humanoide, habitan el único río de Athas, y  comúnmente ejercen de guardaespaldas para nobles que buscan viajar. Una banda de considerable tamaño e Nakkai ha comenzado a actuar como piratas en el Mar de Arena

> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> >| Rasgos |
> >| --- |
> >|**Resistencia dracónica**. Eres resistente al daño de fuego y ganas ventaja en las tiradas de salvación contra efectos que provoquen ese tipo de daño. |
> >|**Aliento dracónico**. Una vez por descanso corto puedes realizar un ataque con tu aliente de fuego. Este ataque es un cono de 5m e inflige 2d6 de daño de fuego. Criaturas en el área han de superar una tirada de salvación igual a 8 más tu modificador de constitución + proficiencia, en caso de superarlo reciben la mitad de daño. A nivel 6 el daño aumenta a 3d6 y a nivel 11 a 4d6.  |
>
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> >|**Vuelo**. Presentas un par de alas plumadas que te otorga una velocidad de vuelo de 9m.  |
> >|**Pico**. Puedes realizar un ataque cuerpo a cuerpo con tus pico este inflige un 1d8 más tu modificador de fuerza como daño punzante.|
> >|**Alas**. Tus brazos están pensados para volar no para manejar objetos ni armas, cuando sujetas algo haz de hacerlo con los pies|
> 
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Armadura natural**. Tienes una piel compuesta de gruesas escamas, cuando no estés llevando armadura tu AC es igual a 13 +m tu modificador de destreza.|
> > |**Frenesí**. Una vez por descanso largo, como acción bonus puedes realizar un ataque cuerpo a cuerpo con tus fauces con tus fauces. Inflige 1d6 + tu modificador de fuerza de daño cortante. Si golpeas a una criatura con este ataque ganas el daño infligido como puntos de vida temporales. |
> > 
## Orcos 
Velocidad: 9 metros
Tamaño: Mediano

| Rasgos |
| --- |
|**Indomable**. La primea vez en un descanso largo que un golpe te dejaría inconsciente te recuperas con 1 punto de vida. |
|**Visión en la oscuridad**. Estás acostumbrado a la luz crepuscular de los bosques y al cielo nocturno. Puedes ver en la penumbra a una distancia de 19.5 metros como si fuera luz brillante y, en la oscuridad como si fuera penumbra. En la oscuridad no puedes distinguir colores, solo tonos de gris.|

> [!column| flex 2 no-title]
> > [!infobox|wfull left]
> > # Athas
> > Los orcos de Athas sobrevivieron en parte a la ayuda ofrecida por Gruumush y en parte a la desestabilización climática provocada por la ascensión de Borys. Cuando el sol oscureció y las arenas sustituyeron al mar una pequeña población de orcos quedó atrapada en las islas situadas sobre el mar de Arena. Durante generaciones perfeccionaron sus barcos y sus habilidad como navegantes, ahora junto a los gigantes son la principal preocupación de los barcos mercantes que tratan de atravesar las arenas.
> 
> > [!infobox|wfull left]
> > # Gruumush
> > La última legión abandonada por Gruumush antes del silencio de los dioses. Los descendientes de los orcos llegados a Athas para combatir en las cruzadas han recurrido a una vida como cazadores de esclavos. Los clanes principales ejerciendo como cazarrecompensas privados para los lores y nobles de las ciudades.

> [!column| flex 2 no-title]
> > [!infobox|wfull left]
> >| Rasgos |
> >| --- |
> >|**Navegante**. Tienes conocimientos sobre barcos de arena y las corrientes del mar de Arena, realizas las tiradas para ubicarte y manejar estos vehículos con ventaja. Ganas proficiencia en supervivencia. |
> >|**Experiencia druídica**. Aprendes el truco Shillelagh. |
>
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Crítico Salvaje**. Tus críticos al atacas son al sacar un 19 o 20|
> > |**Agresivo**. Siempre estas buscando bronca, como acción bonus puede usar la mitad de tu movimiento para acercarte a un enemigo de tu elección. |
> 


## Herencias
> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> > # Aasimar
> > Los Aasimares nacen cuando un celestial se encuentra en las proximidades de un embarazo. Las magia divina de la criatura influye en el no nato yal nacer estos presentan cualidades similares a la de los ángeles. Esto se manifiesta de diversas formas, algunos presentan pares de ojos extra, otros  alas sobre la cabeza y se han registrado casos de cuellos cercenados cuyos cráneos se mantienen unidos al cuerpo por magia.
>
> > [!infobox|wfull left]
> > # Híbrido
> > Los híbridos son el resultado de un experimento druídico fallido. Son pocos quienes han encontrado a un híbrido pero muestran ser una mezcla entra una persona y un animal, algunos presentan rasgos más grotescos como antenas y cuernos, mientras que otros tan sólo han crecido de tamaños y poseen más pelo.
> 
> > [!infobox|wfull left]
> > # Suli
> > Suli es el nombre dado a los mestizos de los djiin en Athas. Debido a sus capacidades de transformación un Djiin puede reproducirse con cualquier especie del planeta, aunque su naturaleza mágica afecta a la criatura. Los hijos tienden a ser considerablemente más grandes que la media y portar un segundo par de brazos.


> [!column| flex 3 no-title]
> > [!infobox|wfull left]
> >| Rasgos |
> > | --- |
> >|**No temáis**. Tu extravagante fisiología resulta aterradora para la mayoría ganas proficiencia en intimidación. |
> >|**Inmunidad divina**. Eres inmune al daño divino. |
> > |**Visión en la oscuridad**. Tus ojos te permiten ver en la penumbra a una distancia de 19.5 metros como si fuera luz brillante y, en la oscuridad como si fuera penumbra. En la oscuridad no puedes distinguir colores, solo tonos de gris.|
>
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> >|**Fisiología animal** Selecciona una de las siguientes entidades y añade un +1 a una de tus características <ul><li>Mamífero, obtienes la dote de garras</li><li>Ave, puedes saltar hasta el doble de tu movimiento</li><li>Insecto, obtienes velocidad de excavación igual a tu movimiento</li><li>Reptil, presentas una cola escamosa eres capas de manejar objetos simples con ella y realizar ataques con ella como si tuvieses al dote de cola.</li></ul> |
>
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> > |**Cuatro brazos** Posees un segundo par de brazos que puede usar para sujetar, armas, escudos y objetos. |
> > |**Habilidoso** Obtienes una acción adicional extra. |

> [!column| flex 2 no-title]
> > [!infobox|wfull left]
> > # Tiefling
> > Los Tieflings nacen cuando un diablo se encuentra en las proximidades de un embarazo. La magia infernal de la criatura influye en el no nato y al nacer estos presentan cualidades similares a la de los residentes del infernos. Esto se manifiesta en la forma de cuernos de ónice emergiendo de sus cráneos y tonos de piel extravagantes..
> 
> > [!infobox|wfull left]
> > # Renacido
> > Los renacidos son personas que han sido revividas mediante el uso de magia profana o druídica, puede haber pasado horas o años entre la resurrección, pero esta ha cambiado su cuerpo de forma permanente.

> [!column| flex 2 no-title]
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> >|**Ascendencia Demoníaca** Eres resistente al daño de fuego. Una vez por descanso corto como acción bonus al recibir daño puedes obligar a la criatura que te ha herido a realizar una tirada de salvación de destreza, la dificultad es igual a 8 más tu modificador de constitución + proficiencia. Las criaturas que no superen la tirada reciben 2d10 de daño de fuego, o la mitad en otro caso. El daño aumenta a 3d10 al nivel 6 y 4d10 a nivel 11.|
> 
> > [!infobox|wfull left]
> > | Rasgos |
> > | --- |
> >|**Resucitado**. Tu tipo de criatura pasa a ser no muerto. |
> >|**Inmunidad nigromántica**. Eres inmune al daño necrótico. |
> > |**Visión en la oscuridad**. Tus ojos te permiten ver en la penumbra a una distancia de 19.5 metros como si fuera luz brillante y, en la oscuridad como si fuera penumbra. En la oscuridad no puedes distinguir colores, solo tonos de gris.|


