### Regiones
Marquis se divide en 4 territorios gestionados por diferentes líderes políticos, a pesar de esto un tratado fue firmado hace 150 años que aseguraba la colaboración de las 4 naciones en cuanto a asuntos de mazmorras, creando la figura del [[Gremio]] como mediador entre las mismas y financiado por todas.
- [[Costa Fría]]
- [[Enbo]]
- [[Forcosia]]
- [[Neveleti]]

Adicionalmente cercano al continente se encuentra [[Inka]]  una nación basada en la necromancia ubicada en el archipiélago norte

### Historia 

### Calendario
Marquis hace uso de un calendario lunar compuesto por 10 meses de 30 días cada uno. El inicio de cada mes es marcado por la aparición de la luna llena y un día comienza al anochecer. 9 de los 10 meses están dedicados a una de las deidades del [[Nonantheón]] con el décimo haciendo honor a la campeona mortal [[Sylvale]].

Actualmente es el Gü de 1047 AG.

Mes | Estación |
---|---|
[[Bràxas]] | Verano |
[[Sunmo]] | Verano |
[[Havstramben]] | Verano |
[[Gü]] | Otoño |
[[Dabuum]] | Otoño |
[[Lamastu]] | Otoño |
[[Fisrum]] | Primavera |
[[Fernamë]] | Primavera |
[[Anamë]] | Invierno |
[[Sylvale]] | Invierno |