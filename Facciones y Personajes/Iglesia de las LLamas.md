---
location: Twii
alegiance: Enbo
leader: Gran Illuminada Ner
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |

## Información 
Órden religiosa formada por [[Svartalfein]] tras su conquista de [[Enbo]], poco después de la guerra divina. Durante miles de años ha operado en Enbo como la religión principal del país y prácticamente la totalidad de la población comparte sus doctrinas. A pesar de los intentos de extenderse, pocos habitantes fuera de Enbo se han convertido a esta fe, aunque los pilares de fuerza y determinación a superar la mortalidad tienen popularidad entre las clases bajas.
La principal meta de los creyentes es alcanzar la Iluminación, un proceso por el cual la carne y la mente mortal transcienden y alcanzan la forma verdadera del ser. Por ahora sólo dos miembros han alcanzado este estatus: [[Sylvale]] y el [[Pintor de Estrellas]]. Quienes alcanzan la iluminación abandonan el plano físico y avanzan a la divinidad. Existen diferentes etapas previas a la Iluminación, siendo una de estas la draconificación proceso por el cuál una humanoide gana las propiedades y características de los dragones. Se dice que [[Svartalfein]] es un caso de draconificación completa mientras que la [[Gran Iluminada Ner]] es una draconificación parcial, conservando su mente y cabeza humana.
