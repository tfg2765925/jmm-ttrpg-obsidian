---
location: Aguas Claras
alegiance: Gremio de Exploradores
race: Elfa
class: Exploradora
age: 78
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Raza** | `=this.race` |
> **Clase** | `=this.class` |
> **Edad** | `=this.age` |
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
Marlow es una elfa de estatura baja, piel morena y pelo corto. Viste con el atuendo de oficial del gremio siendo este una gabardina roja con camisa blanca y pantalones de viaje. Presenta dos prótesis mágicas en la forma de brazos extra que le han ganado el apodo de la arañas de aguas claras.
Miembro de la tríada de exploradores de élite del gremio, Marlow es la más reciente incorporación a esta élite de guerreros. Aprendiz del anterior miembro de la tríada Marlow apuntaba maneras desde joven para alcanzar este puesto, cuando la edad obligó a su maestro a jubilarse Marlow fue recomendada para ocupar su lugar. De sus 5 años de servicio en este puesto destacan su involucración en la batalla de [[Aguas Claras]] y la recuperación del la Lanza del Alba, un artefacto de la guerra divina que se teorizaba que estaba provocando las tormentas de arena en el sur de [[Enbo]].
