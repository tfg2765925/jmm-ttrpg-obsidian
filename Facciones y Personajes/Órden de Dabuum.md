---
location: Ulchiv
alegiance: Marquis
leader: San Chizin
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader`

## Información
Órden de caballeros fieles seguidores de [[Dabuum]], se trata de una organización centenaria fundada por un mercenario cuyo nombre ha caído en el olvido. Aunque la sede de la órden se encuentre en [[Ulchiv]] los caballeros de Dabuum operan sobre todo el continente defendiendo los pueblos y aldeas de las amenazas de monstruos provenientes de las zonas salvajes del continente. Aunque no están afiliados con ningún gobierno de forma oficial, la órden mantiene estrechos lazos con la [[Dinastía Ulfris]] y ayudó durante la guerra civil al usurpador.

Al ser esencialmente una órden de cazamonstruos los caballeros de Dabuum suelen encontrar resistencia en [[Enbo]] y muchos de ellos se niegan incluso a servir a los ciudadanos del rey Dragón. Aunque son populares en los pueblos alejados de los focos de población por la labor de protección muchos señalan que la órden ha abandonado su juramento de proteger al débil debido al su a interferencia y afiliación a [[Anthry Ulfris]], así como sus frecuentes encontronazos con la [[Iglesia de las LLamas]] por diferencia indelógicas.