---
location: Atrapado en la mazmorra de Ilfy
alegiance: Ninguna
race: Diablo (Odio)
class: Monstruo
age: 7631
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Raza** | `=this.race` |
> **Clase** | `=this.class` |
> **Edad** | `=this.age` |
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
Diablo del odio encerrado por [[Gienne]] en la [[mazmorra de Ilfy]]. Tzechozby es un diablo pálido con cuernos de carnero y tres ojos insectodies, posee un cuerpo humano pero está desproporcionado y estirado lo que le otorga una altura de hasta 2 metros y medio. El diablo tiene un punto débil por los humanoides, y ha tratado de ayudarlo a lo largo de los siglos siempre tomando una forma mortal para no levantar sospecha. Durante la Guerra Divina informó a los reinos humanos de forma de herir a las abominaciones y magia de protección contra los hechizos infernales.
Durante el asalto a [[Descenso del Héroe]] ayudó al rey enano avisando de la invasión y permitiendo a los enanos a preparar una contraofensiva, aunque el reino se perdió la invasión no consiguió sus objetivos y cuando Gienne descubrió quién alertó a la ciudad de la invasión aprisionó a Tzechozby bajo la tierra en una de sus iglesias.
