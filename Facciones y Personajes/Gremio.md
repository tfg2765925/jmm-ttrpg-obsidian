---
location: Aguas Claras
alegiance: Marquis
leader: Tríada del Gremio
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader`
## Información
Uno de los principales poderes políticos del continente, la sede principal del gremio se encuentra actualmente en la mazmorra de [[Aguas Claras]] tras su reciente desbordamiento mágico. Se trata de una entidad financiada parcialmente por los gobiernos de [[Marquis]] y las ganancias obtenidas por los exploradores en las mazmorras. El gremio está liderado por un consejo compuesto por 3 antiguos exploradores. [[Marlow Brisaserena]], [[Ilfik Matrillo de Cobre]] y [[Tumerán Filosombrío]] estos tres exploradores forman la élite del Gremio y son quienes se encargan de tomar las decisiones de  desplegar a las milicias del gremio en caso de desbordamiento mágico.
El Gremio investigas las mazmorra y es consciente de la relación de las mazmorras con las [[Ciudades Perdidas]] y el cadáver de [[Nuzrum]]. Las mazmorras se originan debido a la sangre divina derramada durante la gran guerra. La sangre divina corrompe la tierra y transforma las estructuras cercanas, puede permanecer inerte durante siglos pero una acumulación de magia o una disrupción en el sello son capaces de activar sus propiedades engendrando monstruos.
El Gremio trata de contener la expansión de la sangre divina y oculta su existencia del resto del mundo para evitar una conflicto internacional, cuando se descubre una mazmorra es necesario encontrar la cámara original, es entonces cunado se envía a un [[Arcarnista]] para contener la veta de sangre y evitar la expansión de la mazmorra. Pueden existir diferentes vetas de sangre y encontrarse hasta a kilómetros bajo tierra, eliminar la veta calma la mazmorra pero seguirá engendrando monstruos durante varios años en función del nivel de exposición a la sangre despertada.