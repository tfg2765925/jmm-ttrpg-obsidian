# `=this.file.name`
> [!column| flex 2 no-title]
>> [!infobox|wsmall left text-center]
> ># Estadísticas
>> ||
>> :---:|:---:|
>> Nivel | 0 |
>> Tipo | Exploración |
>> Descripción | Un antiguo asentamiento druídico, desatendido desde hace décadas, una naturaleza perversa se ha apoderado de esta arboleda |
>> # Enemigos
>> - [[Bestias]]
>> - [[Defensor Verde]]
>> - [[Dríade Joven]]
>> - [[Guerrero Ghorano]]
>> - [[Pimpollo]]
>
>> [!infobox|wsmall left text-center bg-c-yellow]
>> # Rasgos
>> **Campo de Batalla Tomado por la Naturaleza**
>> Cualquier personaje que supere una tirada de conocimiento(11) para identificar la arboleda puede descubrir los restos de un campo de batalla, los cadáveres de Ghoranos y humanos se encuentran enterrados bajos capas de musgo. En 15+ se devela que uno de los árboles caídos es en realidad un defensor verde muerto, con diversas armas oxidadas clavadas en su costado. Con un 20+ o un crítico se devela que alguno de los árboles son en realidad humanoides transformados por magia en árboles marchitos.
>> ---
>> **No sois Bienvenidos - Acción**
>> Una dríada joven, dos guerreros ghoranos y un número de pimpollos igual al número de personajes entran en escena amenazando a los jugadores de que se marchen y exigiendo una compensación por la trasgresión.
>> ---
>> **Profanar - Miedo - Acción**
>> Gasta un punto de miedo para invocar a un [[Elemental Caótico Menor]], atraído por el conflicto.