# `=this.file.name`
> [!column| flex 2 no-title]
>> [!infobox|wsmall left text-center]
> ># Estadísticas
>> ||
>> :---:|:---:|
>> Nivel | 0 |
>> Tipo | Exploración |
>> Descripción | Un río de amplio caudal y aguas rápidas, lo suficientemente profundo para ahogar a la mayoría de las razas. |
>> # Enemigos
>> - [[Bestias]]
>> - [[Humanoides]]
>> - [[Goblins]]
>
>> [!infobox|wsmall left text-center bg-c-yellow]
>> # Rasgos
>> **Camino Peligroso**
>> Cruzar el río requiere de una tirada de agilidad(10), cualquier criatura que la falle es inmediatamente afectado por la Contracorriente
>> ---
>> **Contracorriente - Acción**
>> Cada personaje que se encuentre en el río debe superar una tirada de agilidad(10) o verse obligado a desplazarse a una distancia corta en la dirección del agua. Los personajes que superen la tirada marcan un punto de estrés de todas formas, o ninguno en caso de éxito.
>> ---
>> **Emboscada Subterránea- Miedo - Acción**
>> Gasta un punto de miedo para invocar a un [[Gusano Gigante]] que emerge de la tierra usando su habilidad erupción sin gastar marcadores.