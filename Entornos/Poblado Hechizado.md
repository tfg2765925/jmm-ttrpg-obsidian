# `=this.file.name`
> [!column| flex 2 no-title]
>> [!infobox|wsmall left text-center]
> ># Estadísticas
>> ||
>> :---:|:---:|
>> Nivel | 2 |
>> Tipo | Misterio/Combate |
>> Descripción | Una poblado tranquilo y a rebosar de vida. Diferentes grupos de guardias patrullan las callejuelas mientras que grupos de mercaderes y aristas se repartenlos diferentes puestos en la plaza. |
>> # Enemigos
>> - [[Humanoides]]
>> - [[Fátula]]
>
>> [!infobox|wsmall left text-center bg-c-yellow]
>> # Rasgos
>> **Algo no Está Bien**
>> Los jugadores llegan a diferentes lugares sin recodar el camino que han tomado para alcanzarlo. Personajes desaparecerán de un día para otro y quienes superen una tirada de conocimiento(14) encontrarán en su bolsillo una nota con su letra que no recuerdan haber escrito.
>> ---
>> **Confabulación - Acción**
>> Cuando un jugador realiza una tirada con miedo generaran un recuerdo inexistente que explique el motivo de las inexactitudes.
>> ---
>> **Gritos en la Noche - Acción**
>> Durante ciertas horas de la noche los jugadores no se ven afectados por Confabulación, en ocasiones se pueden escuchar gritos de auxilio desde la calle. El origen de los gritos es una [[Fátula]] a mitad de devorar a un ciudadano. La Fátula invocará a un número [[Maestro de Batalla]] y un número de [[Mercenario]] igual al de los jugadores +1, e intentará huir.
>> ---
>> **Se Encuentra Entre Nosotros - Miedo - Acción**
>> Gasta un punto de miedo para invocar una de las cabezas de la Fátula que tratará de reducir a un jugador y arrastrarlo a su guarida. Puede liberarse con una tirada de fuerza(14) o cuando la Fátula recibe daño grave o mayor.