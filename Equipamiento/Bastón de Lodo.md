---
price: 1 montón de oro
trait: Conocimiento
range: Larga distancia
damage_type: mágico
burden: 2 manos
score: d8+2
---
# `=this.file.name`

> [!infobox|wfit center]
> 
> | |
> ---|---|---|---|
> **Precio** | `=this.price` | **Tipo de Daño** | `=this.damage_type` |
> **Habilidad** | `=this.trait` | **Rango** | `=this.range` |
> **Manejo** | `=this.burden` | **Dado/Armadura** | `=this.score` |

## Descripción
Bastón de piedra con una bola de lodo rezumante en la punta. 

## Rasgo
Al realizar un ataque con este arma los enemigos medianos han de superar una tirada de destreza DC 12 o no poder moverse durante un turno. Los loditos son débiles a los ataques del bastón.