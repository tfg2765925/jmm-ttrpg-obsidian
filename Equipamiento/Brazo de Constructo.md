---
price: 1 montón de oro
trait: Fuerza o Instinto
range: Cuerpo a Cuerpo
damage_type: Mágico
burden: 2 manos
score: d10+2
---
# `=this.file.name`

> [!infobox|wfit center]
> 
> | |
> ---|---|---|---|
> **Precio** | `=this.price` | **Tipo de Daño** | `=this.damage_type` |
> **Habilidad** | `=this.trait` | **Rango** | `=this.range` |
> **Manejo** | `=this.burden` | **Dado/Armadura** | `=this.score` |

## Descripción
Brazo descartado de un constructo mediano, emite un ligero zumbido eléctrico.

## Rasgo
1 vez por día: gasta un punto de esperanza para activar un campo eléctrico. Todas las criaturas enemigas se encuentran vulnerables durante la siguiente ronda