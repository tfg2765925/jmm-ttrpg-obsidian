## Acto 1
>[!recite]+
>En un bosque petrificado de Costa fría alejado de las miradas humanas reside un sociedad diminuta de hadas. Que ha establecido su poblado en el tronco de un árbol féerico protegido por una ilusión. El perímetro del pueblo está vigilado constantemente por 3 colosos de piedra y madera que se encargan de protegerlo, y es así como esta sociedad secreta ha sobrevivido durante miles de años, hasta una fatídica tarde de verano.
>Vuestro grupo se ha detenido bajo las raíces de un árbol a medio desenterrar, lleváis persiguiendo a vuestra presa durante horas y el rastro muestra que está cansada y herida, pero ha abandonado los límites de patrulla de los colosos. Allí fuera no estáis protegidos de las amenazas mayores, pero volver atrás implica retornar con las manos vacías y con menos equipamiento.

>[!recite]+ Caza
> Seguís el rastro de sangre sobre los montones de hojas secas,  a lo lejos observáis a vuestra presa, un hurón. Con la espalda repleta de de lanzas y flechas la bestia sigue de pié pero a duras penas. Le veis escarbar bajo un tronco, en un intento de crear un refugio improvisado para lamerse las heridas.

```encounter-table
name: Cacería
creatures: 
  - 1: Hurón Gigante, 10, 14
```

En función del tiempo que tarden en cosechar las partes de hurón un grupo de humanoides una elfa y un ribbit, llegarán a la escena. Tienen una percepción de 15 y al ver a las hadas tratarán de apresarlas. Si son descubiertas les perseguirán hasta el rango de los [[Colosos]] donde los interceptarán y matarán.

## Acto2
>[!recite]+
>Llegáis al paseo que marca la entrada a [[Pueblo Añejo]], un elaborado sendero de arcos de madera y decorados con flores para el festival verde. El sendero desemboca en una muralla de setas con una apertura ante ella una aburrida hada de color morado vestida con una túnica azul y con el pelo rizado recogido en un moño alto os  recibe.
>"Papeles y lista de objetos por favor".
>"Bienvenidos a Pueblo Añejo disfruten del Verde" - dice antes de lanzar un puñado de confeti al aire y volver a su expresión desinteresada.
>Tras pasar la muralla la ilusión que oculta al pueblo desaparece permitiéndoos ver una bulliciosa metrópolis construida a los pies del un descomunal tronco féerico. Centenares de edificios construidos en su mayoría con madera petrificada y tejados de tréboles, se alzan varios hasta casi un metro de altura. Cientos de hadas voladoras saltan de una ventana a otra, preparando las decoraciones para el festival y por la calle principal avanza un carromato tirado por dos ratones de campo. La calle desemboca en la plaza un hundimiento en la tierra reforzado con baldosas de piedra. En su centro se está construyendo un escenario improvisado.

Dejarlos rolear un rato y vender las carnes y dientes recuperados si los tienen, al llegar la noche comienza una ventisca inesperada.

>[!recite]+
>Te despierta el ruido del viento, la temperatura de tu habitación ha bajado hasta el punto que apenas sientes tus extremidades por la ventanas de tu piso ves que se están colando granizo y hielo y montoncitos de nieve comienzan a acumularse en el suelo. El exterior es una caótico sin fin de hadas en ropa de noche, algunas voladoras siendo zarandeadas por los vientos y desapareciendo en la ventisca. En el suelo la luz del un candil revela a un guardia tratando de escoltar a una muchedumbre. Lentamente la columna avanza hacia el centro del pueblo hacia el refugio situado en el tronco del árbol.
>Los supervivientes se han apilado como han podido en las estrechas estancias excavadas dentro de la tierra, incluso a cubierto el silbido del viendo es estremecedor, guardias y magos corren apresurados por los pasillos tratando a los heridos y trayendo nuevas hadas. Escuchas a un grupo que se está congregando en la habitación cercana, la habitación es extensa el techo compuesto por las raíces del árbol e iluminada por orbes de colores, en el centro sobre una piedra se encuentra [[Ludvick Fir]] el presidente del [[Consejo Verde]].
>"Sé que nos encontramos ante una catástrofe sin precedentes hermanos y hermanas pero debemos conservar la calma ante todo.  Aún desconocemos el origen de este desbordamiento mágico pero estamos trabajando en asegurar que el mayor número de amigos y familiares alcancen este refugio, os pido paciencia y comprensión ante esta situación. No podemos superarla sólo debemos estar unidos o este bien podría ser el fin de nuestro pueblo. Aquellos que sean capaces de emplear magia regenerativa por favor acudan al ala este y el resto transporten con cuidado a los heridos. Además necesitamos formar grupos de expedición al exterior para traer más supervivientes aquellos con el coraje necesario para esta tarea por favor vengan a verme al ala norte."

Permitir que la party se reencuentre aquí o antes durante el incidente.

>[!recite]+
>Gracias por venir, veo que sois los cazadores que  estuvieron vendiendo las partes del hurón ayer en la plaza, tengo un favor que pediros. Lo que habéis escuchado ahí fuera es sólo una parte de la verdad, soy consciente del motivo de este desbordamiento mágico: la Daga del Invierno una reliquia de la [[Guerra Divina]] que fue ocultada en un santuario cercano por la campeona mortal [[Sylvale]]. Parece ser que los años han provocado que el encantamiento se descontrole o peor alguien la ha activado. La situación es incluso más desesperada de lo que parece la daga es también un disruptor mágico, los rituales que mantienen la cúpula y permiten a los colosos moverse han sido disipados. Actualmente el pueblo está sin protección es sólo cuestión de tiempo que las razas altas den con nosotros o alguna bestia de los bosques. Necesitamos que alguien alcance el santuario y desactive la daga, bastaría con este encantamiento. Los guardias están demasiado ocupados buscando supervivientes y espantando criaturas del bosque y no podemos permitir que esta información salga a la luz o cundirá el pánico, os imploro que aceptéis esta tarea.

Cuando aceptan Ludvick les teletransporta a las afueras de [[La Vergüenza del Hechicero]].

## Acto 3

>[!recite]+
>Os veis transportados a un camino de piedra conduce a la entrada del templo dos hileras de obeliscos conducen hacia la entrada del templo. Una estructura de piedra compuesto por un único piso que se asemeja a una pagoda invertida. Sorprendentemente la temperatura aquí es más agradable que en el refugio, aunque los cadáveres congelados de 3 hadas indican que no siempre ha sido así. Mirando al cielo tras las copas de los árboles comprendéis el motivo, la tormenta se ha originado en este templo y el ojo del huracán está situado justo encima.

La puerta principal está bloqueada al intentar abrirla una voz desde el interior preguntará al grupo quiénes son.

### La Vergüenza del Hechicero P0

![[lvh-p0.png]]


#### P0-1 Cofre trampa
>[!recite]+ Cofre Trampa
> La sala parecer un antiguo laboratorio en el centro se encuentra una mesa de madera con diversos vasos precipitados, un hada vestida con túnica parece estar trabajando en algo traspasando un líquido verde hacia un vial.

```encounter-table
name: Laboratorio
creatures:
 - 3: Hada Guerrera
 - 2: Hada Hechicera
```
Una de las hadas porta una barra de metal terminada en 3 dientes no alineados
>[!recite]+
>En la habitación de al lado reposa un cofre de piedra cerrado con un candado.

El cofre contiene una trampa de virotes al abrirlo es necesario que las criaturas en frente del mismo superen una tirada de destreza con DC 16 o reciben `dice: 3d10` de daño físico.

#### P0-2 Descenso al nivel inferior
>[!recite]+ Descenso al nivel inferior
>La sala contiene un hueco de ascensor con una cuerda que asciende hasta el techo. Al lado del agujero se encuentra un mecanismo para activarlo pero le falta la palanca.

#### P0-3 Trampa de fuego
>[!recite]+ Trampa de fuego
>La estancia es una sala cuadrada con puertas en cada una de sus paredes, a ambos lados de cada una de ellas hay un estandarte raído y  con la runa de fuego dibujada en blanco sobre el mismo. La habitación parece estar vacía a primera vista. A excepción de una mesa de piedra en la que reposa un cáliz dorado

Esta estancia tiene una trampa, una tirada de percepción DC 16 revelará que el polvo de las paredes ha desaparecido pero en el centro de la estancia. Entrar al centro de la estancia activa los lanzallamas requiriendo de una tirada de destreza con DC 12 para recibir la mitad de daño `dice: 6d6` de fuego.

#### P0-4 Puzle giratorio
>[!recite]+ Puzle giratorio
> Esta habitación contiene un pilar de piedra  de unos dos palmos de grosor elevándose sobre un 3 círculos concéntricos, cada uno de los círculos tiene una flecha apuntando en diferentes direcciones. En la pared oeste de la estancia, a la que apunta una de las flechas podéis ver una incisión en la piedra. Reposando en una esquina hay un palo alargado con diferentes incisos. pero parece que el tope está roto.

#### P0-5 Mosaico de la guerra divina
>[!recite]+ Mosaico de la guerra divina
>Las puertas se abren para revelar un cuartillo oscuro, al encender la luz veis que las paredes representan loas acontecimientos que previos a la guerra, y su desenlace. En una pared se puede ver como los dioses descienden al planeta, aquellos que hacen contacto quemando la tierra bajo sus pies. Otra muestra una figura humanoide alzando a otro más pequeño. La tercera muestra un campo de batalla gigantes luchando entre ellos y pequeñas criaturas batallando monstruos en el suelo. La última imagen representa a Sylvale alzando su espada a los cielos mientras 9 gigantes ascienden.
>En el suelo se puede ver una mapa de este piso.

#### P0-6 Constructo roto
Estancia secreta que se desvela al activar una piedra específica en el pasillo

>[!recite]+ Constructo roto
>La pared se abre para revelar un pequeño taller, en él un imponente robot de piedra y madera reposa colgado del techo inerte.

El constructo se puede despiezar para obtener un [[Brazo de Constructo]]

#### P0-7 Entrada
>[!recite]+ Entrada
>La puerta se abre hacia una estancia de piedra, sujetada por cuatro pilares con estatuas de hada sujetando el techo. En la sala residen 4 hadas armadas y vestidas con armaduras similares a la de los guardias pero con una capa roja sobre su hombro izquierdo. Al fondo de la estancia veis un pasillo iluminado por luces mágicas que termina en una estancia con dos puertas de hierro una en la pared norte y otra en la pared oeste.

```encounter-table
name: Entrada
creatures:
 - 4: Hada Guerrera
```

La puerta norte está bloqueada pero la este no, la llave de la puerta norte se encuentra en una de las hadas guardando la entrada.

#### P0-8 Hada herida
>[!recite]+ Hada herida
>La temperatura desciende considerablemente al entrar al pasillo, a la derecha una luz cálida se filtra bajo la puerta. La lo que debió ser antes una cocina está completamente destrozada, la mesa central partida por la mitad y los fuegos congelados reposando contra una pared se encuentra un hada, con la apariencia de una mantis religiosa. Está actualmente ocupada sujetando una tela ensangrentada contra su torso.

El hada se presentará como [[Harrentha]], la segunda al mando del [[Escuadrón de Destrucción de Humanos]], ha sido atacada por espectros de escarcha y casi no sobrevive. Es leal a [[Sombrero Rojo]] pero no está dispuesta a sacrificar a su especie para vengarse. Está protegiendo la manivela que activa el mecanismo para descender al piso inferior.

### La Vergüenza del Hechicero P-1

![[lvh-p-1.png]]

#### P-1-1 Restos de los disidentes
>[!recite]+ Restos de los disidentes
> Una macabra escena os espera al entrar, los cuerpos de 4 hadas vestidas con las armaduras del EDH se han apilado de forma caótica en el centro de la estancia. La sangre de los cuerpos se ha acumulado y el charco ocupa prácticamente todo el suelo.

#### P-1-2 Entrada a la daga
>[!recite]+ Entrada a la daga
> Un portón de piedra ocupa el centro de la pared opuesta, está decorada con preciosas columnas de roca tallada todas representando a una hada sujetando el techo.A ambos lados de la estancia dos hadas portando arcos y montadas sobre ratones montan guardia.

```encounter-table
name: Entrada a la daga
creatures:
 - 2: Jinete de Ratones
```

#### P-1-3 Ascenso al piso superior
>[!recite]+ Ascenso al piso superior
> El ascensor desciende varios minutos hasta una salita de piedra circular iluminada por 6 luces flotantes. Frente a a vosotros se extiende un pasillo oscuro.

#### P-1-4 Encuentro con los espectros
>[!recite]+ Encuentro con los espectros
>A medida que avanzáis por el pasillo la temperatura desciende, una luz azulada proviene del final de mismo y escuchais el leve silbido del viento. Al entrar os veis sorprendidos por 3 espectros azulados  flotando sobre una fuente congelada, lo que debió ser antes un jardín de setas es ahora un páramo helado.

```encounter-table
name: Encuentro con los espectros
creatures:
 - 4: Espectro de Escarcha
```

## Acto 4
>[!recite]+ La Daga del Invierno
>Debloqueais el portón entrando a un patio subterránea emergiendo varios centímetros de la tierra se encuentra la Daga del invierno, brillando con fuerza con luz pálida. Unas escaleras conducen a una terraza hacia el centro de la hoja, sobre la misma se encuentra un hada vestida con armadura pesada, y una capa de general que oculta sus brazos izquierdos, a ambos lados dos hadas portando una indumentaria similar a las que os habéis encontrando por la mazmorra desenvainan sus sables.

```encounter-table
name: La Daga del Invierno
creatures:
 - 1: Sombrero Rojo
 - 2: Hada Guerrera, 5
```

