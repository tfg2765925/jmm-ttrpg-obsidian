Aventura de inicio a Daggerheart traducida y adaptada por mí.

>[!recite]+
>[[Marlow Brisaserena]], miembro del [[Consejo de Tres]] del [[Gremio]] de exploradores os ha contratado para entregar una caravana de provisiones y una extravagante y pesada caja, el contenido de la cuál no ha mencionado. Las provisiones están dirigidas para la base del Gremio situada en el [[Bosque de Piedravía]], mientras que la caja pesada está dirigida a un figura a la que sólo se describe como el Arcanista Blanco. Se os ha entregado un mapa, una caravana y una promesa de pago en cuanto lleguéis a Piedravía.

## Acto 1: La Caravana Mercante

>[!recite]+
>Tras días de viaje vuestra caravana ha llegado a la linde del bosque de Piedravía. Una arboleda compuesta por colosales robles se extiende de este a oeste, al dirigir la caravana por la estrecha carretera de piedra os despedís del cielo azulado de esta tarde de verano y os veis sumidos en una penumbra permanente provocada por las copas de los árboles. Se escuchan los curiosos sonidos de los híbridos del bosque, graznidos guturales de las sapo-grullas, el apresurado batir de alas de un conibrí y una familia de zorriélagos  que escapa a las copas de los árboles al acercarse la caravana.

Dejar que se describan uno a uno.

>[!recite]+
>Al realizar un giro brusco vuestros caballos se detienen al ver a unos metros de distancia un carruaje volcado sobre la carretera. Frutas y verduras están descartadas por el suelo y tras un lateral asoman dos piernas humanoides. Tras la el carro se alza una criatura de plumaje plateado, su pecho cubierto de sangre y de su pico cuelgan resto de carne fresca. La criatura es un híbrido cuadrúpedo entre una lechuzo y un lobo, se alza sobre sus cuatro patas alcanzando los dos metros de altura y os observa atentamente, con sus alas extendidas.
>Percepción(15) dos cría de lobuzo se esconden tras la patas de su madre.

```encounter-table
name: La Caravana
creatures: 
  - 1: Lobuza, 3, 10
```

## Acto 2: Emboscada

>[!recite]+
>De la nada aparecen 3 figuras saltando entre las ramas de los árboles, dos de ellas se quedan tras el vagón mientras que la última realiza una voltereta sobre el mismo y se lanza a la carrera contra el conductor. Al estar más cerca las figuras revelan ser Ribbits de distintos colores, todos encapuchados y protegidos con una armadura de piedras negras.

```encounter-table
name: La Emboscada
creatures: 
  - 3: Emboscador, 3, 13
  - 1: Ladrón, 3, 14
```

## Acto 3: Llegada a Piedravía

>[!recite]+
>El camino continúa entre los árboles hasta llegar a dos pilares de piedra que se lazan varios metros sobre el suelo. Inscritos en ellos podéis ver diversos grabados que los más estudiosos entre vosotros identificarán como runas enanas. al pasar entre los sentís una ligera fuerza que empuja hacia atrás, a medida que los caballos avanzan esta fuerza aumenta hasta que se produce un sonoro pop y vuestro cuerpo avanza hacia adelante. Mirando atrás podéis ver que el aire entre los pilares parece ondulas como si se tratase de la superficie de un lago.
>Más adelante los árboles dan paso a una amplia explanada en la que se encuentra el templo principal. A sus pies podéis ver diversas chozas, no se ve a nadie por las calles de la ciudad, colgando de gruesas cuerdas veis 2 edificios cilíndricos construidos con madera y techados con arcilla rojiza. La primera estructura es considerablemente más grande y presenta dos pisos y varias ventanas, la segunda se asemeja más a casa humilde, aunque presenta un desagradable color negruzco como si hubiese recibido el impacto de una bola de fuego.

>[!recite]+
>En la base de la pirámide se encuentra un humano con pelo corto y negro, una nariz prominente y una terrible cicatriz que va serpentea el hombro derecho hasta el ombligo. Viste con una pantalones marrones, unas botas de viaje y no parece llevar armas.

```encounter-table
name: Extraño
creatures:
 - 1: Avatar de Nuzrum, 10, 13
```
Durante este combate el avatar no puede morir, encaso de que se quede sin vida este se recupera inmediatamente al principio de su turno.

>[!recite]+
>Un humanoide con rasgo de insecto desciende de los cielos y se interpone entre el extraño y vosotros, os grita: "rápido debemos huir ahora mismo no podemos matarle!" antes de agarrarte y comenzar a alzar el vuelo hacia una de las casas colgantes.

El insectoide se presentará como el Arcanista Blanco. una hada versada en la magia antigua que contactó con el gremio tras una reciente agitación de la mazmorra. El humano que han encontrado antes resulta ser uno de los exploradores enviados por el gremio, durante una incursión algo en el interior de la mazmorra ha tomado su cuerpo y ahora mismo deambula por el pueblo atacando a cualquier cosa que encuentra.
El anterior aventurero se ha convertido en una forma muy rara de no muerto, su alma se encuentra anclada en el interior de la pirámide, y no importa el daño que reciba nunca morirá realmente.  En la primera incursión el aventurero perdió la vida y su cuerpo mutó. Durante la primera semana el resto del grupo lo mantuvo prisionero en el interior de la mazmorra mientras esperaban refuerzos para lidiar con él pero hace un día logro escapar.
El cargamento de la caravana contiene un potente elixir mágico que permitirá crear un disruptor entre la pirámide y el cuerpo permitiendo que en una área reducida sea posible matarle.

Preparar el ritual les llevará unas horas les invita a descansar con el resto de evacuados en la taberna de al lado y prepararse para la noche.

## Acto 4: Exorcismo

>[!recite]+
>La luna llena se justo encima sobre la pirámide los preparativos del ritual están casi listos el arcanista se encuentra ahora mismo usando los últimos resto de elixir para pintar las runas en la piedra. Cuando termina se limpia el sudor de la frente y se dirige a vosotros: "SI hacemos algos lo suficientemente vistoso deberíamos llamar su atención y él vendrá. Durante el ritual no podré ayudaros pero mientras se encuentre dentro del círculo no debería poder revivir. Os deseo suerte, cuando estéis preparados haré una señal para atraer a la criatura."

```encounter-table
name: Batalla en la Cúspide
creatures:
 - 1: Avatar de Nuzrum, 10, 13
 - 3: Esqueleto, 2, 12
```