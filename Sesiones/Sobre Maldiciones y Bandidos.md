## Acto 1
Los protagonistas llegarán a [[Luel]] contratados por un mago reconocido de la ciudad que ha encontrado un misterioso espécimen de oso con un siniestro ojo amarillo de gran tamaño emergiendo de su cabeza. El mago quiere encontrar el origen de esta aflicción ya que está convencido que ha sido creada por magia y ha escuchado historias de otras criaturas similares en el [[Paso Angosto de Jogerth]].

## Escena 1: Torre de Magia

## Escena 2: Barril Encantado

## Escena 3: Hospital de Luel

## Acto 2
En el acto dos los protagonistas se adentran en el paso, si han contratado un guía se eliminan los encuentros aleatorios 7 y 8 y no marcan puntos de estrés al avanzar.

 