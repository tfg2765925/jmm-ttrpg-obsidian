## Acto 1
>[!recite]+
> [[Ulchiv]] es una imponente ciudad amurallada construida en el corazón de [[Neveleti]]. Dos gremios de comercio controlan todas las exportaciones de la metrópolis, [[La Compañía Brava]] gestiona los recursos de monstruo y arqueológicos extraídos de las mazmorras bajo la ciudad así como el comercio de objetos mágicos. [[El Conglomerado Turquesa]] gestiona las explotaciones mineras, madereras y marítimas de Ulchiv. Durante años ambas han luchado una guerra secreta por el control de la ciudad. 
> Hace 3 días fueron descubiertas unas vasijas de incalculable valor en la [[Mazmorra Norte de Ulchiv]],  estas vasijas fueron vendidas secretamente a la [[Iglesia de las LLamas]], sin embargo, Ymir el tasador contratado por la Compañía Brava y encargado de realizar la entrega ha aparecido muerto. No queda rastro de las vasijas y los agentes del conglomerado turquesa son los principales sospechosos.
> Habéis sido contratados por las cabezas del Gremio para recuperar las vasijas y entregarlas al barco mercante con el nombre de [[El Unicornio]] que llegará a puerto en 4 días, vuestros jefes os han indicado este incidente no puede ser conocido por el público y mucho menos las autoridades, sin duda el Conglomerado tiene intenciones similares puesto que las noticias de las vasijas no han salido a la luz.
> Os encontráis en el círculo exterior de la ciudad, tenéis órdenes de reuniros con un agente de la Compañía en [[El Barril de Fjord]], donde se os darán más detalles sobre los acontecimientos, la nota indicaba que el hombre al que buscáis es un goblin pálido.

El goblin les suministrará la siguiente información.
- El tasador estaba viviendo en la habitación D del conglomerado de viviendas situado en la zona este del círculo exterior.
- La compañía tenía agentes haciendo guardia fuera, la noche del asesinato no entró ni salió nadie por la puerta principal pero un hombre descamisado y un simian se pelearon en la parte trasera antes de anochecer.
- El tasador murió unas horas después del anochecer, antes de esto no salió nadie, pero entraron una elfa y una pareja humana.
- Desde el asesinato no se ha producido ningún movimiento extraño por lo que se cree que las vasijas siguen dentro.
- La muerte se ha registrado como un suicidio.
- El cadáver del tasador afirma no haber visto a nadie debido a que las luces estaban apagadas.
- Al tasador lo mataron rompiéndole el cuello.

### Personajes del misterio

| Personaje | Descripción |
| ---- | ---- |
| Soria la Regente | Una anciana elfa, viste con un traje rojo que termina en una falda  acolchada de la cuál sobre asoman dos botas de viaje con puntera. Va todos lados con un sombrero vaquero con el ala absurdamente grande. |
| Unmi el Gladiador | Un ursutropo que se gana la vida en la [[Arena de Ulchiv]] como luchador, siempre viste con sólo un par de pantalones raídos y unas botas reforzadas con placas de metal, todo su torso está cubierto por cicatrices. Su brazo derecho ha sido sustituido por una prótesis. |
| Loybi el Bibliotecario | Un hombre polilla de pelaje blanco con tintes dorados presenta la melena del cuello rapada y carece de antenas viste con una camisa sencilla y unos pantalones de viaje, siempre atada a la cintura una chaqueta. Es el hermano pequeño de Ril |
| Ril el Guardia | Un Simian que porta una armadura de cuero, con refuerzos de hierro. Posee una impresionante complexión física e incluso para la raza de los hombres bestia demuestra una altura descomunal. todo el tren superior está expuesto a excepción de una fada de tiras y unas espinilleras de metal. |
| El Tasador | Un anciano humano vestido con ropajes simples que cubre con una gabardina acolchada, porta siempre una descomunal mochila que utiliza como puesto improvisa. Presenta unas gafas de culo de vaso y el paso del tiempo se ha llevado su pelo portando ahora una bien acicalado rodapies |
| Ronga la Minera | Una mujer rana entrada en carnes que viste el atuendo estándar de minero de Piedravia un mono caquis sobre una camisa blanca y botas reforzadas |
| Ernie la Asistente | Mujer humana de unos 20 años, es la asistente de Soria y viste exactamente igual que la alcaldesa a excepción del sombrero y los tacones que ha sustituido por una bailarinas |
| Buun el Carnicero | El padre del Unmi, un hombre que viste con una camisa simple un delantal con una jamón pintado y un par de pantalones cortos que mantiene en su sitio con un cinturón del que cuelga diversos cuchillos enfundados |
| Firi la tabernera | Una semidemonio humanoide de pequeño tamaño que presenta un plumaje vistoso y reluciente que combina con un top ajustado y pantalones de campana. Es la mujer de Gunther y regentan el bar |
| Gunther el tabernero | Un humano de piel morena y pelo largo recogido en una coleta alta presenta una complexión corpulenta similar a la de un competidor de halterofilia. viste con una camiseta blanca siempre manchada de sudor y comida y unos pantalones de campana que mantiene limpios mediante una cuerda de gran grosor que usa a modo de cinturón |

### Pistas

El bibliotecario ha robado las vasijas, planea echarle la culpa a su hermano el jefe de la guardia. 

- El robo se produjo a media noche, nadie vio nada pues no había ninguna antorcha encendida.
- El mercader escuchó pisadas metálicas en su puerta antes de que alguien la tumbase y entrase
- Las vasijas están en la casa del guardia en un sótano cerrado con llave
- El guardia no sabe leer
- La casa donde se hospedaba el mercader estaba recogida en un libro accesible sólo a la alcaldesa, el mercader y el guardia
- El hermano a pesar de ser una polilla es extremadamente fuerte 
- El hermano tiene conocimiento del valor de las vasijas debido a su trasfondo.
- De ser investigado el bibliotecario confesará que habló con su hermano sobre el valor de las vasijas, olvidándose de que no sabe leer e insinuando que él tenía acceso a los documentos
- Firi Y Gunther quieren dejar atrás Ulchiv pero no tienen los fondos suficientes para empezar de nuevo en otro sitio
- Buun está preocupado por su hijo y comenta que lo ha visto más violento últimamente.
- Ernie la asistente ha podido acceder a los documentos

## Acto 2
 El bibliotecario quiere huir con las vasijas, las vasijas permiten crear pequeños monstruos controlados por el usuario. Tiene un control casi completo del hermano al que ha utilizado como tapadera y pretende hacer que cargue con el asesinato. 
 Otro grupo de investigación contratado por el conglomerado está investigando las vasijas también.
```encounter-table
name: Piso de Loybi
creatures:
  - 1: Invocador, 6, 13
  - 2: Diablillo Menor, 2, 14
```