## Acto 1

> [!recite]+
> Os encontráis sentados en un furgón de madera, vuestro movimiento restringido por grilletes de acero que os conectan a otro individuo sentado a vuestro lado. Escucháis el traqueteo de las ruedas contras la calzada y los cascos de los caballos. Es en este momento que percibís lo que parece ser una lluvia ligera sucediendo en el exterior.

Dejar que describan al personaje y el motivo por el que fueron capturados.


> [!recite]+
> Escuchas un golpe contra la pared seguido de diversos golpes ligeros como si alguien estuviese clavando un cuchillo contra las paredes del furgón. En el techo unos pasos salpicar contra los charcos formados por la lluvia. Tras un minuto un cuerno de guerra rompe el silencio seguidos de gritos provenientes del exterior. Se escucha un forcejeo y a los caballos relinchar. El furgón comienza a moverse de forma errática y una masa de metal traviesa parcialmente la pared norte. Finalmente el furgón vuelca.


> [!recite]+
> Salís el accidente y descubrís un claro otro furgón como el vuestro ha volcado y veis un tercero alejarse a toda velocidad en la distancia. Entre los restos uno de los caballos que ha sobrevivido se levanta y sale corriendo hacia el bosque. Entre la linde veis las figuras de varios Goblins preparándose para el combate. Del otro furgón emerge un humano con un brazo descolocado todavía firmemente abrazado a un hacha de leñador. Los conductores de vuestro carruaje también se han recuperado y se encuentran en mitad de estrangular al un goblin encapuchado

```encounter-table
name: Primer Combate
creatures:
  - 1: Goblin Sacerdote, 4, 14
  - 3: Goblin Bandido, 2, 14
  - 1: Soldado Lodocito, 6, 16
  - 4: Marioneta Lodocito, 3, 14
```

## Acto 2

> [!recite]+
> Os abrís paso por el camino hasta llegar a la entrada del pueblo señalada por un cartel de madera que lee [[Ilfy]]. Un muro de piedra de poca altura se extiende a ambos lados de la señal y dos aldeanos parecen mirar al cielo con las bocas abiertas. El interior de la aldea está plagada de humanoides que vagan formando un cúmulo desordenado de personas sobre una colina, en la lejanía podéis observar una hacienda, las ventanas del piso superior desvelan que la alguien ha encendido una luz. A los pies de la hacienda hay una concentración de unos veinte o treinta humanoides algunos vestidos con ropas de humildes y otros con armaduras.
> DC 12 Percepción: A los pies de la colina veis una figura moviéndose a gran velocidad, al acercarse a un edificio de piedra saltando por una ventana y desaparece en el interior.

>[!recite]+ Armería
>El interior de la estancia muestra un taller desordenado con herramientas y armas a medio hacer desperdigadas por el suelo. En el centro de la estancia se puede un joven humano [[Anthan]] forcejear con un dracónido corpulento. El dracónido lo agarra por el cuello y lo estampa contra la mesa, el humano entonces, grita "LO SIENTO" antes de sacar un cuchillo de su zurrón y cercenarle la mano. Entonces le propina una patada y os dirige una mirada antes de salir corriendo por la puerta trasera.

```encounter-table
name: Armería
creatures:
  - 1: Soldado Lodocito, 6, 16
```

>[!recite]+ Hacienda
> El interior de la hacienda está decorado con diferentes cuadros y esculturas de [[Anamë]]. las ventanas interiores que contiene un pequeño jardín con una fuente seca, en el jadín se encuentran dos humanos y un semidemonio mirandoos fijamente sin moverse. Las dos escaleras están bloqueadas por barricadas formadas por muebles y estatuas, abajo se puede ver a un par de infectado intentando sin suerte desbloquearlas. 
> Se os conduce hacia el dormitorio principal en el cuál se encuentran otros 5 superviviente [[Nurgen]] y dos firbolgs, un elfo herido y una Goblin cuidando de sus heridas. Nurgen se encuentra pálido y sudoroso tendido en su cama con sus manos juntas recitando un encantamiento. [[Anthan]] se adelanta:
> "He encontrado a estas personas en el exterior dicen que los pringosos los han traído en un carro pero no los han infectado."
> Nurgen explicará que está manteniendo un encantamiento sobre la entrada a la mazmorra de la que emergieron los lodocitos por primera vez, ha deducido que lo que sea que utilizan para controlar cuerpos se encuentra en el interior.

>[!recite]+ Montón de basura
> Las marcas de ruedas conducen a un establo donde se han apilado dos montones de objetos, uno parece estar compuesto de pertenencias mundanas ropa, maletas, etc... y otro en el parece que se han apilado armas, armaduras y amuletos. A su lado se encuentra un caballo marrón que las observa de detenidamente.
> Al acercaros el caballo gira su perfil haca vosotros, su cuello se hincha y aparecen dos protuberancias parecidas al fango. Entonces habla con voz profunda:
> Maa-magos... Ayudarnos... liberarnos... Tomar oro... no-norte madre espera

```encounter-table
name: Establos
creatures:
  - 2: Caballo Lodocito, 6, 14
  - 3: Lodocito Menor, 2, 14
```

## Acto 3 

>[!recite]+
> Os dirijis hacia la gruta descrita por Nurgen, con la atención de los aldeanos centrada en la Hacienda escapar hacia el exterior es tarea sencilla. Andáis durante horas por el bosque hasta llegar a un claro artificial, cubierto de cadáveres de goblins al norte una gruta de piedra blanca emite un brillo dorado. Podéis ver a 2 humanoides cubiertos de bultos plateados guardando la entrada.

```encounter-table
name: Primer Combate
creatures:
  - 2: Soldado Lodocito, 6, 16
  - 5: Lodocito Menor, 2, 14
```

Los lodocitos menores pueden tomar el control de los cadáveres de goblin convirtiéndose en Goblin Bandido

### Interior de la Mazmorra
![[mazmorra_de_ilfy.png]]

#### 1. Entrada
>[!recite]+
> La gruta desciende hacia una estancia pequeña iluminada por un candil mágico, en el suelo se encuentran varias herramientas de leñador y dos mochilas con ramas secas. en la pared norte se encuentra una verja de hierro cerrada con el cerrojo hecho añicos. En la pared este una entrada que desemboca en otra habitación.

Esta estancia no tiene nada a parte del equipamiento dejado atrás y el candil.
#### 2. Altar de Gienne
>[!recite]+
>La estancia contiene un altar cubierto de una sustancia marrón , el único elemento de interés es una estatua representando a un conjunto de cuerpos formando una amalgama caótica.

Se trata de un altar para [[Gienne]] Deidad de las abominaciones. beber el Líquido toma 1 punto de vida, por cada dos puntos tomados otorga un modificador de +2 a los ataques durante la siguiente hora.
#### 3. Nido Lodocito
>[!recite]+
>Una habitación grande completamente a oscuras, en la cuál se escucha un goteo constante, al entrar se escucha un gruñido y un aldeano a medio descomponer se alza contra vuestro grupo.

```encounter-table
name: Nido Lodocito
creatures:
  - 1: Marioneta Lodocito, 3, 14
  - 6: Lodocito Menor, 2, 14
```

Tras el combate uno de los montones de lodo se disipa revelando un cofre de piedra que contiene un [[Bastón de Lodo]].
#### 4. Prisión
>[!recite]+
>Tras una verja de piedra se encuentra una minúscula estancia oscurecida al acercar el candil se revela que en el interior hay un [[Diablo]]
>
Se presentará como [[Tzechozby]], no revelará el motivo de su encarcelamiento pero promete ayudar al grupo si le liberan, indica que hizo algo para enfadar a Gienne pero no indicará el qué. Si le liberan le concederá al grupo una armadura mágica que les otorga otra ranura de armadura a cada uno

#### 5. Fuente de Gienne
>[!recite]+
> Sobre la estancia reposa una masa de lodo y cadáveres que se contrae y expande de forma arrítmica. Cuando entráis a la estancia con una velocidad sorprenderte el cuerpo de la criatura se contra hasta formar un pilar sólido del que emergen 4 tentáculos.

```encounter-table
name: Nido Lodocito
creatures:
  - 1: Gran Lodocito, 9, 12
```