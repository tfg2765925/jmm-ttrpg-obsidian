# La Compañía de Mara

La compañía es un grupo de mercenarios que actúa principalmente en la región de Neveleti. Está liderada por Mara, una maga humana que antiguamente ejercía como investigadora del gremio. Inicialmente fundada por Mara, Bári y Luudna en 1045, el grupo ha ido adquiriendo miembros con el paso de los años, entre ellos un Goblin llamado Ogrym, Tav el semidemonio y una colonia de hongos que quiere que se refieran a ella como Mycul.

## Mara
Una maga humana de 43 años de edad lleva el pelo largo castaño siempre recogido en una gruesa trenza. Viste con la indumentaria oficial del gremio, armadura de malla, capa de invierno, botas y pantalones militares. Mide alrededor de 1.7 y tiene una complexión algo regordeta.
Mara ejercía anteriormente como una prospectora de mazmorras para el Gremio de Exploradores, una organización intercontinental encargada de investigar y categorizar las diferentes estructuras subterráneas que surgen debido a la proliferación de magia divina. Durante una de las incursiones Mara y su grupo encontraron a una aparición que había tomando el cuerpo de un humano. Su grupo se enfrentó al espíritu pero ella fue la única superviviente, en las semanas posteriores la mazmorra se descontroló provocando un desbordamiento mágico que casi arrasa todas las poblaciones cercanas. Por suerte la rápida actuación del Gremio y la fuerzas militares de las ciudades estado cercanas contuvieron la invasión. Sin embargo  a día de hoy el gremio sigue lidiando con frecuentes incursiones de monstruos que ascienden de la mazmorras. Tras esto Mara abandonó el Gremio, decidiendo ejercer como cazarrecompensas. A los pocos meses dio con Luddna y Bári y al conocer la existencia del tercer ojo, decidió acompañarles en sus viajes.

 ![[Mara-Relaciones.png]]

## Luudna
Gigante tríclope de unos 3 metros de alto, 17 años de edad y complexión delgada. tiene el pelo rapado y suele portar un pañuelo que oculta su tercer ojo. viste con pieles y una armadura hecha a medida con un blasón de Bráxas, un ojo cerrado de color morado, situado sobre el corazón.
Luuda es la única superviviente de las tribus nómadas que habitaban las cordilleras de Neveleti. Su grupo fue víctima de un ataque de la Mano de Piedra, una orden paramilitar que habita bajo la cordillera  de Neveleti. Sobrevivió gracias a que sus padres la ocultaron en una caverna poco antes de que se produjese el ataque. A los pocos días fue encontrada por un grupo de exploradores de la Mano, que andaban buscando supervivientes. Por pura casualidad, Bári un antiguo capitán de la guardia real enana se encontraba por allí  y rescató a la joven Luudna.
Desde entonces viajan juntos buscando respuestas para el misterioso tercer ojo de Luudna. Pocos años tras formar el grupo de Mara se encontraron con Ogrym, Luudna y él han hecho buenas migas, probablemente el tener un trasfondo similar les ha unido.

 ![[Luudna-Relaciones.png]]

## Bári
Enano duergar de piel morada y barba blanca, mide alrededor de 1.5 y porta una calva brillante y presenta una complexión similar a la de un campeón de halterofilia. Viste con una armadura de la guardia de Neveleti, una capa que oculta prácticamente todo su cuerpo y una botas reforzadas con hierro. Es un enano mayor rondando los 112 años aunque dice que hace dácadas que no lleva la cuenta así que podrían ser más.
Anteriormente Bári servía como capitán en las fuerzas armadas del inframundo, la órden militar del reino de enano bajo las montañas de Neveleti. Su reino recibió la visita de un celestial profetizando la aparición de un gigante de tres ojos, que se convertiría en un arma contra la sociedad enana. Fue seleccionado para liderar una de las cuadrillas de caza de la Mano de Piedra en la superficie. Al no encontrar al gigante de tres ojos, la cuadrillas recibieron la orden de masacrar a las tribus gigantes como medida preventiva. Consumido por la culpa y bañado en sangre de inocentes Bári abandonó su puesto. A los pocos días encontró a una niña gigante portando el tercer ojo de Bráxas, arrinconada por una partida de reconocimiento. Se enfrentó a sus antiguos hermanos de armas y se presentó a la niña como un disidente del reino enano.
Bári es consciente de su participación en el genocidio de la sociedad de su nueva hija adoptiva y del peligro que esta supone para su antigua gente. Aún así ha decidido adoptarla y viajar con ella en busca de un lugar seguro, con la esperanza de redimirse en el proceso y evitar la profecía.

 ![[Bári-Relaciones.png]]

## Ogrym
Goblin de un tono verde azulado, aparenta estar entre los 12-15 años y mide alrededor de 1.20. Presenta ambas orejas marcadas con una profunda incisión en forma de triangulo. Tiene el pelo negro estilizado en un mullet mal cuidado y viste con ropas de viajes manchadas de diferentes colores. Siempre lleva consigo un violín de madera bien cuidado y una partitura que guarda en un portalibros en su cadera.
Ogrym era anteriormente un miembro de un circo ambulante conocido como Las Maravillas de Ilfik. Dirigido por excepcional bardo humano llamado Tof, quién encontró a Ogrym cuando no era más que un bebé abandonado por su clan debido a una misteriosa fiebre mágica. Tof y su circo dieron fin a la enfermedad y decidieron adoptar a Ogrym, al ser incapaces de encontrar a sus padres. Ogrym creció rodeado de artistas y con Tof como su maestro, durante años actuaron por todas las ciudades del continente hasta llegar a la capital de Neveleti, Descenso del Héroe. Durante una de las actuaciones los Caballeros de la Órden de Dabuum irrumpieron en el escenario atacando a los circenses, Tof consiguió escapar con Ogrym, pero fue gravemente herido. Le otorgó una partitura ilegible y le pidió que la protegiese con su vida antes de quedarse atrás para entorpecer el avance de la Órden y dar tiempo a Ogrym a escapar.
A los pocos día Ogrym fue encontrado por el grupo de Mara, quién había sido contratado para cazar a un peligroso Goblin que había iniciado un ataque al circo ambulante. Luudna se apiadó del joven y pidió que le perdonasen la vida, cuando Mara vio la partitura la reconoció como una pieza de la Sonata del Ocaso, un antiguo hechizo que había sido usado durante la Guerra Divina. Reconociendo a Ogrym como inofensivo y víctima de un encubrimiento, le ayudaron a fingir su muerte y viaja con ellos desde entonces.

 ![[Ogrym-Relaciones.png]]

## Tav
Semidemonio de pelo largo de un tono azulado, se lo suele dejar suelto. Viste con ropas de viaje mal cuidadas y llenas de cortes y suturas. Siempre lleva puesta un jubón negro con una símbolo rojo de la espalda en la forma de  una garra de dragón. Aparenta estar entre los 28 y 35 años y mide alrededor de 1.90.  Posee un tono de piel rojizo, dos largos cuernos de carnero de hueso negro u una cola adornada con anillos.
Tav sabe poco sobre sí mismo, recuerda haber servido en la guardia del inframundo bajo Bári, y despertarse un día en un lago con la ropa que lleva puesta actualmente. No recuerda que pasó en los 5 años que ha habido desde que estuvo en la capital enana. Su último recuerdo es el de una incursión a una mazmorra clasificada un como peligro de desbordamiento, pero no qué encontró dentro ni como salió. Al tratar de volver a la capital bajo la cordillera fue recibido como un traidor al reino y expulsado de la misma bajo amenaza de muerte.
Durante años ha vagado por el continente hasta encontrarse con su antiguo capitán Bári. Este abandonó la guardia antes de los acontecimientos de la mazmorra así que no sabe que es lo que ocurrió dentro que hizo que Tav fuese considerado un traidor al reino, le ha ofrecido unirse al grupo y viajar juntos en busca de respuestas y significado al símbolo de su espalda.

 ![[Tav-Relaciones.png]]

## Mycul
Una colonia de hongos de complexión regordeta y piernas cortas. Una prominente seta de color marrón se sitúa sobre su cabeza actuando a modo de sombrero. Mycul viste con una túnica verde encima de su armadura.
Mycul son una colonia de hongos que actúan como un único individuo inteligente. Esta raza son conocida como Fungril y habitan en la regiones más inhabitables del continente conocidas como las Ciudades Perdidas. Son una raza anciana que precede a los mismos dioses del continente. Mycul pertenecía anteriormente a la corte del Rey de Esporas, un semidios compuesto de incontables individuos que reside en el antiguo reino divino. Una maldición ha separado su conexión con la corte convirtiendo a Mycul en una colonia distinta a la del rey. Incapaz de volver a unirse a sus hermanos y hermanas y temiendo que so condición pueda extenderse a otros individuos, Mycul abandonó las Ciudades Perdidas, en busca de una forma de levantar el maleficio y reunirse de nuevo con la corte en un un único ser.
Hace 30 años que Mycul abandonó la corte, lo durante este tiempo ha descubierto que la magia que mantiene su bloqueo con la corte y alimenta a las mazmorras presentan la misma naturaleza. Ha deducido que el origen ha de ser el mismo y durante una incursión a las mazmorras al sur de Neveleti conoció al grupo de Mara. Tras ayudarse mutuamente a liberar la mazmorra ambos decidieron formar una relación profesional y ayudarse mutuamente en el futuro.

 ![[Mycul-Relaciones.png]]

# Acto 1 Sombra  sobre Ulchiv
>[!recite]+
>Vuestro grupo fue contratado hace 2 semanas para lidiar con las misteriosas desapariciones que se estaban produciendo en Ulchiv una ciudad situada al este del continente. Tras días investigando disteis con la causa, una dríade corrupta se había infiltrado en Ulchiv y secuestrado a diversos de sus habitantes, arrinconada la dríade se dio a la fuga con su última víctima, una recién nacida. Los siguientes días rastreasteis sin descanso los montes cercanos a la población en busca de la arboleda de la dríade, la bruja os plantó cara para defender su presa, pero no fue rival para vosotros. Con su último aliento pronunció las siguientes palabra: "Era necesario... para... la canción". Buscasteis alrededor de su guarida el resto de los cuerpos pero se cual fuese el ritual debió de haberlos consumido por completo.
>Durante el enfrentamiento Luudna tratando de escudar a Mara de un hechizo se vio sumida en un profundo sueño, transportasteis su cuerpo inconsciente a la ciudad donde os aseguraron que recibiría el mejor tratamiento médico disponible y con la Dríade derrotada no tardaría más de unos días en despertar.
>Regresasteis a Ulchiv recibidos como héroes tras salvar a la población y devolver a la niña sana y salva. El gobernador ha organizado un festín y ha pagado por vuestra estancia durante una semana en la posada más lujosa de la ciudad: El Sueño del Príncipe. Cada uno está celebrando esta victoria de diferentes formas, pero en el exterior se encuentran un padre y una colonia de setas, compartiendo dos jarras de cerveza.

### Escena 1 - Taberna

>[!recite]+
>El primer piso de la posada es una impresionante taberna, el pueblo entero se ha reunido para celebrar vuestra victoria y en el centro, situado sobre un escenario elevado se encuentra una hilera de borrachos agarrados de los hombros y bailando de forma torpe pero animada, en el centro de la cadena, a un metro y poco de suelo y sujetado por sus compañeros de pista se encuentra Ogrym, intentando y fallando en dirigir a su grupo de baile.
>Situados más atrás alejados del follón hay dos figuras con bebidas refinadas una maga humana y un semidemonio ven a su compañero disfrutar del ajetreo y discuten su siguiente movimiento.

>[!recite]+
>El cardenal de la ciudad un hombre polilla entrado en años y de aproximadamente dos metros de altura se abre paso hacia vosotros con una bebida en cada una de sus cuatro manos "Disculpad pero podría acompañaros un momento, será breve no quiero arruinar vuestro momento."
>El cardenal les hablará de sus sospechas de que la dríade no estaba detrás de todas las desapariciones y de su temor a que una entidad aún mas poderosa aproveche el vacía de poder dejado por la dríade para causar más estragos. Les pedirá reunirse con ellos mañana en la catedral, quiere presentarles a unos individuos que han recogido pruebas suficientes para levantar sospechas sobre las desapariciones

# Acto 2 La Desaparición del Párroco
A partir de ahora se encuentran permanente bajo el efecto del [[Poblado Hechizado]].
Cada uno tirará un dado 2d12, el más alto se despertará con una nota en el bolsillo y recordará su conversación con el párroco

### Escena 1 - Catedral
>[!recite]+
>En la plaza principal del Ulchiv se alza una monstruosa catedral catedral gótica adornada con remates dorados, las torres construidas con piedras de maná flotan sobre los cimientos emitiendo un brillo azulado. La puerta de la ciudad se encuentra abierta, al entrar un semiorco, joven, algo regordete os recibe: "Hombre si son los héroes de Ulchiv, habéis venido ha agradecer a los 9 vuestra victoria?"
>El sacerdote les dirá que aquí no tienen cardenal, el anterior murió hace unos años y la iglesia todavía no ha elegido a uno para sustituirlo, cada sacerdote y ciudadano de la catedral les dirá algo distinto, que el cardenal está malo, que está de viaje, que esta catedral no tiene cardenal.
>Los aposentos del cardenal se componen de un escritorio de piedra curvado que se ajusta perfectamente a la pared, hileras de libros t pergaminos apilados ordenadamente en el suelo y en la mesa y en el centro una especie de hamaca que cuelga del techo.

Investigación 10, encontrará un mapa de la ciudad con 3 x rojas
Investigación 15, se encontrará un dibujo a grafito de un hombre calvo con las cuencas hundidas y dientes afilados emergiendo de forma caótica de entre los labios y la mandíbula.
Investigación 20, Un libro detallando a los lunarios, una raza de ratas lunares cuya inteligencia está ligada a las fases de la luna, con notas detallando su cultura y libros a medio escribir sobre figuras importantes y sus títulos. Un diario en el que detallas los intentos del cardenal por acercarse a esta sociedad y sus dificultades, y como la semana pasada pudo hacer contacto por fin, debido a que su líder estaba desesperada y necesitaba su ayuda para salvar a su pueblo.

### Escena 2 - Alcantarillas
El alcantarillado de la ciudad se ha fusionado con una antigua mazmorra abandonada después de uno movimientos de la tierra, cada una de las x marca una entrada al alcantarillado

#### Cruz Norte 
>[!recite]+
> La cruz del norte se encuentra situada al final del canal que sale de la ciudad, se trata de una zona comercial donde se ha congregado un bullicio de gente ahora que es seguro salir a la calle. Al otro lado del canal podéis ver 3 arcos de piedra que muestran una entrada hacia las alcantarillas. A su alrededor u grupo de obres se encuentra ocupado tapando una de las entrada. El capataz una mujer humana algo baja y con el pelo rizado rojo discute con un Katari con rasgos de guepardo a gritos.
> La capataz no quiere dejar entrar al katari debido a que estos es una entrada a una antigua mazmorra que el gobernador ha pedido que se cierre, las obras se han resumido ahora que al dríade está muerta. El katari sin embargo quiere bajar debido a que ha encontrado una carta de su hermano detallando que van a empezar las excavaciones pronto y ha sido seleccionado como un guardaespaldas para los obreros mientras ellos cierran las entradas. La captaz y ninguno de los obreros recuerdan a este supuesto grupo de mercenarios y quieren echarlo de las obras.
> La entrada desciende hacia un antiguo sistema de alcantarillado, al pasar por el arco de la puerta, la poca luz que se filtra del exterior no es suficiente para iluminar esta sala, donde escucháis el sonido de uñas contra el metal y crujidos húmedos.

```encounter-table
name: Alcantarilas
creatures: 
  - 1: Rey de las Ratas, 6, 10
  - 10: Rata Gigante, 1, 10
```

>[!recite]+
> Los cadáveres que están devorando las ratas son un grupo de personas armadas la mayoría en un estado reciente de descomposición, todas sus cabezas han sido brutalmente extirpadas de sus cuerpos dejando grandes detrás espantosos mordiscos.
> El canal continúa hacia la oscuridad

### Cruz Este 
>[!recite]+
>La segunda cruz os lleva aun antiguo pozo abandonado, tras agotarse el agua del pozo los comercios y hogares de alrededor fueron abandonados y ahora han sito tomados por la naturaleza. EN el fondo del pozo se puede observar un brillo tenue. Se trata de una baratilla con forma de luna y hecha de plata, una pequeñísima nota  se puede ver debajo de la misma. En ella se puede leer. Al cardenal, agradecemos el apoyo a nuestro y pueblo, bajo otras circunstancias no nos hubiésemos revelado ante usted pero la situación de Ulchiv ha empeorado tras la caída de la Dríade. Creemos que en cuestión de días el pueblo entero será consumido por una Fátula. He tomado la decisión de evacuar a mi gente, un regimiento de mis soldados más leales le esperan en la casa del tejado rojo, identifíquese con el siguiente código: Sêlune señora de plata cuando la luna esté más alta.

En la casa del tejado rojo parcialmente derruida y saqueada se encuentra una trampilla, aquellos entre vosotros de mayor envergadura tendríais problemas para entrar.

>[!recite]+
>Se escucha un zumbido mecánico y la trampilla comienza a alzarse y devela un túnel con una escalera de mano que desciende hacia la oscuridad. Encaramada a una de las paredes hay una rata con un casco decorado con una pluma en su lomo, se encuentra pintada una luna de plata.

```encounter-table
name: Alcantarilas
creatures: 
  - 2: Rey de las Ratas, 6, 10
  - 3: Alimaña Lunar, 1, 14
```

Huirá y dará la voz de alarma se puede desescalar la situación, hablando y superando tiradas de persuasión. 14 

### Cruz Sur - Final de Ingenio
>[!recite]+
>La tercera cruz os lleva a la parte sur de la ciudad, el casco antiguo de la misma, un sinfín de estrechas calles entorpecen vuestro avanza hasta que por fin llegáis al destino. El Final del ingenio es una taberna de una única planta, la humedad del suelo y que la tierra de esta zona sea más blanda que en el norte han provocado que el lateral izquierdo de la taberna se hunda obligando al techo de la misma a curvarse de forma extraña.
>De las puertas de madera surge un constante barullo y una luz cálida. Al entrar a la taberna el grupo de unos diez-doce humanoides que se encontraban en el interior se callan y os miran fijamente.

Inicialmente no van a ser amables y si explican la situación y el motivo por el cuál están allí el líder, un híbrido entre goblin y humano se ofrecerá a contarles lo que sabe. Ha encontrado unas notas con su letra,  unos documentos oficiales que detallan el contrabando que sale y entra de la ciudad y los nombres de los encargados, sin embargo los de hace dos meses no pueden estar correctos, puesto que ese contrabando fue robado por el grupo de hombres que contrató y los tuvo que ejecutar. Pero a pesar de recodar esto como si fuese ayer él los marcó como recibidos y les dio el dinero a los culpables y no los recuperó. SI le convencen y aceptan sus condiciones les ofrecerá enseñarles el conducto que usan de contrabando y que conduce a las alcantarillas.

Avanzando por el conducto llegarán a un acueducto subterráneo que se extiende varios metros es lo suficientemente ancho como para permitiros andar hombro con hombre y tiene una altura de unos 5 metros, en el suelo se pueden ver estrechos canales de agua secos

Tras unas horas caminando, iluminado por antorchas mágicas se encuentra un antiguo carruaje, volcado y con diversos agujeros en sus paredes y en el suelo del carruaje, sangre reseca adorna el sitio del accidente. Una bestia andrajosa se encuentra tras la caravana, un Oso, cubierta de mugre y sangre porta una herida infectada en su hombro. Al acercaros emite un rugido amenazador.

```encounter-table
name: Aqueducto
creatures: 
  - 1: Oso, 7, 14
```

# Acto 3 Abominación Bajo la Ciudad

### Escena 1 - Restos de la carnicería

### Escena 2 - Fátula

```encounter-table
name: Comedero
creatures: 
  - 1: Fátula, 9, 18
  - 5: Zombie, 1, 10
```