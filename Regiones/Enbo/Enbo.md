---
location: Sur de Marquis
alegiance: Iglesia de las LLamas
leader: Svartalfein
---
# `=this.file.name`

> [!infobox|wfit left]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |

![[Solren.PNG|cover hsmall]]

# Información General
Región mayormente desértica situada al sur del [[Marquis]], dirigida desde hace siglos por un dragón rojo ([[Svartalfein]]), elegido por la [[Reina Arcoíris]] tras la [[Guerra Cromática]]. Se trata de la región de más tamaño del continente y es la principal fuerza militar actualmente. Aunque la forma de gobierno oficial es una teocracia, cada una de las ciudades principales de esencialmente independiente  económica y políticamente de las otras sólo teniendo que responder ante altas sacerdotisas de la [[Iglesia de las LLamas]].

Tras la desaparición de los dioses, los [[Dragones]] quedaron atrás en el plano material. Los dragones metálicos, creaciones del [[Nonantheón]] continuaron librando una guerra contra los sirvientes de los [[Tiranos]] los dragones cromáticos. En el 734, en el continente de Enbo comenzó una invasión de cientos de dragones metálicos liderados por [[Gruaghlothor]] lo que desató un feroz conflicto  contra las fuerzas de los dragones elementales que duró 100 años. La fuerza invasora fue repelida y los restos de los [[Guardianes de Hierro]] se retiraron a los confines del continente. Herida durante la batalla con Gruaghlothor, la reina cromática se retiró a los confines de la tierra para recuperar sus fuerzas, declarando a Svartalfein como regente hasta que ella despertase de su letargo.

### Poblaciones
- [[Unn]]
- [[Niguugn]]
- [[Dunas]]

### Enemigos exclusivos
- [[Gecko Arenero]]
- [[Jinente de Gecko]]
- [[Lavamarquesa]]
- [[Lavartija]]
- [[Protogolem]]
- [[Mechadragón]]