---
location: Enbo
alegiance: Guardianes de Hierro
race: Dragón (Hierro)
class: Dragón
age: 138 (Biológica) +1000 (Mental)
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Raza** | `=this.race` |
> **Clase** | `=this.class` |
> **Edad** | `=this.age` |
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
El señor de los [[Guardianes de Hierro]] y contraparte de la [[Reina Arcoíris]]. Creado por el [[Nonantheón]] como general de sus ejércitos durante la [[Guerra Divina]] es lo más cercano a un dios con forma física que existe en el plano material. Dotado de una forma de pseudo-inmortalidad Guraghlothor no puede morir mientras exista al menos un dragón de metal en este mundo. Cuando su forma física es destruida su alma se reencarna en un dragón metálico del planeta, combinando sus conocimientos y experiencias en un nuevo Gruaglothor.
Tras su derrota en la guerra cromática se desconoce de su actual paradero pero se han registrado casos de dragones metálicos asaltando puestos de la [[Iglesia de las LLamas]] es posible que el señor del hierro esté preparando una nueva incursión para acabar con los dragones elementales.

```statblock
monster: Dragón de Hierro Anciano
description: "Señor de los dragones metálicos general de las fuerzas divinas. Un colosal dragón de 50 metros de altura que ha vivido más de 1000 años"
name: Gruaglothor
hp: 20
attacks:
 - name: Mordisco
   desc: "Cercano `dice: 1d20+10` Daño `dice: 8d8` (físico)"
moves:
- name: Furia Divina (reacción)
  desc: "Cuando recibe un ataque gasta un punto de estrés para realizar un ataque cuerpo a cuerpo contra la criatura que lo ha provocado si se encuentra a rango"
```

