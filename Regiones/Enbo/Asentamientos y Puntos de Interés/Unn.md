---
location: Centro de Enbo
alegiance: Iglesia de las LLamas
leader: Okya
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Una de las tres ciudades principales de [[Enbo]], fundada por [[Svartalfein]] tras su llegada al continente fue planteada como el foco de la innovación mágica de su Imperio. Unn es prácticamente una academia gigantesca con pequeños barrios esparcidos su alrededor para acomodar a quienes vienen a la ciudad para hacer negocios con los miembros de la [[Escuela de Niguugn]]. 
> Unn se identifica por su descomunal torre de arenisca que desafía las leyes de la física, en ella las mentes más brillantes de Enbo investigan la naturaleza de la magia y sus aplicaciones. La archimaga [[Okya]] actúa como rectora de la escuela y principal representante político de la ciudad. Tras la elección de Okya como rectora los principales avances científicos de la escuela se han centrado en aplicaciones militares, recientemente han comenzado excavaciones en las montañas cercanas para la construcción de nuevas máquinas de guerra.