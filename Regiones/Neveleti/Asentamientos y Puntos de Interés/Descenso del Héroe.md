---
location: Neveleti
alegiance: Dinastía Ulfris
leader: Anthry Ulfris
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Conocida hace 500 años como Nungelarth era la capital del un reino enano que sucumbió a una invasión del averno. Durante la guerra civil los resto de la ciudadela fueron usados por los rebeldes como base de operaciones. Desde aquí [[Anthry Ulfris]] lazó su ofensiva contra el reino y comenzó a ganar terreno a las fuerzas imperiales. La ciudad se encuentra situada en un profundo cráter y con los edificios de mayor envergadura asomando de la tierra.
> Diversas grupos de exploración son financiados por la dinastía para explorar los antiguos pasadizos en busca de reliquias y oro. Estas incursiones sirven también como una medida preventiva para limpiar las cavernas de monstruos y evitar que accedan a la ciudad. Gran parte del casco antiguo se ha dado por perdido debido a la presencia de abominaciones que quedaron atrás en este plano de la realidad tras la invasión. Sólo parte más alta y reciente de la ciudad es habitable aunque algunas expediciones al casco antiguo han logrado recuperar piezas de ingeniería enana que se consideraban extintas.