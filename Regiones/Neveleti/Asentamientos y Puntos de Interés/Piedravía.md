---
location: Neveleti
alegiance: Dinastía Ulfris
leader: Arcanista Blanco
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Un poblado construido sobre los restos arqueológicos de un templo anterior a la [[Guerra Divina]]. Dispersas tiendas y construcciones recientes se acumulan a los pies de la pirámide de piedra y ubicada en el centro del poblado. Piedravía está rodeada por un denso bosque de roble habitado por híbridos parientes lejanos de las bestias del caos. Recientes excavaciones han desvelado una mazmorra divina bajo la pirámide de, un grupo de exploración ha sido enviado por el [[Gremio]] para valorar la situación.
> Piedravía sobrevive gracias a la magia del Arcanista que ha imbuido los pilares que rodean al poblado con siglos arcanos para mantener a raya a las bestias.