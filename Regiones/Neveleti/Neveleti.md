---
location: Centro-Este de Marquis 
alegiance: Dinastía Ulfris
leader: Anthry Ulfris 
---
# `=this.file.name`

> [!infobox|wfit left]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |

![[Solren.PNG|cover hsmall]]

# Información General
Región compuesta por extensas estepas y rodeada por picos montañosos en prácticamente todas las direcciones excepto el mar. Concentra el mayor número de población del continente y hasta hace poco la sede del [[Gremio]]. Gobernada desde hace 70 años por la [[Dinastía Ulfris]] tras la última revolución en la cuál [[Anthry Ulfris]] acabó con la vida de [[Ren Merrón]], estableciendo a su familia como la principal fuerza política de la región. Se encuentra respaldada por [[Svartalfein]] lo que ha llevado a muchos a pensar que la familia Ulfris no es más que un títere para la [[Iglesia de las LLamas]]. Desde la revuelta el antiguo [[Castillo Merrón]] quedó abandonado los objetos arcanos presentes en el mismo han propiciado la aparición de una mazmorra arcana, actualmente bajo investigación del Gremio.

Las amplias estepas y picos montañosos de Neveleti son propensos a generar mazmorras de baja presencia mágica por lo que las región se ha convertido en una de las principales proveedoras de partes de monstruos inferiores. La capital [[Descenso del Héroe]] se sitúa sobre 3 mazmorras comerciales y de la misma nacen todas las carreteras que suministran de los materiales mágicos extraídos de las mazmorras al resto de poblaciones. Estas carreteras cumplen una doble función ya que la capital es el foco del comercio en la región.
### Poblaciones
- [[Descenso del Héroe]]
- [[Ulchiv]]


### Enemigos exclusivos
