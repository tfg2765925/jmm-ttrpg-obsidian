---
location: Descenso del Héroe
alegiance: Dinastía Ulfris
race: Elfo
class: Serafín
age: 104
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Anthry_Ulfris.jpg|cover hsmall]]
> 
> | |
> ---|---|
> **Raza** | `=this.race` |
> **Clase** | `=this.class` |
> **Edad** | `=this.age` |
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
Heredero de la [[Dinastía Ulfris]] y el señor de [[Neveleti]], Ascendió al trono tras la victoria de las fuerzas insurgentes contra [[Ren Merrón]]. Se trata de un elfo de pelo blanco rizado y largas cejas, viste con una toga ceremonial encima de su armadura y tiene una complexión esbelta y delgada. La mayor parte de su reinado se ha centrado en apagar las diferentes revoluciones provocadas por las [[Bandas Verdes]], y comenzado excavaciones hacia las profundidades de [[Descenso del Héroe]] en busca de las antiguas armas enanas.
Anthry y Ren eran anteriormente compañeros de batalla, ambas familias siendo aliadas desde la [[Guerra Divina]], sin embargo la longevidad de Anthry y complejo de superioridad incitó un profundo odio al sistema de gobierno del rey humano. Su falta de planificación e incapacidad de acomodar el reino para las razas longevas llevaron al usurpador a enfrentarse al rey en diversas ocasiones. Tras una de estas discusiones, guardias leales a Merrón, asaltaron el Castillo Ulfris por miedo a una posible revolución. La muerte de la familia y el descontento general de la población incitaron una verdadera revolución en la región. Anthry fue apresado como medida cautelar, peo antes de que Ren pudiese hablar con su antiguo amigo este fue liberado por disidentes y comenzó a dirigir la revolución.
