---
location: Neveleti
alegiance: Desconocida
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
Bandoleros fieles a [[Ren Merrón]], originalmente una órden de caballeros que sobrevivió al conflicto y se dedicó a batallar al usurpador desde las sombras. Se desconoce el líder de la organización, y se asume que actúan como una serie de células independientes entre ellas simplemente tratando de causar problemas a la dinastía y esperando el momento para devolver a al verdadero rey al trono.