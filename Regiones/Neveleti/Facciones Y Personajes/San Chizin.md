---
location: Ulchiv
alegiance: Dinastía Ulfris/Órden de Dabuum
race: Simiah
class: Guerrero
age: 42
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Raza** | `=this.race` |
> **Clase** | `=this.class` |
> **Edad** | `=this.age` |
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
Un Simiah anciano con la apariencia de un orangután, viste con una toga ceremonial y un refuerzo metálico en su brazo derecho. San Chizin es la máxima autoridad de la [[Órden de Dabuum]] desde hace más de una década. Su predecesor y maestro fue un querido amigo de [[Anthry Ulfris]] y durante la guerra civil empleó los recursos de la órden para ayudar en el conflicto, esta desviación de los valores de la orden provocaron una ruptura interna que casi acaba con la misma. En la actualidad San Chizin trata de mantener la apariencia de la órden como un organismo imparcial a favor de los débiles pero su ascenso a la santidad y sus estrechos lazos con la dinastía generan recelo entre antiguos y nuevos reclutas que temen que la órden se convierta en una herramienta del estado.