---
location: Costa Fría
alegiance: Ninguna
leader: Ninguno
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Bosquecillo situado en los acantilados rodeando [[Puerto Escarpado]], debe su nombre a los extraños árboles de acero que sólo crecen en esta localización. La madera es extremadamente resistente al corte pero susceptible a los golpes y considerablemente más densa que la madera normal. Debido a estas propiedades la arboleda ha sido ignorada por colonos durante generaciones permitiendo la creación de una sociedad secreta de hadas resguardada de las miradas de las razas altas.