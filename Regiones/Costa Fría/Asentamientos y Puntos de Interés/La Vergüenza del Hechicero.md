---
location: Arboleda Pétrea
alegiance: Consejo Verde
leader: Ninguno
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Una micromazmorra anteriormente conocida como el Santuario del Invierno, situada en la [[Arboleda Pétrea]] es el lugar de reposo de la Daga del Invierno, una de las armas legendarias que fue otorgada a la campeona mortal [[Sylvale]] durante la [[Guerra Divina]]. Tras completar su misión Sylvale retornó la hoja a [[Ludvick Fir]]. El cuál tuvo que recordarle que era demasiado grande para que cualquier hada hiciera uso de ella, descontentos con la idea de que una arma féerica quedase en manos de las razas altas, decidieron esconderla en el corazón del bosque.
> Su nombre se debe a que el creador del santuario, que aborrecía que la daga hubiese sido transformada en una arma para las razas altas. Intentó un golpe de estado contra el [[Consejo Verde]], cuando Sylvale regresó a devolver el arma se aseguró de atravesar al hechicero con ella.
> La Daga fue activada por el [[Escuadrón de Destrucción de Humanos]] en Sunmo de 1032 mientras buscaban un artefacto con el cuál librar una guerra contra la humanidad. Desgraciadamente la activación provocó una helada que casi destruye la arboleda. Un grupo de aventureros féericos lograron detener sus planes y desactivar el arma.

## Enemigos Exclusivos
- [[Espectro de Escarcha]]
- [[Jinente de Ratones]]
- [[Hada Guerrera]]
- [[Hada Hechicera]]

## Mapa

![[lvh-p0.png]]
![[lvh-p-1.png]]