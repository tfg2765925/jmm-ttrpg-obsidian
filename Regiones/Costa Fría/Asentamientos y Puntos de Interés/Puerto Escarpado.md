---
location: Costa Fría
alegiance: Consejo de Laur
leader: Bernard Sernit
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Ciudad costera situada en en le norte de [[Costa Fría]], el puerto principal de esta región alberga una gran cantidad de viajeros y habitantes. Dirigida por [[Bernard Sernit]] tras su reciente reelección como gobernador de la ciudad. Actualmente en un conflicto con bandas de piratas que  atacan los islotes cercanos que la [[CMR]] emplea como almacenes para sus bienes.