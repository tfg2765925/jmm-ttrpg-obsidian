---
location: Costa Fría
alegiance: Ninguna
leader: Ninguno
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Mazmorra militar descubierta hace 5 años por un grupo de pescadores. El pasado Fernamë se declaró una emergencia nivel nacional cuando un desbordamiento mágico sin precedentes acabó con la población del puesto de Aguas Claras. El [[Gremio]] y los Condados de [[Costa Fría ]] pudieron repeler la horda de monstruos marinos en tierra. Tras varias semanas de campaña militar se ha podido establecer un bastión sobre la entrada de la misma. A pesar de esto la situación no se encuentra bajo control y el Gremio ha convocado a todos los exploradores de alto nivel para hacer frente a las frecuentes incursiones de monstruos.
> Se ha llegado a emplear mercenarios y regimientos de Costa Fría para hacer frente a los monstruos. Sin embargo sus números no parecen menguar y son frecuentes los encuentros entre exploradores y bandas de monstruos con ambos bandos intercambiando territorio frecuente mente. Es por ello que la sede del Gremio se ha establecido sobre el Bastión hasta que la situación vuelva a estar bajo control.

## Criaturas de la Mazmorra

## Mapa