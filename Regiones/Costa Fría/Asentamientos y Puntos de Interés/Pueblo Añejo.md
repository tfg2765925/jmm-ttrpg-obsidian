---
location: Arboleda Pétrea
alegiance: Consejo Verde
leader: Ludvick Fir
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Congregación de diferentes asentamientos de las hadas de [[Costa Fría]], una sociedad secreta al margen del [[Consejo de Laur]] que mantiene las distancias con las razas humanoides. Situada en el centro del la [[Arboleda Pétrea]] en un árbol féerico se encuentra este refugio para cualquier raza diminuta. Fundada por [[Ludvick Fir]] cuando las hadas llegaron por primera vez a esta tierra Pueblo Añejo ha crecido hasta convertirse en una metrópolis llena de vida. 
> El pueblo cuenta con una barrera mágica que lo oculta de la mayoría de las amenazas, está siendo mantenida actualmente por Ludvick y sus aprendices. Para aquellos intrusos que no son tan fácil de desorientar el [[Consejo Verde]] ha construido 3 constructos de piedra y madera que vigilan las afueras del pueblo en busca de amenazas.
> Hace 5 años se produjo una alteración medioambiental provocada por desbordamiento mágico en [[La Vergüenza del Hechicero]]. Un grupo de hadas sectarias despertaron por error una reliquia de [[Sylvale]] lo que desato un inesperado invierno en el corazón del bosque debilitando el encantamiento que protege el pueblo e inhabilitando a sus guardianes. La catástrofe fue evitada gracias a un grupo de héroes que se adentraron en la mazmorra y desactivaron la causa del encantamiento.