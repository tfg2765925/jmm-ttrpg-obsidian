---
location: Oeste de Marquis
alegiance: Condados Lauros
leader: Consejo de Laur
---
# `=this.file.name`

> [!infobox|wfit left]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |

![[Solren.PNG|cover hsmall]]

# Información General
Ubicada tas las montañas que separan el mar de [[Enbo]] y limitando con [[Forcosia]], Costa Fría ocupa la principal conexión de [[Marquis]] con el Oeste. Presenta un clima frío debido a las extrañas corrientes marinas del [[Mar Orsis]]. El gobierno de la región recae en el [[Consejo de Laur]] una asociación de los condes de cada una de las ciudades estado de la costa.

Recientemente el [[Gremio]] ha ubicado su sede en la mazmorra de [[Aguas Claras]] tras el reciente desbordamiento mágico. La situación ha alterado a los condes que hasta ahora veían las mazmorras como una fuente de ingresos adicional al comercio. Este incidente ha propiciado una serie de nuevas recompensas para aquellos que se adentren en las profundidades de las mazmorras y acaben con criaturas más peligrosas. La promesa de gloria y dinero ha atraído a multitud de aventureros a la región 

### Poblaciones
- [[Puerto Escarpado]]
- [[Pesar del Pirata]]
- [[Tarrasva]]
- [[Pueblo Añejo]]

### Enemigos exclusivos