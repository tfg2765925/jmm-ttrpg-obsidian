---
location: Pueblo Añejo
alegiance: Sombrero Rojo
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
El EDH es un grupo terrorista fundado por [[Sombrero Rojo]], un disidente del [[Consejo Verde]]. Con la intención de destruir las poblaciones humanas cercanas a la [[Arboleda Pétrea]], el escuadrón se internó en la profundidades del bosque buscando la Daga del Invierno, la encontraron en [[La Vergüenza del Hechicero]], y activaron el artefacto causando un desbordamiento mágico en forma de ventisca que casi destruye el arboleda.
El escuadrón se dispersó tras la muerte de su líder en 1032, aunque ha inspirado a algunos movimientos guerrilleros desde entonces.