---
location: Muerto
alegiance: Escuadrón de Aniquilación de Humanos
race: Hada
class: Paladín
age: 452
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Raza** | `=this.race` |
> **Clase** | `=this.class` |
> **Edad** | `=this.age` |
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
También conocido como Leverión, Sombrero Rojo fue un hada disidente del [[Consejo Verde]] y fundador del [[Escuadrón de Destrucción de Humanos]]. Fue capturado por un grupo de humanos durante una excursión fuera de la [[Arboleda Pétrea]] donde se vendió a la [[CMR]]. Sombrero Rojo estuvo años en cautividad hasta que un grupo de hadas se infiltró en el laboratorio y lo rescató. Tras su regreso Leverión imploró al consejo tomar represalias contras los humanos de [[Puerto Escarpado]] pero esta petición fue denegada por [[Ludvick Fir]] quien creía que el asalto a la CMR era castigo suficiente. Decidido a vengarse de todos los humanos de Puerto Escarpado y proteger a sus hermanos y hermanas, Leverión junto a un grupo de hadas con mentalidad similar decidieron usar la Daga del Invierno y causar un desbordamiento mágico en el puerto.
Desgraciadamente este plan fracasó ya que al encontrar la daga no fueron capaces de transportarla y durante sus intentos activaron el artefacto, comenzando una desbordamiento en su propio territorio. Decidido a destruir Puerto Escarpado Leverión mantuvo la daga activada deduciendo que esta ventisca estaría afectando a los pueblos humanos también y que ellos carecían de los medios para protegerse del encantamiento. 
Finalmente Leverión fue atacado por un grupo de Hadas buscando el origen de la tormenta, el conflicto escaló y tras una ardua batalla Sombrero Rojo sucumbió a sus heridas.

En vida Leverión fue un hada con apariencia de abeja humanoide vestía con una armadura Féerica con una hombrera y una capa de general para cubrir sus brazos amputados.
### Estadísticas

```statblock
monster: Hada Guerrera
name: Sombrero Rojo
description: Conocido anteriormente como Leveríon, es un disidente del Consejo Verde y capitán del Escuadrón de Aniquilación de Humanos.
hp: 6
moves:
- name: Inspiración
  desc: "Todos los aliados que comiencen su turno en distancia cercana a Leverión obtienen ventaja en su siguiente ataque."
- name: Rayo de Escarcha
  desc: "Gasta un punto de miedo para realizar un ataque `dice: 1d20+4` a larga distancia contra hasta dos criaturas a distancia cuerpo a cuerpo entre ellas. En caso de golpear reciben `dice: 3d10+3` de daño mágico"
```



