---
location: Puerto Escarpado
alegiance: Costafría (CMR)
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
Cámara de gobierno de [[Costa Fría]] constituida por cada representante de las familias nobles de las ciudades de la región y la presidenta de la [[CMR]]. El consejo controla las exportaciones de la región, las explotaciones de las mazmorras y a la [[Milicia Fría]]. El consejo actúa con los intereses de las familias nobles y la Corporación Marítima Roja, ha subyugado de forma silenciosa a diversas figuras que han tratado de modificar el sistema de gobierno y eliminan cualquier tipo de oposición a la nobleza.
Desde su fundación hace 102 años diversos miembros han ocupado el consejo, siempre las cabezas de las familias más importantes actualmente. Un miembro puede perder su asiento en la cámara si su familia pierde influencia o se vota su expulsión por al menos mayoría simple.
Actualmente la principal fuerza que se opone al consejo son [[Los Señoritos]] habiendo conseguido asestar algunos golpes importantes a la aristocracia pero incapaces de provocar una revolución o cambio permanente.