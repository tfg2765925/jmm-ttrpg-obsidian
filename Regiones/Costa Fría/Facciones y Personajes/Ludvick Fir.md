---
location: Pueblo Añejo
alegiance: Consejo Verde
race: Hada
class: Hechicero
age: 5042
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Raza** | `=this.race` |
> **Clase** | `=this.class` |
> **Edad** | `=this.age` |
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
Su forma física es la de un escarabajo hércules humanoide que viste con una toga recogida con un cinturón de plata. Ludvick preside el [[Consejo Verde]] una sociedad secreta de hadas ubicada en la [[Arboleda Pétrea]], se trata de una hada antigua que vivió la catástrofe de la [[Guerra Divina]] y amigo cercano de [[Sylvale]]. Fir vive para proteger a su gente y él mismo mantiene el encantamiento que mantiene [[Pueblo Añejo]] oculto de las miradas indeseadas.