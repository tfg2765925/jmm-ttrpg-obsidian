---
location: Pueblo Añejo
alegiance: Pueblo Añejo
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
El Consejo Verde es una sociedad creada tras la[[ Guerra Divina]] para proteger a las hadas de [[Pueblo Añejo]], está compuesto por las cabezas de las 5 familias principales de la [[Arboleda Pétrea]] y presidido por [[Ludvick Fir]].