---
location: Arboleda Pétrea
alegiance: Consejo Verde
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
Es el nombre que se proporciona al trío de constructos que defienden [[Pueblo Añejo]] de amenazas exteriores. Fueron construidos por las primeras hadas que llegaron a [[Marquis]] desde los parajes féericos desde hace mil años han salvaguardado el bosque y sus habitantes. En 1032 una ventisca mágica los dejo temporalmente inhabilitados demostrando que el encantamiento que los alimenta es susceptible a disruptores mágicos de gran protencia.

Cada uno de los constructos está modelado tras un animal y comparten una misma base de datos.

``` statblock
monster: Constructo-Modelo grande
name: Constructo-Modelo sierpe
description: Uno de los tres colosos qe protegen Pueblo Añejo, está modelado tras una cobra con brazos humanos.
ac: 20
attacks:
 - name: Colmillos de Serpiente
   desc: "Cuerpo a cuerpo `1d20+6` Daño `dice: 3d6` (físico) + `dice: 1d4` (veneno)."
moves:
 - name: Apresar
   desc: "Gasta 1 punto de miedo para enroscarse alrededor de un enemigo cuerpo a cuerp `dice: 1d20+8`. La criatura queda atrapada hasta superar una tirada de fuerza con DC 14, por cada turno atrapada recibe `dice: 2d6+3` de daño físico"
```

``` statblock
monster: Constructo-Modelo grande
name: Constructo-Modelo búho
description: Uno de los tres colosos qe protegen Pueblo Añejo, está modelado tras una un buho en el interior de su melena se ocultan 4 brazos mecánicos.
ac: 20
attacks:
 - name: Talones de Búho
   desc: "Cuerpo a cuerpo `1d20+6` Daño `dice: 2d10` (físico) + `dice: 1d6` (sangrado)."
moves:
 - name: Vuelo
   desc: "El constructo es capaz de moverse libremente por el aire"
```

``` statblock
monster: Constructo-Modelo grande
name: Constructo-Modelo oso
description: Uno de los tres colosos qe protegen Pueblo Añejo, está modelado tras una oso pardo con un relé eléctrico en el lomo.
ac: 20
hp: 12
attacks:
 - name: Garras del Oso
   desc: "Cuerpo a cuerpo `1d20+8` Daño `dice: 3d8` (físico) + `dice: 1d8` (sangrado)."
moves:
 - name: Campo eléctrico
   desc: "Gasta 2 puntos de miedo para activar el relé de eléctrico, todas las criaturas enemigas a rango cercano del cosntructo atacan con desventaja, los aliados con ventaja."
```
