---
location: Norte de Marquis
alegiance: Familia Luel
leader: Levistine
---
# `=this.file.name`

> [!infobox|wfit left]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |

![[Solren.PNG|cover hsmall]]

# Información General
Región forestal situada al norte del [[Marquis]]. La mayoría de asentamientos se ubican en la cara sur de la cordillera, el norte se encuentra inhabitado y el [[Gremio]] manda una expedición anual para crear una ruta segura hasta las [[Ciudades Perdidas]]. Gobernada por la [[Familia Luel]], aunque su influencia sólo alcanza los terrenos del sur, el norte siendo demasiado inhóspito como para poder establecer asentamientos permanentes. La capital de Forcosia es [[Luel]], se encuentra situada el [[Paso Angosto de Jogerth]] y es el principal foco de población de la región y hogar de la familia real.

Se trata de una región fría cubierta por densos bosques y separada por una cordillera que separa la zona habitable de la región de la inhóspita del norte. La principal población del continente se compone de humanos y medianos y están concentrados en Luel. Siendo otras poblaciones asentamientos pensados para suministrar de recursos a la capital.

### Poblaciones
- [[Luel]]
- [[Ilfy]]

### Enemigos exclusivos
- [[Fuegos Fatuos]]




