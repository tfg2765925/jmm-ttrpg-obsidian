---
location: Ilfy
alegiance: Familia Luel
race: Humano
class: Luchador
age: 15
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Raza** | `=this.race` |
> **Clase** | `=this.class` |
> **Edad** | `=this.age` |
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
Joven humano alto, con el pelo oscuro rapado en su totalidad da la impresión de que se lo ha dejado crecer recientemente. Viste con ropas de viaje y un caso de caballero negro. Aprendiz de herrero que se ha atrincherado en la hacienda del gobernador junto con un arsenal de armas que logró llevarse de su taller antes de la aparición de los lodocitos. Uno de los pocos a los que se les permite salir de la hacienda para buscar alimento.