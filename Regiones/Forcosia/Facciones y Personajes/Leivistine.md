---
location: Luel
alegiance: Familia Luel
race: Mediano
class: ---
age: 14
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Raza** | `=this.race` |
> **Clase** | `=this.class` |
> **Edad** | `=this.age` |
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
Heredero de la [[Familia Luel]], Levistine Luel es un niño mediano que ascendió al trono tras la desaparición de su padre durante una expedición al norte. Aunque actualmente es la cabeza del Gobierno en [[Forcosia]] su madre ([[Laffai]]) ejerce como reina regente.