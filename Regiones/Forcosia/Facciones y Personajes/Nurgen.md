---
location: Ilfy
alegiance: Familia Luel
race: Humano
class: Clérigo
age: 56
---
# `=this.file.name`

> [!infobox|wfit left]
> ![[Solren.PNG|cover hsmall]]
> 
> | |
> ---|---|
> **Raza** | `=this.race` |
> **Clase** | `=this.class` |
> **Edad** | `=this.age` |
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |

## Información
Un humano entrado en edad, de complexión algo regordeta y con pelo canoso. La edad y un afeitado parcial hacen que ostente un rodapies y un coletilla pequeña recogida en forma de lazo. Viste con una túnica roja de la iglesia de [[Anamë]] y siempre lleva consigo unas gafas pequeñas con los cristales tintados.

Es el gobernador de [[Ilfy]] y un sacerdote de alto rango de Anamë. Es un hombre cauteloso y principalmente preocupado en la seguridad de su pueblo experto en hechizos de protección y ha dedicado su vida a proteger el [[Forcosia]] de amenazas de las mazmorras una herida durante una batida de caza le obligó a retirase. Su deseo de proteger y habilidades le llevaron al este donde llegó a [[Ilfy]] hace 5 años como curandero en poco tiempo se ganó la confianza el pueblo y se convirtió en su gobernador.

Actualmente se encuentra atrincherado en su hacienda manteniendo un glifo de protección para bloquear la entrada a la mazmorra.