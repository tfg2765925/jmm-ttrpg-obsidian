---
location: Forcosia
alegiance: Ninguna
leader: Ninguno
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Un frondoso bosque que se extiende por la zona este de [[Forcosia]] y limita con la cordillera, [[Ilfy]] es el único asentamiento cercano al mismo. La princial exportación es madera aunque cazadores de monstruos han encontrado suerte buscando materiales extraños en sus profundidades. Diversas bandas de [[Goblins]] luchan por control del diferentes zonas del bosque y en ocasiones se enfrentas a los aldeanos de Ilfy. Su nombre se debe a que durante los meses de Sunmo y Gü concentraciones de [[Fuegos Fatuos]] se pueden avisar en las lindes del bosque 

## Criaturas
- [[Fuegos Fatuos]]