---
location: Norte Forcosia
alegiance: Ninguna
leader: Ninguno
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Las ciudades perdidas es el nombre que se le da a la antigua nación previa a la [[Guerra Divina]]. Se trataba del centro del poder divino desde el cuál las deidades gobernaba el plano material, al comenzar al guerra las principales batallas entre la divinidad tomaron lugar en estas tierra. La escala del conflicto en esta zona fue tal que se ha convertido en un páramo desierto incluso 5000 años tras la guerra. Pocos humanoides pueden sobrevivir a la alta concentración de sangre divina en la inmediaciones, el gremio lleva años tratando de obtener muestras pero pocas expediciones han sobrevivido y aún menos han conseguido traer devuelta objetos de valor.
> El nombre y la cultura de los asentamientos han sido perdidas en el tiempo se rumorea que en la capital, el hogar de los dioses, se oculta el secreto para la asención a la verdadera divinidad.