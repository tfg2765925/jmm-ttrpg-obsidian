---
location: Forcosia
alegiance: Ninguna
leader: Ninguno
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Estrecho paso escarbado en la montaña por un mago enloquecido durante la [[Guerra Divina]], su hechizo marcó un canal que divide la cordillera en dos. [[Forcosia]] y el [[Gremio]] financiaron una excavación hace 80 años para convertirlo en un paso que conectase el norte y el sur de Forcosia. La entrada al canal se encuentra en [[Luel]] y pocas personas tienen permiso para acceder al mismo.

## Criaturas
- [[Ogro de Cueva]]
- [[Goblins]]
