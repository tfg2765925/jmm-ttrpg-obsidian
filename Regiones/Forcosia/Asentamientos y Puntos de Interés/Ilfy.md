---
location: Forcosia
alegiance: Familia Luel
leader: Nurgen
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Un pequeño pueblo leñador que se encuentra en las afueras del [[Bosque de Luces]]. Se encarga de suministrar madera para la zona este de Forcosia, recientemente se ha producido un desbordamiento mágico en la proximidades. El Gobernador [[Nurgen]] se encuentra escondido en su hacienda con un grupo de aldeanos no infectados. Han bloqueado la entrada principal a la mazmorra con un Glifo mágico mayor y están esperando a que lleguen refuerzos.