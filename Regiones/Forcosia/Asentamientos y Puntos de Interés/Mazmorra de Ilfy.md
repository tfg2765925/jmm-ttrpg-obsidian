---
location: Ilfy
alegiance: Ninguna
leader: Ninguno
---
# `=this.file.name`

> [!infobox|wfull center]
> | |
> ---|---|
> **Ubicación** | `=this.location` |
> **Lealtad** | `=this.alegiance` |
> **Líder** | `=this.leader` |
> ## Descripción
> Mazmorra Divina descubierta el 14 de Bràxas por los leñadores de [[Ilfy]]. Una primera incursión por un grupo de leñadores causó un desbordamiento mágico casi inmediato cuando entraron en contacto con una colonia de [[Lodocitos]]. Los leñadores fueron rápidamente consumidos por el lodo y atacaron a sus vecinos llevándolos al interior de la mazmorra para que el cuerpo original pudiese crear más sirvientes.
> Ilfy y sus cercanías se encuentran vigiladas por [[Marioneta Lodocito]] que capturan cualquier criatura que encuentren para llevarla con vida al interior de la mazmorra.

## Criaturas
- [[Marioneta Lodocito]]
- [[Gran Lodocito]]
- [[Soldado Lodocito]]
- [[Lodocito Menor]]
## Mapa
![[mazmorra_de_ilfy.png]]


